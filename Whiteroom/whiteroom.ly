%* SVN FILE: $Id$ *%
% @author $Author$
% @version $Revision$
% @lastrevision $Date$
% @filesource $URL$

% This is a simple Barbershop template for lilypond.
% If you need something slightly more complex, search for it on the internet.

\version "2.17.97"

\header {
  % http://lilypond.org/doc/v2.12/Documentation/user/lilypond/Creating-titles
  title = "White Room"
  composer = "PETE BROWN and JACK BRUCE"
  arranger = "Jeff Harris"
  copyright = \markup \smaller \column {"Arrangement © 2009 Jeff Harris, Barbershop Bass Publishing, ASCAP. All Rights Reserved. No recording use, public performance" "for profit use or any other use requiring authorization, or reproduction or sale of copies in any form shall be made of or from this" "Arrangement unless licensed by the copyright owner or an agent or organization acting on behalf of the copyright owner." }
  tagline = "This arrangement was not written with the intention of its being used in SPEBSQSA contests."
}

\include "english.ly"
\pointAndClickOff
%\pointAndClickOff
#(set-global-staff-size 17.82 )
%{  Note: You should always turn off point and click in any LilyPond files to be
    distributed to avoid including path information about your computer in the
    .pdf file, which can pose a security risk. 
%}

\paper {
  #(set-paper-size "letter")
  bottom-margin   = 0.75\in
  top-margin      = 0.50\in
  foot-separation = 0.25\in
  left-margin     = 0.75\in
  line-width      = 7.00\in
}

global = {
  \key g \minor
  \time 5/4
  \autoBeamOff
  \set Score.tempoHideNote = ##t
  \tempo 4=116
  \set Staff.midiInstrument = #"trombone"
}

tenorMusic = \relative c'' {
  \tuplet 3/2 { g8 g g } g4 g g8 g g4 |
  \tuplet 3/2 { f8 f f } f4 f f8 f f4 |
  \tuplet 3/2 { f8 f f } f4 f f8 f f4 |
  \tuplet 3/2 { e8 e e } e4 e e8 e e4 |
  \tuplet 3/2 { g8 g g } g4 g g8 g g4 |
  \tuplet 3/2 { f8 f f } f4 f f8 f f4 |
  \tuplet 3/2 { f8 f f } f4 f f8 f f4 |
  \tuplet 3/2 { e8 e e } e4 e e8 e e4 \bar "||"
  \time 4/4
  e2 s2 |
  \key d \major
  s1
  s1
  
}
tenorWords = \lyricmode {
  _
}

leadMusic = \relative c' {
  \tuplet 3/2 { d8 d d } d4 d d8 d d4 |
  \tuplet 3/2 { c8 c c } c4 c c8 c c4 |
  \tuplet 3/2 { d8 d d } d4 d d8 d d4 |
  \tuplet 3/2 { c8 c c } c4 c c8 c c4 |
  \tuplet 3/2 { d8 d d } d4 d d8 d d4 |
  \tuplet 3/2 { c8 c c } c4 c c8 c c4 |
  \tuplet 3/2 { d8 d d } d4 d d8 d d4 |
  \tuplet 3/2 { c8 c c } c4 c c8 c c4 |
  \time 4/4
  c2 r8 f8~ f16 e8. |
  \key d \major
  e8. d16~ d4 r8 c c16 c8. |
}
leadWords = \lyricmode {
  dun dun dun dun dun dun dun dun.
  dun dun dun dun dun dun dun dun.
  dun dun dun dun dun dun dun dun.
  dun dun dun dun dun dun dun dun.
  dun dun dun dun dun dun dun dun.
  dun dun dun dun dun dun dun dun.
  dun dun dun dun dun dun dun dun.
  dun dun dun dun dun dun dun dun.
}

bariMusic = \relative c' {
  \tuplet 3/2 { bf8 bf bf } bf4 bf bf8 bf bf4 |
  \tuplet 3/2 { a8 a a } a4 a a8 a a4 |
  \tuplet 3/2 { a8 a a } a4 a a8 a a4 |
  \tuplet 3/2 { g8 g g } g4 g g8 g g4 |
  \tuplet 3/2 { bf8 bf bf } bf4 bf bf8 bf bf4 |
  \tuplet 3/2 { a8 a a } a4 a a8 a a4 |
  \tuplet 3/2 { a8 a a } a4 a a8 a a4 |
  \tuplet 3/2 { g8 g g } g4 g g8 g g4 |
  \time 4/4
  g2 r2 |
  \key d \major
  s1
  s1
}
bariWords = \lyricmode {
  _
}

bassMusic = \relative c' {
  \tuplet 3/2 { g8 g g } g4 g g8 g g4 |
  \tuplet 3/2 { f8 f f } f4 f f8 f f4 |
  \tuplet 3/2 { d8 d d } d4 d d8 d d4 |
  \tuplet 3/2 { c8 c c } c4 c c8 c c4 |
  \tuplet 3/2 { g'8 g g } g4 g g8 g g4 |
  \tuplet 3/2 { f8 f f } f4 f f8 f f4 |
  \tuplet 3/2 { d8 d d } d4 d d8 d d4 |
  \tuplet 3/2 { c8 c c } c4 c c8 c c4 |
  \time 4/4
  a2 r2
  \key d \major
  d4 d d d
  c c c c
}
bassWords = \lyricmode {
  _
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%       STOP EDITING HERE.    STOP EDITING HERE.       %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\score {
  \new ChoirStaff <<
    \new Lyrics = tenors { s1 }
    \new Staff = topstaff <<
      \clef "treble_8"
      \new Voice = "tenors" {
        \voiceOne
        << \global \tenorMusic >>
      }
      \new Voice = "leads" {
        \voiceTwo
        << \global \leadMusic >>
      }
    >>
    \new Lyrics = "leads" { s1 }
    \new Lyrics = "baris" { s1 }
    \new Staff = bottomstaff <<
      \clef bass
      \new Voice = "baris" {
        \voiceOne
        << \global \bariMusic >>
      }
      \new Voice = "basses" {
        \voiceTwo << \global \bassMusic >>
      }
    >>
    \new Lyrics = basses { s1 }
    \context Lyrics = tenors \lyricsto tenors \tenorWords
    \context Lyrics = leads \lyricsto leads \leadWords
    \context Lyrics = baris \lyricsto baris \bariWords
    \context Lyrics = basses \lyricsto basses \bassWords
  >>
  \layout {
    \context {
      % a little smaller so lyrics can be closer to the staff
      \Staff
      % \override VerticalAxisGroup.minimum-Y-extent = #'(-3 . 3)
      % \override SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/1)
      % \override SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/32)
      % \override Score.SeparationItem.padding = #1
    }
  }
  \midi { }
}


%{
convert-ly (GNU LilyPond) 2.18.0  convert-ly: Processing `'...
Applying conversion: 2.13.0, 2.13.1, 2.13.4, 2.13.10,  Not smart
enough to convert minimum-Y-extent. Vertical spacing no longer depends
on the Y-extent of a VerticalAxisGroup. Please refer to the manual for
details, and update manually. 2.13.16, 2.13.18, 2.13.20, 2.13.27,
2.13.29, 2.13.31, 2.13.36, 2.13.39, 2.13.40,  Not smart enough to
convert foot-separation. Adjust settings for last-bottom-spacing
instead. Please refer to the manual for details, and update manually.
2.13.42, 2.13.44, 2.13.46, 2.13.48, 2.13.51, 2.14.0, 2.15.7, 2.15.9,
2.15.10, 2.15.16, 2.15.17, 2.15.18, 2.15.19, 2.15.20, 2.15.25,
2.15.32, 2.15.39, 2.15.40, 2.15.42, 2.15.43, 2.16.0, 2.17.0, 2.17.4,
2.17.5, 2.17.6, 2.17.11, 2.17.14, 2.17.15, 2.17.18, 2.17.19, 2.17.20,
2.17.25, 2.17.27, 2.17.29, 2.17.97
%}
