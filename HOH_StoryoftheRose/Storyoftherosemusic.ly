%{ SVN FILE: $Id$
   @author $Author$
   @version $Revision$
   @lastrevision $Date$
   @filesource $URL$

   The Story of the Rose (Heritage of Harmony Edition)
   Words by "Alice" Music by Andrew Mack
   copyright 1899.

%}

\version "2.17.97"
\language "english"

\header {
  % http://lilypond.org/doc/v2.12/Documentation/user/lilypond/Creating-titles
  title = "THE STORY OF THE ROSE"
  subtitle = "(HEART OF MY HEART)"
  subsubtitle = "(1899)"
  composer = "ANDREW MACK"
  lastupdated = "02/05/2014"
  copyright = \markup \smaller \column {" " "Arrangement © 1988 SPEBSQSA" }
  tagline = \markup \column {"Questions about the contest suitability of this song/arrangement should be directed to the judging community" "and measured against current contest rules. Ask before you sing." "$Id$" }
}

\include "english.ly"

global = {
  \key af \major
  \time 3/4
  \autoBeamOff
  \set Score.tempoHideNote = ##t
  % \override SpacingSpanner.spacing-increment = #8.0
}

% print this by using '^\xVoice'
xVoice = \markup { \smaller \sans { x } }


tenorMusic = \relative c' {
  % dynamic marking is only for Midi track and should be removed before printing
  \set midiInstrument = #"clarinet"
  % Verse 1
  \partial 4 df4^\markup { "Verse 1:" }|
  ef2 ef4 | e e e | ef?2 ef4 | ef( f) ef |

  % 05
  d2 d4 | d d d | ef2. | df2 df4 |

  % 09
  c2 c4 | e2 e4 | ef?2 ef4 | ef2. |

  % 13
  d4 d d | d2 d4 | df?2. | df2 s4 |

  % 17 Chorus 1:
  c4^\markup { "Chorus 1:" } c c4 | ef2 d4 | df?2. | df2. |

  % 21
  df4 c df | df2 df4 | df2. | c2 s4 |

  %25
  ef4 d ef | ef2 ef4 | d2. | d2 d4 |

  % 29
  d2. | d2 d4 | df?2. | df2 s4 |

  % 33
  c4 c c | ef2 d4 | df?2. | df2. |

  % 37
  df4 c df | df?2 df4 | df2. | c2 ef4 |

  % 41
  e4 ds e | ef?2^\xVoice f4 | gf2\( f4\) | f\( ff2 \) |

  % 45
  ef2. | df2. | c2 d4 | df2 df4

  % 49 Verse 2:
  ef2^\markup { "Verse 2:" } ef4 | e e e | ef?2 ef4 | ef( f) ef |

  % 53
  d2 d4 | d d d | ef2 d4 | df2 s4 |

  % 57
  c4 c4 c4 | e2 e4 | ef?2 ef4 | ef2 ef4 |

  % 61
  d2 d4 | d2 d4 | df?2.~ | df2 s4 |

  % 65 Chorus 2:
  c4^\markup { "Chorus 2:" } c c4 | ef2 d4 | df?2. | df2. |

  % 69
  df4 c df | df2 df4 | df4\( d df \) | c2 s4 |

  % 73
  ef4 d ef | ef2 ef4 | d2. | d2 d4 |

  % 77
  d2. | d2 d4 | df?2. | df2 s4 |

  % 81
  c4 c c | ef2 d4 | df?2. | df2. |

  % 85
  df4 c df | df?2 df4 | df2. | c2 ef4 |

  % 89
  e4 ds e | ef?2^\xVoice f4 | gf2\( f4\) | f\( ff2 \) |

  % 91
  ef2\( e4 | ef?2.\) | d2.\( | df?2. \) |

  % 95
  b2.\(^\fermata | c2\fermata \) s4 |
}

tenorWords = \lyricmode {
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _

}

leadMusic = \relative c {
  % dynamic marking is only for Midi track and should be removed before printing
  \set midiInstrument = #"trombone"
  \tempo 4=132

  \partial 4 ef

  % Verse 1
  c'2 c4 | c c c | c2 c4 | c( df) c |

  % 05
  bf2 f4 | f4 g af | c2. | bf2 ef,4 |

  % 09
  af2 af4 | g2 g4 | f2 f4 | c'2. |

  % 13
  bf4 c bf | g2 f4 | bf2. | bf2 b4\rest \bar "||"

  % 17 Chorus 1:
  af4 g af4 | ef2 f4 | af2. | g2. |

  % 21
  bf4 a bf | ef,2 bf'4 | bf2. | af2 b4\rest |

  %25
  c4 b c | f,2 c'4 | c2. | bf2 f4 |

  % 29
  c'2. | bf2 f4 | af2. | g2 b4\rest |

  % 33
  af4 g af | ef2 f4 | af2. | g2. |

  % 37
  bf4 a bf | ef,2 bf'4 | bf2. | af2 c4 |

  % 41
  c4 b c | f2 ef4 | ef2. | df2. |

  % 45
  c2. | bf2. | af2 af4 | g2 \bar "||" ef4

  % 49 Verse 2:
  c'2 c4 | c c c | c2 c4 | c( df) c |

  % 53
  bf2 f4 | f4 g af | c2.( | bf2) b4\rest |

  % 57
  af4 af af | g2 g4 | f2 f4 | c'2 c4 |

  % 61
  bf4( c) bf | g2 f4 | bf2.~ | bf2 b4\rest \bar "||"

  % 65 Chorus 2:
  af4 g af4 | ef2 f4 | af2. | g2. |

  % 69
  bf4 a bf | ef,2 bf'4 | bf2. | af2 b4\rest |

  % 73
  c4 b c | f,2 c'4 | c2. | bf2 f4 |

  % 77
  c'2. | bf2 f4 | af2. | g2 b4\rest |

  % 81
  af4 g af | ef2 f4 | af2. | g2. |

  % 85
  bf4 a bf | ef,2 bf'4 | bf2. | af2 c4 |

  % 89
  c4 b c | f2 ef4 | ef2. | df2. |

  % 93
  c2.( | c2.) | bf2.( | bf2.) |

  % 97
  af2.( | af2) b4\rest
  \bar "|."

}
leadWords = \lyricmode {
  % Verse 1
  A youth one day in a gar -- den fair __ a

  %5
  rose found with -- ered and dy -- ing, And

  %9
  all for love, ah! love in vain,

  %13
  This rose was sad -- ly sigh -- ing.

  %17 Chorus 1:
  Heart of my heart, I love you,

  %21
  Life would be naught with -- out you.

  %25
  Light of my life, my dar -- ling, I

  %29
  Love you, I love you.

  %33
  I can for -- get you nev -- er,

  %37
  From you I ne'er can sev -- er. Oh,

  %41
  say you'll be mine for -- ev -- er;

  %45
  I love you, I do. Oh,

  %49 Verse 2:
  sweet wild rose of a sum -- mer day, __ thy

  %53
  love has all been in vain. __

  %57
  Loved by a maid, then cast a -- way, I

  %61
  e -- cho thy re -- frain. __

  %65 Chorus 2:
  Heart of my heart, I love you,

  %69
  Life would be naught with -- out you.

  %73
  Light of my life, my dar -- ling, I

  %77
  Love you, I love you.

  %81
  I can for -- get you nev -- er,

  %85
  From you I ne'er can sev -- er. Oh,

  %89
  say you'll be mine for -- ev -- er;

  %93
  I __ love __  you. __

}

bariMusic = \relative c' {
  % dynamic marking is only for Midi track and should be removed before printing
  \set midiInstrument = #"clarinet"
  % Verse 1
  \partial 4 g
  af2 af4 | bf bf bf | a2 a4 | a2 a4 |

  % 05
  af?2 af4 | af4 bf f | g2. | g2 g4 |

  % 09
  ef2 ef4 | bf'2 bf4 | a2 a4 | a2. |

  % 13
  af?4 af af | bf2 af4 | af2. | g2 s4 |

  % 17 Chorus 1:
  ef4 ef4 ef4 | af2 af4 | f2. | ef2. |

  % 21
  g4 fs g | g2 g4 | g2. | ef2 s4 |

  % 25
  a4 gs a | a2 a4 | af?2. | af2 af4 |

  % 29
  af2. | af2 af4 | bf2. | bf2 s4 |

  % 33
  ef,4 ef ef | af2 af4 | f2. | ef2. |

  % 37
  g4 fs g | g2 g4 | g2. | ef2 af4 |

  % 41
  bf4 a bf | a2 a4 | bf2\( a4\) | bf4\( af?2\) |

  % 45
  af2. | g2. | ef2 f4 | ef2 g4

  % 49 Verse 2:
  af2 af4 | bf bf bf | a2 a4 | a2 a4 |

  % 53
  af?2 af4 | af4 bf f | g2 af4 | g2 s4 |

  % 57
  ef4 ef ef | bf'2 bf4 | a2 a4 | a2 a4 |

  % 61
  af?2 af4 | bf2 af4 | af2.\( | g2\) s4 |

  % 65 Chorus 1:
  ef4 ef4 ef4 | af2 af4 | f2. | ef2. |

  % 69
  g4 fs g | g2 g4 | g4\(af g\) | ef2 s4 |

  % 73
  a4 gs a | a2 a4 | af?2. | af2 af4 |

  % 77
  af2. | af2 af4 | bf2. | bf2 s4 |

  % 81
  ef,4 ef ef | af2 af4 | f2. | ef2. |

  % 85
  g4 fs g | g2 g4 | g4\( af g\) | ef2 af4 |

  % 89
  bf4 a bf | a2 a4 | bf2\( a4\) | bf4\( af?2\) |

  % 93
  af2\( bf4 | a2.\) | f4\( af2 | g2.\)

  % 97
  f2.( | ef2) b4\rest
}


bariWords = \lyricmode {
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _

}

bassMusic = \relative c {
  % dynamic marking is only for Midi track and should be removed before printing
  \set midiInstrument = #"clarinet"

  % Verse 1
  \partial 4 bf

  af2 af'4 | g g g | f2 f4 | f2 f4 |

  % 05
  f2 bf,4 | bf4 bf bf | ef2. | ef2 bf4 |

  % 09
  af2 af4 | c2 c4 | c2 c4 | f2. |

  % 13
  f4 f f | bf,2 bf4 | f'2. | ef2 b4\rest |

  % 17 Chorus 1:
  af4 af4 af4 | c2 cf4 | bf2. | bf2. |

  % 21
  ef4 ef ef | bf2 ef4 | ef2. | af,2 b4\rest |

  % 25
  f'4 f f | c2 f4 | f2. | f2 bf,4 |

  % 29
  f'2. | f2 bf,4 | f'2( ff4) | ef2 b4\rest |

  % 33
  af4 af af | c2 cf4 | bf2. | bf2. |

  % 37
  ef4 ef ef | bf2 ef4 | ef2. | af,2 af'4 |

  % 41
  g4 fs g | c,2 c4 | c2. | bf2. |

  % 45
  ef2. | ef2. | af,2 cf4 | bf2 bf4

  % 49 Verse 2:
  af2 af'4 | g g g | f2 f4 | f2 f4 |

  % 53
  f2 bf,4 | bf4 bf bf | ef2 f4 | ef2 b4\rest |

  % 57
  af4 af af | c2 c4 | c2 c4 | f2 f4 |

  % 61
  f2 f4 | bf,2 bf4 | f'2 ff4 | ef2 b4\rest \bar "||"

  % 65 Chorus 2:
  af4 af4 af4 | c2 cf4 | bf2. | bf2. |

  % 69
  ef4 ef ef | bf2 ef4 | ef4\( f ef\) | af,2 b4\rest |

  % 73
  f'4 f f | c2 f4 | f2. | f2 bf,4 |

  % 77
  f'2. | f2 bf,4 | f'2( ff4) | ef2 b4\rest |

  % 81
  af4 af af | c2 cf4 | bf2. | bf2. |

  % 85
  ef4 ef ef | bf2 ef4 | ef4\( f ef \) | af,2 af'4 |

  % 89
  g4 fs g | c,2 c4 | c2. | bf2. |

  % 93
  af4\( c g' | f2.\) | bf,4\( f' ff | ef2.\) |

  % 97
  df2.( | af2) b4\rest
}

bassWords = \lyricmode {
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _

}



