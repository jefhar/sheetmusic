%{ SVN FILE: $Id$
   @author $Author$
   @version $Revision$
   @lastrevision $Date$
   @filesource $URL$

  The Story of the Rose (Heritage of Harmony Edition)
  Words by "Alice" Music by Andrew Mack
  copyright 1899.

%}

\version "2.17.97"

\include "articulate.ly"
\include "../letterpaper.ly"
\include "../HOH_StoryoftheRose/Storyoftherosemusic.ly"

#(ly:set-option 'midi-extension "midi")

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%       STOP EDITING HERE.    STOP EDITING HERE.       %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% showLastLength = R2*48
\score
{
  \new ChoirStaff <<
    %\new ChordNames { \global \set Staff.midiInstrument = #"accordion" \theChords }
    \new Lyrics = "tenors" \with {
      % this is needed for lyrics above a staff
      \override VerticalAxisGroup.staff-affinity = #DOWN
    }{ s1 }
    \new Staff = "topstaff" <<

      \clef "treble_8"
      \new Voice = "tenors" {
        \voiceOne
        <<
          \global
          % \set midiInstrument = #"clarinet"
          \tenorMusic
        >>
      }
      \new Voice = "leads" {
        \voiceTwo
        <<
          \global
          \leadMusic
        >>
      }
    >>
    \new Lyrics = "leads" { s1 }
    \new Lyrics = "baris" \with {
      % this is needed for lyrics above a staff
      \override VerticalAxisGroup.staff-affinity = #DOWN
    } { s1 }
    \new Staff = "bottomstaff" <<
      \clef bass
      \new Voice = "baris" {
        \voiceOne
        <<
          \global
          \bariMusic
        >>
      }
      \new Voice = "basses" {
        \voiceTwo <<
          \global
          \bassMusic
        >>
      }
    >>
    \new Lyrics = "basses" { s1 }
    \context Lyrics = tenors \lyricsto tenors \tenorWords
    \context Lyrics = leads \lyricsto leads \leadWords
    \context Lyrics = baris \lyricsto baris \bariWords
    \context Lyrics = basses \lyricsto basses \bassWords
  >>
  \layout {
    \context {
      \Staff
    }
    \context {
      \Lyrics
      \override LyricSpace.minimum-distance = #2.0
    }
  }
  \midi {
    midiChannelMapping = #'voice
    \context {
      \ChoirStaff
      \remove "Staff_performer"
    }
    \context {
      \Staff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"
    }
  }
}

