#!/bin/bash
rm -rf out && lilypond-book --output=out --pdf book.lytex && cd out && pdflatex book.tex && pdflatex book.tex && cd .. && acroread -geometry 1100x600 /a "view=Fit" out/book.pdf
