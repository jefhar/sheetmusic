%{ SVN FILE: $Id$
  * vim: set expandtab tabstop=4 softtabstop=4 shiftwidth=4: *
  @author $Author$
  @version $Revision$
  @lastrevision $Date$
  @filesource $URL$

  Triskadecamania
%}

\version "2.17.97"

\header {
  % http://lilypond.org/doc/v2.14/Documentation/user/lilypond/Creating-titles
  title = "Triskadecamania"
  subtitle = "(1992)"
  composer = "Jeffrey Harris (b1970)"
  % poet = "Lyrics by"
  copyright = \markup \smaller \column {"Copyright © 1992, 2012 Jeff Harris, Barbershop Bass Publishing, ASCAP. All Rights Reserved. No recording" "use, public performance for profit use or any other use requiring authorization, or reproduction or sale of copies in" "any form shall be made of or from this Creation unless licensed by the copyright owner or an agent or organization" "acting on behalf of the copyright owner." }
  tagline = \markup \tiny \italic \column { "$Id$"}

}

\include "english.ly"

global = {
  \key c \major
  \time 3/8
  %\autoBeamOff
  \set Score.tempoHideNote = ##t
  \tempo 8=96
  % \set Staff.midiInstrument = #"trombone"
  \numericTimeSignature
}

subP = \markup { \italic {sub.} \dynamic p }

fluteOne = \relative c''' {
  \override Glissando.style = #'zigzag
  \set Staff.midiInstrument = #"flute"
  \clef "G"
  bf4\mf a8 |
  c4 b8 |
  af8 g, cs,~
  cs4. |
  fs'4 r8 |
  d,8\< f e |
  e'8\sfz r8 r8 |
  g,4 fs8 |
  a'4 af8 |
  f8 e4 |
  bf8. e8. |
  c'8. b,8. |
  d8 d d |
  r8 d r |
  \once \override Staff.NoteHead.style = #'cross e8\ff-\bendAfter #-8 ^\markup {"Ah!" }  r r |
  df8\mf\< df df->\ff \bar "|."
}

fluteTwo = \relative c' {
  \set Staff.midiInstrument = #"flute"
  \clef "G"
  r4. |
  r4. |
  r4. |
  r4. |
  e4\mf ds8 |
  gf4 f8 |
  d'8 df g, |
  r8 c4~\< |
  c8\f r r |
  r8 a af |
  b8 bf4 |
  b4 c8 |
  a8 as cs, |
  d'8 af' ds,, |
  fs'8 g,8. e'16 |
  f8\mf\< f f->\ff
}

violin = \relative c'' {
  \clef "G"
  \set Staff.midiInstrument = #"violin"
  r4. |
  r4. |
  bf4\mf a8 |
  c4 b8 |
  gs'8\< cs,4\f |
  g4 fs8 |
  ef'8. d8. |
  f8. e8. |
  r4. |
  d4 cs8 |
  e4 ef8 |
  c,8~\ff c4\p\< |
  b'8. f'8. |
  bf,8. g'8.
  r8\ff a,4 |
  gs8\mf\< gs gs->\ff
}

chimes = \relative c'' {
  \clef "G"
  \set Staff.midiInstrument = "tubular bells"
  r4. |
  r4. |
  r4. |
  r4. |
  r4. |
  r4. |
  e4\mp ef8 |
  gf,4 <f f'>8 |
  r8 d'4\laissezVibrer |
  g,4. \laissezVibrer |
  r8 r8 cs |
  c?4.\laissezVibrer |
  a4 a8 |
  gs4 gs8 |
  b8 b4 |
  bf8\mp\< bf bf->\mf |

}


%{
convert-ly (GNU LilyPond) 2.18.0  convert-ly: Processing `'...
Applying conversion: 2.15.7, 2.15.9, 2.15.10, 2.15.16, 2.15.17,
2.15.18, 2.15.19, 2.15.20, 2.15.25, 2.15.32, 2.15.39, 2.15.40,
2.15.42, 2.15.43, 2.16.0, 2.17.0, 2.17.4, 2.17.5, 2.17.6, 2.17.11,
2.17.14, 2.17.15, 2.17.18, 2.17.19, 2.17.20, 2.17.25, 2.17.27,
2.17.29, 2.17.97
%}
