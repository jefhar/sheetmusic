%{ SVN FILE: $Id$
  * vim: set expandtab tabstop=4 softtabstop=4 shiftwidth=4: *
  @author $Author$
  @version $Revision$
  @lastrevision $Date$
  @filesource $URL$

  Triskadecamania
%}

\version "2.17.97"

\include "../6x9paper.ly"

\include "../Triskadecamania/triskamusic.ly"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%       STOP EDITING HERE.    STOP EDITING HERE.       %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\score {
  <<
    \new StaffGroup = "thestaff" <<
      \new GrandStaff = "GrandStaff_flutes" <<
        \new Staff = "Staff_fluteI" {
          \set Staff.instrumentName = #"Flute I"
          \set Staff.shortInstrumentName = #"Fl. I"
          \global
          \fluteOne
        }
        \new Staff = "Staff_fluteII" {
          \set Staff.instrumentName = #"Flute II"
          \set Staff.shortInstrumentName = #"Fl. II"
          \global
          \fluteTwo
        }
      >>
      \new Staff = "Staff_violin" {
        \set Staff.instrumentName = #"Violin"
        \set Staff.shortInstrumentName = #"Vn."
        \global
        \violin
      }
      \new Staff = "Staff_chimes" {
        \set Staff.instrumentName = #"Chimes"
        \set Staff.shortInstrumentName = #"Chm."
        \global
        \chimes
      }
    >>
  >>
  \layout {
  }
}



%{
convert-ly (GNU LilyPond) 2.18.0  convert-ly: Processing `'...
Applying conversion: 2.15.7, 2.15.9, 2.15.10, 2.15.16, 2.15.17,
2.15.18, 2.15.19, 2.15.20, 2.15.25, 2.15.32, 2.15.39, 2.15.40,
2.15.42, 2.15.43, 2.16.0, 2.17.0, 2.17.4, 2.17.5, 2.17.6, 2.17.11,
2.17.14, 2.17.15, 2.17.18, 2.17.19, 2.17.20, 2.17.25, 2.17.27,
2.17.29, 2.17.97
%}
