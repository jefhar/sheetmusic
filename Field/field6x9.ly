%{ SVN FILE: $Id$
  @author $Author$
  @version $Revision$
  @lastrevision $Date$
  @filesource $URL$

  My Field
%}

\version "2.17.97"

\include "../6x9paper.ly"

\include "../Field/fieldmusic.ly"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%       STOP EDITING HERE.    STOP EDITING HERE.       %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\score {
  \context ChoirStaff
  <<
    \context Staff = soprano <<
      \set Staff.instrumentName = #"Soprano"
      \set Staff.shortInstrumentName = #"S"
      \context Voice = sop \sopranoNotes
      \lyricsto "sop" \new Lyrics \sopranoWords
    >>
    \context Staff = alto <<
      \set Staff.instrumentName = #"Alto"
      \set Staff.shortInstrumentName = #"A"
      \context Voice = alt \altoNotes
      \lyricsto "alt" \new Lyrics \altoWords
    >>
    \context Staff = tenor <<
      \clef "G_8"
      \set Staff.instrumentName = #"Tenor"
      \set Staff.shortInstrumentName = #"T"
      \context Voice = ten \tenorNotes
      \lyricsto "ten" \new Lyrics \tenorWords
    >>
    \context Staff = bass <<
      \set Staff.instrumentName = #"Bass"
      \set Staff.shortInstrumentName = #"B"
      \context Voice = bas \bassNotes
      \lyricsto "bas" \new Lyrics \bassWords
    >>

    \new PianoStaff
    <<
      \set PianoStaff.instrumentName = #"Piano"
      \set PianoStaff.shortInstrumentName = #"Pno."
      \new Staff  <<
        \partcombine \pianoS \pianoA
      >>
      \new Staff  <<
        \partcombine \pianoT \pianoB

      >>
    >>
  >>

  \layout {
    \context {
      \RemoveEmptyStaffContext
    }
    \context {
      \Lyrics
      \override LyricSpace.minimum-distance = #2.0
    }
  }
}

%{
convert-ly (GNU LilyPond) 2.18.0  convert-ly: Processing `'...
Applying conversion: 2.15.7, 2.15.9, 2.15.10, 2.15.16, 2.15.17,
2.15.18, 2.15.19, 2.15.20, 2.15.25, 2.15.32, 2.15.39, 2.15.40,
2.15.42, 2.15.43, 2.16.0, 2.17.0, 2.17.4, 2.17.5, 2.17.6, 2.17.11,
2.17.14, 2.17.15, 2.17.18, 2.17.19, 2.17.20, 2.17.25, 2.17.27,
2.17.29, 2.17.97
%}
