%* SVN FILE: $Id$ *%
%* vim: set expandtab tabstop=4 softtabstop=4 shiftwidth=4: *%
% @author $Author$
% @version $Revision$
% @lastrevision $Date$
% @filesource $URL$

% This is a simple Barbershop template for lilypond.
% If you need something slightly more complex, search for it on the internet.

\version "2.17.97"

\header {
  % http://lilypond.org/doc/v2.12/Documentation/user/lilypond/Creating-titles
  title = "My Field"
  %  subtitle = "(A Special Tag)"
  subsubtitle = "1991"
  composer = "Music by Jeff Harris (b. 1970)"
  poet = "Lyrics by Jeff Harris"
  %  arranger = "Arranged by"
  copyright = \markup \smaller \column {"Arrangement © 1991, 2012 Jeff Harris, Barbershop Bass Publishing, ASCAP. All Rights Reserved. No recording use, public" "performance for profit use or any other use requiring authorization, or reproduction or sale of copies in any form shall be" "made of or from this Arrangement unless licensed by the copyright owner or an agent or organization acting on behalf of the" "copyright owner." }
}

\include "english.ly"

global = {
  \key c \major
  \time 4/4
  \set Score.autoBeaming = ##f
  \set Score.tempoHideNote = ##t
  \tempo 4=144
  \set Staff.midiInstrument = #"trombone"
  \partial 8
}

\include "english.ly"

subP = \markup { \italic {sub.} \dynamic p }

longrest = \relative c' {
  r8 |
  r1 |
  r1 |
  r1 |
  r1 |
  r1 |
  r1 |
  r1 |
  r1 |
  r1 |
  r1 |
}

sopranoNotesA = \relative c' {
  f8\f |
  g4 c c c8 c |
  b4 c2 b4 |
  c2. r8 f,8 |
  g4 c c4. c8 |
  f4 c c c8 c |
  g4 c c c8 c |
  b4 c2 b4 |
  c2. \breathe e,8 f |
  g4 c c8 b a4~ |
  a8 g b2 d4 |
  c2. r8 c |
  g'8 f e d c4 \tuplet 3/2 { b8 c d } |
  c2. r8 c |
  g'8 f e d c4 \tuplet 3/2 { b8 c d } |
  c2. r8 f, |
  g4 c c8 c c4 |
  b4 c2 b4 |
  c2. r8 f, |
  g4 c c c8 c |
  f4 c c c8 c |
  g4 c c8 c c4 |
  b4 c2 b4 |
  c2. r8 f,8 |
  g4 c4 c8 b a4~ |
  a8 g b2 d4 |
  c2. r8 c |
  g'8 f e d c4 \tuplet 3/2 {b8 c d } |
  c2. r8 c |
  g'8 f e d c4 \tuplet 3/2 {b8 c d } |
  c2. r8
}

sopranoNotesB = \relative c'' {
  a4 a a a8 a |
  a8 c b4. a8 a4 |
  gs4 a b c |
  gs4 a4. gs8 g4 |
  b2 a |
  gs4\< a4. gs8 a4\!
  b4 c d e |
  f2-> e-> |
  c4 b8 a gs4. fs8 |
  gs4 a fs gs |
  a1 \breathe |
  a4 a a a8 a |
  a8 c b4. a8 a4 |
  gs4 a b c |
  gs4 a4. gs8 a4 |
  b2 a\fermata

  \bar "|."

}
sopranoWords = \lyricmode {
 I
 used to play in the
 grass field out
 there. The
 grass was long the
 sky was blue and I
 used to play in the
 grass field out
 there. I en-
 joyed breath- ing the crisp
 and fresh clean
 air. There
 used to be a broook bab- bl- ing
 by. There
 used to be a brook bab- bl- ing
 by. It
 was a hap- py place
 for me to
 be. Long,
 long a- go when I
 had no school and It
 was a hap- py place
 for me to
 be. And
 when I could, I climbed
 the tall- est
 tree. And
 jumped right down in- to a swim- ming
 pool. And
 jumped right down in- to a swim- ming
 pool.
 One day while sit- ting
 in the tree I saw
 a cloud of smoke.
 I heard an en-
 gine start.
 Then came the bull-
 doz- ers and a
 "\"Yee-" "Haw\""
 I ran a- way and
 felt sick in my
 heart.
 To- day I breathe sick
 and pol- lut- ed air.
 I look and see
 a park- ing lot
 out there.
}

altoNotesA = \relative c' {
 d8\f
 d2 e4 e8 f |
 g4 fs g4. f8~ |
 f4 e2 r8 d |
 d2 e4. e8 |
 a4 a g fs8 fs |
 d4 f4. e8 e e |
 g4 fs g4. f8~ |
 f4 e2 d4 |
 d2 e4 e |
 e4 e2 f4~ |
 f4 e8(d e4) r8 fs |
 g4 g g \tuplet 3/2 { g8 g g } |
 e2. r8 fs |
 g4 g g \tuplet 3/2 { g8 g g } |
 e2. r8 d |
 d2 e8 e f4 |
 g4 fs g4. f8~ |
 f4 e2 r8 d |
 d2 e4 e8 f |
 a4 a g fs8 fs |
 d4 f e8 e f4 |
 g4 fs g4. f8~
 f4 e2 r8 d |
 d2 e4 e |
 e4 e2 f4~ |
 f4 e8( d e4) r8 fs |
 g4 g g \tuplet 3/2 { g8 g g } |
 e2. r8 fs |
 g4 g g \tuplet 3/2 { g8 g g } |
 e2. r8
}

altoNotesB = \relative c' {
 e4 e f f8 f |
 e8 e f4 f f |
 e4 f gs e |
 e4 e e e |
 e2 e |
 e4\< e e e\!
 gs4 a f gs |
 b2-> c-> |
 a4 g e2 |
 e4 fs d d |
 e1 \breathe |
 e4 e f f8 f |
 e8 e e4 e e |
 e4 f gs e |
 e4 e e e |
 d2. c4\fermata |
}

altoWords = \lyricmode {
 I
 used play in the
 grass field out out
 there. The
 grass long the
 sky was blue and I
 used to play in the
 grass field out out
 there. I
 liked the crisp
 and fresh clean
 air. There
 was a brook bab- bl- ing
 by. There
 was a brook bab- bl- ing
 by. It was hap- py place
 for me to to
 be. Long,
 a- go when I
 had no school and it
 was a hap- py place
 for me to to
 be. And
 when could climb
 the tall- est
 tree. And
 jumped down to a swim- ming
 pool. And
 jumped down to a swim- ming
 pool.
 One day while sit- ting
 in the tree I saw
 a cloud of smoke.
 I heard an en-
 gine start.
 Then came the bull-
 doz- ers and a
 "\"Yee-" "Haw\""
 I ran 'way
 felt sick in my
 heart.
 To- day I breathe sick
 and pol- lut- ed air.
 I look and see
 a park- ing lot
 out there.
}

tenorNotesA = \relative c' {

  a8\f
  b2 c4 c8 c |
  d4 c2 d4 |
  g,2. r8 a |
  b2 c4. c8 |
  c4 c e a,8 a |
  b4 a c c8 c |
  d4 c2 d4 |
  g,2. \breathe a4 |
  b2 c4 c |
  c4 gs2 a4 |
  c2. r8 a |
  b4 c d \tuplet 3/2 { d8 c b } |
  c2. r8 a |
  b4 c d \tuplet 3/2 { d8 c b } |
  c2. r8 a |
  b2 c8 c c4 |
  d4 c2 d4 |
  g,2. r8 a |
  b2 c4 c8 c |
  c4 c e a,8 a |
  b4 a c8 c c4 |
  d4 c2 d4 |
  g,2. r8 a |
  b2 c4 c |
  c gs2 b4 |
  c2. r8 a |
  b4 c d \tuplet 3/2 { d8 c b } |
  c2. r8 a |
  b4 c d \tuplet 3/2 { d8 c b } |
  c2. r8
}

tenorNotesB = \relative c' {
c4 c d d8 d |
  c8 c gs4 gs c |
  c4 c e c |
  c4 c b c |
  gs2 a |
  c4\< c b c\!
  b4 a bf b |
  d2-> c-> |
  e4 d b2 |
  b4 a2 b4 |
  c1 \breathe |
  c4 c d d8 d |
  c8 c gs4 gs c |
  c4 c e c |
  c4 c b c |
  gs2 a \fermata |
}

tenorWords = \lyricmode {
 I
 used play in the
 grass field out
 there. The
 grass long the
 sky was blue and I
 used to play in the
 grass field out
 there. I
 like the crisp
 and fresh clean
 air. There
 was a brook bab- bl- ing
 by. there
 was a brook bab- bl- ing
 by. It
 was hap- py place
 for me to
 be. Long,
 a- go when I
 had no school and it
 was a hap- py place
 for me to
 be. And
 when could climb
 the tall- est
 tree. And
 jumped down to a swim- ming
 pool. And
 jumped down to a swim- ming
 pool.
 One day while sit- ting
 in the tree I saw
 a cloud of smoke.
 I heard an en-
 gine start.
 Then came the bull-
 doz- ers and a
 "\"Yee-" "Haw\""
 I ran 'way
 felt sick in
 heart.
 To- day I breathe sick
 and pol- lut- ed air.
 I look and see
 a park- ing lot
 out there.
}

bassNotesA = \relative c {
  \clef bass
  d8\f |
  g,2 c4 a8 a |
  g4 af4 g2 |
  c2. r8 d |
  g,2 c4. c8 |
  f,4 f c' d8 a |
  g2 c4 a8 a |
  g4 af g2 |
  c2. \breathe d4 |
  g,2 c4 a |
  c4 b a g |
  c2. r8 d |
  g,4 g g g |
  c2. r8 d |
  g,4 g g g |
  c2. r8 d8 |
  g,2 c8 a a4 |
  g4 af g2 |
  c2. r8 d |
  g,2 c4 c8 c |
  f,4 f c' d8 a |
  g2 c8 a a4 |
  g4 af g2 |
  c2. r8 d |
  g,2 c4 a |
  c4 b a g |
  c2. r8 d |
  g,4 g g g |
  c2. r8 d |
  g,4 g g g |
  c2. r8
}

bassNotesB = \relative c {
  a4 a d d8 d |
  a8 a e'4 e a, |
  c4 a e' a, |
  c4 a e' a, |
  e'2 a, |
  c4\< a e' a,\! |
  e'4 f f e |
  gs2-> a-> |
  a4 b, e2 |
  e4 d2 e4 |
  a,1 \breathe |
  a4 a d d8 d |
  a8 a e'4 e a, |
  c4 f, e a |
  c4 a e' a, |
  e'4 e, a2\fermata
}

bassWords = \lyricmode {
 I
 used play in the
 grass field out
 there. The
 grass long the
 sky was blue and I
 used play in the
 grass field out
 there. I
 like the crisp
 and fresh fresh clean
 air. There
 was a brook bab
 by. There
 was a brook bab
 by. It
 was hap- py place
 for me to
 be. Long
 a- go when I
 had no school and it
 was hap- py place
 for me to
 be. And
 when could climb
 the tall tall- est
 tree. And
 jumped down to a
 pool. And
 jumped down to a
 pool.
 One day while sit- ting
 in the tree I saw
 a cloud of smoke.
 I heard an en-
 gine start.
 Then came the bull-
 doz- ers and a
 "\"Yee-" "Haw\""
 I ran 'way
 felt sick in
 heart.
 To- day I breathe sick
 and pol- lut- ed air.
 I look and see
 a park- ing lot
 out out there.
}

pianoBreakS = \relative c' {
  f8
  g4 c c c8 c |
  b4 c2 b4 |
  c2. r8 f,8 |
  g4 c4 c4. c8 |
  f4 c c c8 c |
  g4 c4 c c8 c |
  b4 c2 b4 |
  c2. r8 c8 |
  d8 c d4 e2~ |
  e2..\fermata r8

}

pianoBreakA = \relative c' {
d8 |
  d2 e4 e8 f |
  g4 fs g4. f8~ |
  f4 e2 r8 d8 |
  d2 e4. e8 |
  a4 a g fs8 fs |
  d4 f4. e8 e f |
  g4 fs g4. f8~ |
  f4 e2 r8 e8 |
  f2 e2~ |
  e2.. \fermata r8
}

pianoBreakT = \relative c' {
a8 |
  b2 c4 c8 c |
  d4 c2 d4 |
  g,2. r8 a8 |
  b2 c4. c8 |
  c4 c e a,8 a |
  b4 a c c8 c |
  d4 c2 d4 |
  g,2. r8 a |
  a2 gs~ |
  <b, gs'>2.. \fermata d8\rest
}

pianoBreakB = \relative c {
  d8 |
  g,2 c4 a8 a |
  g4 af g2 |
  c2. r8 d8 |
  g,2 c4. c8 |
  f,4 f c' d8 a |
  g2 c4 a8 a |
  g4 af g2 |
  c2. r8 a |
  d2 b2( |
  e,2..) \fermata s8
}

sopranoNotes = { \global \sopranoNotesA \longrest \sopranoNotesB }
altoNotes = { \global \altoNotesA \longrest \altoNotesB }
tenorNotes = { \global \tenorNotesA \longrest \tenorNotesB }
bassNotes = { \global \bassNotesA \longrest \bassNotesB }

pianoS = { \global \sopranoNotesA \pianoBreakS \sopranoNotesB}
pianoA = { \global \altoNotesA \pianoBreakA \altoNotesB}
pianoT = { \global \tenorNotesA \pianoBreakT \tenorNotesB }
pianoB = { \global \bassNotesA \pianoBreakB \bassNotesB }


%{
convert-ly (GNU LilyPond) 2.18.0  convert-ly: Processing `'...
Applying conversion: 2.15.7, 2.15.9, 2.15.10, 2.15.16, 2.15.17,
2.15.18, 2.15.19, 2.15.20, 2.15.25, 2.15.32, 2.15.39, 2.15.40,
2.15.42, 2.15.43, 2.16.0, 2.17.0, 2.17.4, 2.17.5, 2.17.6, 2.17.11,
2.17.14, 2.17.15, 2.17.18, 2.17.19, 2.17.20, 2.17.25, 2.17.27,
2.17.29, 2.17.97
%}
