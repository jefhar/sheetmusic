%{ SVN FILE: $Id$
  @author $Author$
  @version $Revision$
  @lastrevision $Date$
  @filesource $URL$

  Let Me Call You Sweetheart
  Lyrics by Beth Slater Whitson, Music by Leo Friedman.
  copyright MCMX by Leo Friedman.

  Arranged for Barbershop Quartet by Greg Lyne 10-1-84.
  Abridged by Palo Alto-Mountain View Chapter, SQPBSQSA
%}

\version "2.17.97"

\header {
  % http://lilypond.org/doc/v2.12/Documentation/user/lilypond/Creating-titles
  title = "Let Me Call You Sweetheart"
  subtitle = "(1910)"
  poet = "Words by BETH SLATER WHITSON"
  arranger = "Arr. by GREG LYNE (10-1-84)"
  lastupdated = "11/9/2012"
  composer = "Music by LEO FRIEDMAN"
  % copyright = \markup \smaller \column {"Arrangement © 2012 Jeff Harris, Barbershop Bass Publishing, ASCAP. All Rights Reserved. No recording use, public performance" "for profit use or any other use requiring authorization, or reproduction or sale of copies in any form shall be made of or from this" "Arrangement unless licensed by the copyright owner or an agent or organization acting on behalf of the copyright owner." }
  tagline = \markup \column {"Questions about the contest suitability of this song/arrangement should be directed to the judging community" "and measured against current contest rules. Ask before you sing." "$Id$" }
  subsubtitle = "Abridged by Palo Alto-Mountian View Chapter, SPEBSQSA"
}

\include "english.ly"

global = {
  \key bf \major
  \time 3/4
  % \autoBeamOff
  \set Score.tempoHideNote = ##t
  \tempo 4=144
  % \override SpacingSpanner.spacing-increment = #8.0

}

xVoice = \markup { \smaller \sans { x } } % print this by using '\xVoice'

tenorMusic = \relative c' {
  % dynamic marking is only for Midi track and should be removed before printing
  \set midiInstrument = #"clarinet"
  f2^\markup { "VERSE" } ef4 |
  d2 d4 |
  cs2 cs4 |
  d2. |
  
  f2. |
  e |
  ef \( |
  ef2 \) s4 |
  
  g2 d4 |
  f2 ef4 |
  ef2 ef4 |
  ef2. |
  
  ef2. |
  g |
  f \( |
  f2 \) s4 |
  
  fs2 fs4 |
  fs2 fs4 |
  g2 fs4 |
  g4 \( fs f \) |
  
  e2 e4 |
  e2 e4 |
  f2 e4 |
  f2. |
  
  f4\rest e e |
  f2 e4 |
  f4\rest f g |
  fs2 s4 |
  
  f4\rest f f |
  g4\rest e e |
  ef?2. \( |
  ef2 \) s4 \bar "||"
  
  % Chorus
  f2^\markup { "Chorus" } ef4 |
  d2 ef4 |
  f4 d2 |
  cs2 d4 |
  
  ef2. |
  f |
  e2 \( f4 |
  e2 \) s4 |
  
  ef?2 d4 |
  ef2 ef4 |
  ef4 ef2 |
  e2 ef4 |
  
  f2. |
  d |
  ef2. ( |
  ef2 ) s4 |
  
  f2 ef4 |
  d2 ef4 |
  f4 d2 |
  cs2 d4 |
  
  ef2. |
  d4 \( ef f \) |
  e2 \( ds4 |
  e2 \) s4 |
  
  ef?2 ef4 |
  g2 g4 |
  f4 d2 |
  g2 f4 |

  d4\rest e e |
  ef?2 ef4 |
  d2. ( |
  d2 ) s4 | \bar "||"
  
  % Tag key changed.
  ef?2^\markup { "Tag" } ef4 |
  g2 g4 |
  f2. |
  f4 g f |
  
  e2. |
  ef2. |
  f4\rest ef d |
  f2 e4 |
  
  ef2. |
  c |
  d( |
  d4 ) s4 s4 |
  
  
}
tenorWords = \lyricmode {
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ in a land of love, it seems
  dar -- ling just with you.__ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ I'm in love with
  _ _ _ _ _ _ _ _ _ _ _
  I'm in love with on -- ly you. __

}

leadMusic = \relative c' {
  % dynamic marking is only for Midi track and should be removed before printing
  \set midiInstrument = #"trombone"
  d2 a4 |
  c2 bf4 |
  a2 g4 |
  f2. |
  
  bf2. |
  g |
  a (
  a2 ) r4 |
  
  ef'2 b4 |
  d2 c4 |
  a2 g4 |
  f2. |
  
  c' |
  cs |
  d ( |
  d2 ) r4 |
  
  d2 ef4 |
  d2 c4 |
  bf2 c4 |
  d2. |
  
  c2 d4 |
  c2 bf4 |
  a2 bf4 |
  c2. |
  
  g2 a4 |
  bf2 g4 |
  a2 bf4 |
  c2 r4 |
  
  g2. |
  c |
  c (
  c2 ) r4 |
  
  % Chorus
  d,2 f4 |
  bf2 c4 |
  d4 f,2 |
  e2 f4 |
  
  g2. |
  g2. |
  g2. ( |
  g2 ) r4 |
  
  a2 gs4 |
  a2 bf4 |
  c4 a2 |
  g2 a4 |
  
  f2. |
  f |
  f ( |
  f2 ) r4 |
  
  d2 f4 |
  bf2 c4 |
  d4 f,2 |
  e2 f4 |
  
  g2. |
  g |
  c ( |
  c2 ) r4 |
  
  c2 bf4 |
  a2 bf4 |
  d4 f,2 |
  ef'2 d4 |
  
  g,2. |
  a |
  bf ( |
  bf2 ) r4 |
  
  % tag key changed
  c2 bf4 |
  a2 bf4 |
  d2. |
  d4 ef d |
  
  c2. |
  c |
  bf ( |
  bf |
  bf |
  bf |
  bf |
  bf4 ) r4 r |
  
  \bar "|." 

}
leadWords = \lyricmode {
  I am dream -- ing Dear of you
  Day by day, __
  Dream -- ing when the skies are blue,
  When they're gray; __
  
  When they sil -- v'ry moon -- light gleams __
  Still I wan -- der on in dreams
  In a land of love, it seems Just with you. __
  
  Let me call you "\"Sweet" -- "heart,\""
  I'm in love with you, __
  Let me hear you whis -- per
  that you love me too. __
  
  Keep the love -- light glow -- ing
  in your eyes __ so __ true. __
  Let me call you "\"Sweet" -- "heart,\""
  I'm __ in love with you.
  
  Let me call you "\"Sweet" -- "heart,\""
  I'm in love with you. __
}

bariMusic = \relative c' {
  % dynamic marking is only for Midi track and should be removed before printing
  \set midiInstrument = #"clarinet"
  % Verse 1
  bf2 f4 |
  f2 f4 |
  g2 e4 |
  bf'2. |
  
  f2. |
  bf |
  g \(
  f2 \) s4 |
  
  c'2 gs4 |
  a2 a4 |
  f2 bf4 |
  a2. |
  
  a2. |
  a |
  bf \( |
  a2 \) s4 |
  
  c2 c4 |
  c2 a4 |
  d2 d4 |
  bf \( c b \) |
  
  bf2 bf4 |
  bf2 g4 |
  c2 c4 |
  a2. |
  
  s4 bf c |
  g2 bf4 |
  s4 c c |
  a2 s4 |
  
  s4 b b |
  s4 bf bf |
  s4 a bf |
  a2 s4 |
  
  % Chorus
  bf2 a4 |
  f2 a4 |
  bf4 bf2 |
  g2 af4 |
  
  bf2. |
  b2. |
  bf2. ( |
  bf2 ) s4 |
  
  c2 b4 |
  c2 g4 |
  a4 c2 |
  bf2 c4 |
  
  bf2. |
  gs |
  a ( |
  a2 ) s4 |
  
  bf2 a4 |
  f2 a4 |
  bf4 bf2 |
  g2 af4 |
  
  bf2. |
  b |
  bf2 \( a4 |
  bf2 \) s4 |
  
  g2 g4 |
  cs2 cs4 |
  bf4 bf \( c \) |
  b2 b4 |
  
  s4 bf bf |
  c2 f,4 |
  f2. ( |
  f2 ) s4 |
  
  % Tag changed key.
  g2 g4 |
  cs2 cs4 |
  bf2 \( c4 \) |
  b2 s4 |
  
  s4 c4 b |
  bf2 a4 |
  g4\rest g f |
  af2 df4 |
  
  c2. |
  gf |
  f ( |
  f4 ) s s |
  
}
bariWords = \lyricmode {
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ in a land of love, it seems
  dar -- ling just with on -- ly you.__ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _
  _ _ I'm in love with
  _ _ _ _ _ _ _ _
  I'm in love with I'm in love with on -- ly you. __

}

bassMusic = \relative c {
  % dynamic marking is only for Midi track and should be removed before printing
  \set midiInstrument = #"clarinet"
  % Verse 1
  bf2 c4 |
  bf2 bf4 |
  e2 a,4 |
  bf2. |
  
  d2. |
  df |
  c \( |
  c2 \) d4\rest |
  
  c2 e4 |
  f2 f4 |
  c2 c4 |
  c2. |
  
  f2. |
  e |
  bf \(
  d2 \) d4\rest
  
  a'2 a4 |
  a2 d,4 |
  g2 a4 |
  g \( a g \) |
  
  g2 g4 |
  g2 c,4 |
  f2 g4 |
  f2. |
  
  r4 c c |
  df2 c4 |
  r4 f ef |
  d2 d4\rest |

  d4\rest d d |
  d4\rest g g |
  d4\rest f g |
  f2 d4\rest |
  
  % Chorus
  bf2 c4 |
  bf2 f'4 |
  bf,4 bf2 |
  a2 bf4 |
  
  ef2. |
  d2. |
  c2 \( d4 |
  c2 \) d4\rest |
  
  f2 f4 |
  f2 c4 |
  f4 f2 |
  c2 f4 |
  
  d2. |
  b |
  c ( |
  c2 ) d4\rest |
  
  bf2 c4 |
  bf2 f'4 |
  bf,4 bf2 |
  a2 bf4 |
  
  ef4 \( d c \) |
  f \( ef d \) |
  g2 \( fs4 |
  g2 \) d4\rest |
  
  ef2 ef4 |
  e2 ef4 |
  bf4 bf \( af \) |
  g \( g' \) g |
  
  d4\rest c c |
  f2 c4 |
  bf2. (
  bf2 ) d4\rest |
  
  % Tag key changed
  ef2 ef4 |
  e2 ef4 |
  bf \( f' af \) |
  g2 d4\rest |
  
  d4\rest a' g |
  gf2 f4 |
  bf,2. \( |
  d2 \) g4 |
  g2. |
  ef2. |
  bf2. ( |
  bf4 ) d\rest d\rest |
  
}
bassWords = \lyricmode {
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ in a land of love, it seems
  dar -- ling just with on -- ly you.__ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ I'm in love with
  _ _ _ _ _ _ _ _ _
  I'm in love with you, __  _ with on -- ly you. __
}


%{
convert-ly (GNU LilyPond) 2.16.1  convert-ly: Processing `'...
Applying conversion: 2.15.7, 2.15.9, 2.15.10, 2.15.16, 2.15.17,
2.15.18, 2.15.19, 2.15.20, 2.15.25, 2.15.32, 2.15.39, 2.15.40,
2.15.42, 2.15.43, 2.16.0
%}


%{
convert-ly (GNU LilyPond) 2.18.0  convert-ly: Processing `'...
Applying conversion: 2.17.0, 2.17.4, 2.17.5, 2.17.6, 2.17.11, 2.17.14,
2.17.15, 2.17.18, 2.17.19, 2.17.20, 2.17.25, 2.17.27, 2.17.29, 2.17.97
%}
