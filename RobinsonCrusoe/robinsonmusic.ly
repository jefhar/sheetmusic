%{ SVN FILE: $Id$
  @author $Author$
  @version $Revision$
  @lastrevision $Date$
  @filesource $URL$

  Where Did Robinson Crusoe Go (With Friday on Saturday Night)?
  Lyrics by Sam M. Lewis and Joe Young, Music by Geo. W. Meyer.
  copyright 1916.

  Arranged for Barbershop Quartet by Jeff Harris.
%}

\version "2.17.97"

\header {
  % http://lilypond.org/doc/v2.12/Documentation/user/lilypond/Creating-titles
  title = "Where Did Robinson Crusoe Go"
  subtitle = "(With Friday on Saturday Night)?"
  subsubtitle = "(1916)"
  poet = \markup \column {"Lyric by SAM M. LEWIS." "and JOE YOUNG." }
  lastupdated = "10/27/2007"
  composer = "Music by GEO. W. MEYER."
  arranger = "Arranged by JEFF HARRIS."
  copyright = \markup \smaller \column {"Arrangement © 2012 Jeff Harris, Barbershop Bass Publishing, ASCAP. All Rights Reserved. No recording use, public performance" "for profit use or any other use requiring authorization, or reproduction or sale of copies in any form shall be made of or from this" "Arrangement unless licensed by the copyright owner or an agent or organization acting on behalf of the copyright owner." }
  tagline = \markup \column {"Questions about the contest suitability of this song/arrangement should be directed to the judging community" "and measured against current contest rules. Ask before you sing." "$Id$" }
}

\include "english.ly"

global = {
  \key af \major
  \time 2/4
  % \autoBeamOff
  \set Score.tempoHideNote = ##t
  \tempo 4=120
  % \override SpacingSpanner.spacing-increment = #8.0

}

xVoice = \markup { \smaller \sans { x } }

tenorMusic = \relative c' {
  % dynamic marking is only for Midi track and should be removed before printing
  \set midiInstrument = #"clarinet"
  c8^\markup { "Verse 1" } c c c~ |
  c c c ef |
  df^\xVoice f4 ef8~ |
  ef2|

  %  5
  c8 c c c~ |
  c c c ef |
  df^\xVoice f4 ef8~ |
  ef2|

  %  9
  ef8 d ef ef~^\xVoice |
  ef f f ef
  d d4 d8~ |
  d4. d8 |

  % 13
  d8 d4 d8~ |
  d4 d8 d8 |
  df c df df~^\xVoice |
  df2|

  % 17
  c8 c c c~ |
  c c c ef |
  f f4 g8~ |
  g2|

  % 21
  gf8 af gf gf~ |
  gf8 f gf4 |
  f2( |
  e) |

  % 25
  d8 ef f ef~ |
  ef eff d4 |
  ef8 ef g ef~ |
  ef ef df4 |

  % 29
  d8 df d d~ |
  d d d4 |
  r8 df8 d\noBeam \override BreathingSign.text = \markup {
    \musicglyph #"scripts.caesura.straight"
  } \breathe df8 |
  df2 |

  % 33
  c8^\markup { "Verse 2" } c c c~ |
  c c c ef |
  df^\xVoice f4 ef8~ |
  ef2|

  % 37
  c8 c c c~ |
  c c c ef |
  df^\xVoice f4 ef8~ |
  ef2|

  % 41
  ef8 d ef ef~^\xVoice |
  ef f f ef
  d d4 d8~ |
  d4. d8 |

  % 45
  d8 d4 d8~ |
  d4 d8 d8 |
  df c df df~^\xVoice |
  df2|

  % 49
  c8 c c c~ |
  c c c ef |
  df f4 g8~ |
  g2|

  % 53
  gf8 af gf ef~ |
  ef ef ef4 |
  f2( |
  e) |

  % 57
  d8 ef f ef~ |
  ef eff d4 |
  ef8 ef g ef~ |
  ef ef df4 |

  % 61
  d8 df d d~ |
  d d d4 |
  df?8 d4 d8 |
  df?2 |

  % 65
  c8^\markup { "Chorus" } s c c~ |
  c c ef4 |
  ef8^\xVoice f4 f8~ |
  f4 ef |

  % 69
  d8 d d d~ |
  d df d4 |
  d2~ |
  d4 d8 d |

  % 73
  df df ef df~^\xVoice |
  df d4 d8 |
  df8 df ef df~^\xVoice |
  df4 d8 df |

  % 77
  c4 df8 ef ~
  ef8 df c ef |
  f f f g~ |
  g4 g8 f |

  % 81
  f8 ef ef ef~ |
  ef ef4 ef8 |
  f8 ef ef ef~ |
  ef ef4 ef8 |

  % 85
  d8 d d d~ |
  d d4 d8 |
  d8 d d d~ |
  d8 df?4 d8 |

  % 89
  c8 s c c~ |
  c c ef4 |
  ef8^\xVoice ef4^\xVoice f8~
  f4 ef |

  % 93
  f8 f d^\xVoice d~
  d d df4 |
  c2~ |
  c|

  % 97 - Tag
  c8^\markup { "Tag" } s c c~ |
  c c ef4 |
  ef8^\xVoice ef4^\xVoice r8
  r8 f4.~ |

  % 101
  f2~ |
  f4\fermata a4|
  af8 a af c~ |
  c8 af bf4 |

  % 105
  gs8 gs es es~
  es es e4 |
  df8 ef f c'~ |
  c af bf4 |

  % 109
  af4 r |
  af4 r |
  r8 af4 af8 |
  af4 r |

}
tenorWords = \lyricmode {
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _
  the hut was shut.
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _
  _ just for two.
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ go, __ _

}

leadMusic = \relative c {
  % dynamic marking is only for Midi track and should be removed before printing
  \set midiInstrument = #"trombone"
  ef8 f af ef~ |
  ef f af c |
  ef d4 df8~ |
  df2 |

  %  5
  ef,8 f af ef~ |
  ef f af c |
  ef d4 df8~ |
  df2 |

  %  9
  c8 b c f~ |
  f ef df c |
  bf af4 f8~ |
  f4. c'8 |

  % 13
  bf8 af4 f8~ |
  f4 c'8 af |
  bf8 a bf ef~ |
  ef2 |

  % 17
  ef,8 f af ef~ |
  ef8 f af c |
  ef8 d4 df8~ |
  df2 |

  % 21
  ef8 f ef c~ |
  c bf af4 |
  bf2~ |
  bf |

  % 25
  bf8 c d! c~ |
  c cf bf4 |
  bf8 c ef c~ |
  c cf bf4 |

  % 29
  bf8 a bf af~ |
  af8 g f4 |
  bf2~ |
  bf2 \bar "||" \break

  % 33 - Verse 2
  ef,8 f af ef~ |
  ef f af c |
  ef d4 df8~ |
  df2 |

  % 37
  ef,8 f af ef~ |
  ef f af c |
  ef d4 df8~ |
  df2 |

  % 41
  c8 b c f~ |
  f ef df c |
  bf af4 f8~ |
  f4. c'8 |

  % 45
  bf8 af4 f8~ |
  f4 c'8 af |
  bf8 a bf ef~ |
  ef2 |

  % 49
  ef,8 f af ef~ |
  ef8 f ef\f gf |
  g? gs4 bf8~ |
  bf2 |

  % 53
  c8 c c af~ |
  af df c4 |
  df2( |
  df) |

  % 57
  bf8 c d c~ |
  c cf bf4 |
  bf8 c ef c~ |
  c cf bf4 |

  % 61
  bf8 a bf af~ |
  af8 g f4 |
  bf2~ |
  bf2 \bar "||" \break

  % 65 Chorus
  ef,8 b'8\rest f af~ |
  af bf c4 |
  f8 ef4 df8~ |
  df4 c |

  % 69
  c8 bf af f~ |
  f e f4 |
  bf2~ |
  bf4 af8 f |

  % 73
  g bf df f~ |
  f8 af,4 f8 |
  g8 bf df f~ |
  f4 f,8 g |

  % 77
  af4 bf8 c~ |
  c bf af c |
  bf a bf ef~ |
  ef4 ef8 d |

  % 81
  df8 c f, df'~ |
  df8 c4 f,8 |
  df'8 c f, df'~ |
  df c4 c8 |

  % 85
  c8 bf f c'~ |
  c bf4 f8 |
  c' bf f af~ |
  af g4 f8 |

  % 89
  ef8 b'8\rest f af~ |
  af bf c4 |
  g'8 f4 ef8~ |
  ef4 c |

  % 93
  df8 ef f c~ |
  c af bf4 |
  af2~ |
  af2 \bar "||" \break

  % 97 - Tag
  ef8 r8 f af~ |
  af bf c4 |
  g'8 f4 ef8 ~
  ef2~ |

  % 101
  ef2~ |
  ef4 c4 |
  df8 ef8 f d~ |
  d8 d df4 |

  % 105
  b8 bs b gs~ |
  gs gs as4 |
  f8 f af d~ |
  d d df4 |

  % 109
  ef4 r |
  f4 r|
  r8 eff4 df8 |
  ef4 r\bar "|."

}
leadWords = \lyricmode {
  Thous -- ands of years a -- go or may -- be more, __
  Out on an is -- land, on a lone -- ly shore, __
  Rob -- in -- son Cru -- soe land -- ed one fine day, __ No
  rent to pay, __ And no wife to o -- bey; __

  His good man Fri -- day was his on -- ly friend, __
  He did -- n't bor -- row or lend; __
  They built a lit -- tle hut, Lived there till Fri -- day, But
  Sat -- ur -- day night __ it was shut. __

  Rob -- in -- son Cru -- soe was a good old scout, __
  Rob -- in -- son Cru -- soe knew his way a -- bout, __
  He'd go out hunt -- ing chick -- ens now and then, __ But
  he knew when __ he was chas -- ing a hen; __

  Once he told Fri -- day, “you must stay at home, __
  I've got to go __ out a -- lone,” __
  Fri -- day felt ver -- y blue, He said it's wrong __ of you,
  Could -- n't you fix __ it for two? __

  Where did Rob -- in -- son Cru -- soe go, __ with
  Fri -- day on Sat -- ur -- day night? __ Ev' -- ry
  Sat -- ur -- day night __ they would start in to roam, __ And on
  Sun -- day morn -- ing they'd come stag -- ger -- ing home. __ On this

  is -- land lived wild __ men in can -- ni -- bal trim -- min', And
  where there are wild __ men there must be wild wom -- en, So
  where did Rob -- in -- son Cru -- soe go, __ with
  Fri -- day on Sat -- ur -- day night? __
  Where did Rob -- in -- son Cru -- soe go, __
  With Fri -- day on Sat -- ur -- day,
  Fri -- day on Sat -- ur -- day,
  Fri -- day on Sat -- ur -- day
  night, night.
  Quite the night!

}

bariMusic = \relative c' {
  % dynamic marking is only for Midi track and should be removed before printing
  \set midiInstrument = #"clarinet"
  af8 af ef af~ |
  af af ef gf |
  g? gs4 g8~ |
  g2 |

  %  5
  af8 af ef af~ |
  af af ef gf |
  g? gs4 g8~ |
  g2 |

  %  9
  a8 af a a~ |
  a a a a |
  af bf4 af8~ |
  af4. af8 |

  % 13
  af8 bf4 af8~ |
  af4 af8 f8 |
  g8 gf g g~ |
  g2 |

  % 17
  af8 af ef af~ |
  af af ef gf |
  g? gs4 bf8~ |
  bf2 |

  % 21
  c8 c c af~ |
  af8 df c4 |
  af2( |
  g2) |

  % 25
  af8 af af a~ |
  a af af4 |
  g8 g bf g~ |
  g g g4 |

  % 29
  af8 g af f~ |
  f bf af4 |
  s8 g8 af8\noBeam \override BreathingSign.text = \markup { \musicglyph #"scripts.caesura.straight"
  } \breathe af8 |
  g2 |

  % 33
  af8 af ef af~ |
  af af ef gf |
  g gs4 g8~ |
  g2 |

  % 37
  af8 af ef af~ |
  af af ef gf |
  g gs4 g8~ |
  g2 |

  % 41
  a8 af a a~ |
  a a a a |
  af bf4 af8~ |
  af4. af8 |

  % 45
  af8 bf4 af8~ |
  af4 af8 f8 |
  g8 gf g g~ |
  g2 |

  % 49
  af8 af ef af~ |
  af af af a |
  bf bf4 ef8~ |
  ef2 |

  % 53
  af,8 af af gf~ |
  gf g gf4 |
  af2( |
  gf) |

  % 57
  af8 af af a~ |
  a af af4 |
  g8 g bf g~ |
  g g g4 |

  % 61
  af8 g af f~ |
  f bf af4 |
  g8 af4 af8 |
  g2 |

  % 65
  af8 s af ef~ |
  ef gf af4 |
  a8 a4 a8~ |
  a4 a |

  % 69
  af8 af bf af~ |
  af g af4 |
  af2~ |
  af4 b8 af |

  % 73
  ef8 g g g~ |
  g f4 af8 |
  bf8 g g g~ |
  g4 af8 bf |

  % 77
  ef,4 g8 af~ |
  af g ef af |
  d d d df~ |
  df4 df8 af |

  % 81
  a8 a a a~ |
  a a4 a8 |
  a8 a a a~ |
  a a4 a8 |

  % 85
  af8 af af af~ |
  af f4 af8 |
  af8 af af bf~ |
  bf bf4 af8 |

  % 89
  af8 s af ef~ |
  ef gf af4 |
  a8 a4 a8~ |
  a4 a |

  % 93
  af8 a af bf~ |
  bf f g4 |
  ef8 ef8 f8 ef8 |
  f8 ef8~ ef4 |

  % 97 - Tag
  af8\p s af ef~ |
  ef gf af4 |
  a8 a4 s8 |
  s4 r8 a8~ |

  % 101
  a2~ |
  a4\fermata ef4 |
  f8 f d' bf~ |
  bf f g4 |

  % 105
  e8 fs gs ds'~ |
  ds b cs4 |
  af8 a d bf~ |
  bf f g4 |

  % 109
  c4 r |
  df4 r |
  r8 cf4 bf8 |
  c4 r |

}
bariWords = \lyricmode {

  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _
  the hut was shut.
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _
  _ just for two.
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ I wan -- na go there.
  _ _ _ _ _ _ _ go,  __
  _

}

bassMusic = \relative c {
  % dynamic marking is only for Midi track and should be removed before printing
  \set midiInstrument = #"clarinet"
  af8 af af af~ |
  af af af a |
  bf bf4 bf8 ~ |
  bf2|

  %  5
  af8 af af af~ |
  af af af a |
  bf bf4 bf8 ~ |
  bf2|

  %  9
  f'8 f f c~ |
  c c f f |
  bf, bf4 bf8~ |
  bf4. f'8 |

  % 13
  f8 f4 bf,8~ |
  bf4 f'8 bf,
  ef ef ef bf~ |
  bf2 |

  % 17
  af8 af af af~ |
  af af af a |
  bf bf4 ef8 ~ |
  ef2|

  % 21
  af8 af af ef~ |
  ef ef ef4 |
  df2( |
  c) |

  % 25
  f8 f bf, f'~ |
  f f f4 |
  ef8 ef ef ef~ |
  ef ef e4 |

  % 29
  f8 e f bf,~ |
  bf bf bf4 |
  d8\rest ef8 f8\noBeam ff8 |
  ef2 |

  % 33
  af,8 af af af~ |
  af af af a |
  bf bf4 bf8 ~ |
  bf2|

  % 37
  af8 af af af~ |
  af af af a |
  bf bf4 bf8 ~ |
  bf2|

  % 41
  f'8 f f c~ |
  c c f f |
  bf, bf4 bf8~ |
  bf4. f'8 |

  % 45
  f8 f4 bf,8~ |
  bf4 f'8 bf,
  ef ef ef bf~ |
  bf2 |


  % 49
  af8 af af af~ |
  \textSpannerDown
  af af af_\markup \sans "Bass Melody" c |
  ef_\startTextSpan d4 df8~ |
  df2 |

  % 53
  ef8 f ef c~ |
  c bf af4 |
  bf2~ |
  bf_\stopTextSpan

  % 57
  f'8 f bf, f'~ |
  f f f4 |
  ef8 ef ef ef~ |
  ef ef e4 |

  % 61
  f8 e f bf,~ |
  bf bf bf4 |
  ef8 f4 ff8 |
  ef2 |

  % 65
  af,8 d8\rest af af~ |
  af ef' af,4 |
  c8 c4 f8~ |
  f4 f |

  % 69
  f8 f f bf,~ |
  bf bf bf4 |
  f'2~ |
  f4 f8 bf, |

  % 73
  bf?8 ef bf bf~ |
  bf bf4 bf8 |
  ef ef bf bf~ |
  bf4 bf8 ef |

  % 77
  af,4 ef'8 af,~ |
  af8 ef' af, af |
  bf d bf bf~ |
  bf4 bf8 bf |

  % 81
  f'8 f c fs~ |
  fs8 f4 c8 |
  f f c fs~ |
  fs8 f4 f8 |

  % 85
  f8 f bf, f'~ |
  f8 bf,4 bf8 |
  f' f bf, f'~ |
  f8 ef4 b8 |

  % 89
  af8 d8\rest af af~ |
  af ef' af,4 |
  c8 c4 c8~ |
  c4 f |


  % 93
  bf,8 c bf f'~ |
  f8 bf, ef4 |
  af,2~ |
  af2 |

  % 97 - Tag
  af8 d8\rest af af~ |
  af ef' af,4 |
  c8 c4 d8\rest |
  d4\rest r4 |

  % 101
  r4 c4~ |
  c4 f,4 |
  bf8 c bf f'~ |
  f8 bf, ef4|

  % 105
  cs8 ds cs cs~ |
  cs8 cs fs4|
  bf,8 c bf f'~ |
  f8 bf, ef4|

  % 109
  af4 af |
  af4 af |
  r8 ff4 gf8 |
  af4 r4 |


}
bassWords = \lyricmode {
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _
  the hut was shut.
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _
  _ just for two.
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _
  _ _ _ go, _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  night, _ night.
}


%{
convert-ly (GNU LilyPond) 2.18.0  convert-ly: Processing `'...
Applying conversion: 2.15.7, 2.15.9, 2.15.10, 2.15.16, 2.15.17,
2.15.18, 2.15.19, 2.15.20, 2.15.25, 2.15.32, 2.15.39, 2.15.40,
2.15.42, 2.15.43, 2.16.0, 2.17.0, 2.17.4, 2.17.5, 2.17.6, 2.17.11,
2.17.14, 2.17.15, 2.17.18, 2.17.19, 2.17.20, 2.17.25, 2.17.27,
2.17.29, 2.17.97
%}


%{
convert-ly (GNU LilyPond) 2.18.0  convert-ly: Processing `'...
Applying conversion:     The document has not been changed.
%}
