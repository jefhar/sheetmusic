%{ SVN FILE: $Id$
  * vim: set expandtab tabstop=4 softtabstop=4 shiftwidth=4: *
  @author $Author$
  @version $Revision$
  @lastrevision $Date$
  @filesource $URL$

  Theme, Variations, and Invention in F Major: Invention
%}

\version "2.17.97"

\include "english.ly"
\include "articulate.ly"

inventionglobal = {
  \key f \major
  \time 4/4
  % \autoBeamOff
  \set Score.tempoHideNote = ##t
  \tempo 4=120
  % \set Staff.midiInstrument = #"trombone"
}

inventionfirst = \relative c' {
  f4 g8( e f g a bf) |
  c4 bf a8 g f4 |
  f'8-. a,-. c-. g-. a-. c,-. f c' |
  bf8-. e,-. g-. e-. f-. c'-. a f |
  f4 g8( e-> f4->) bf8( g |
  c4->) d8( bf ef4->) f8( d |
  c8) d e f d e f g |
  af,8 bf c d ef d c bf |
  g8 bf ef, g bf af df16 c bf af |
  g8 af8 bf e d ef ef gf |
  df16 f ef f df f ef f af,8 f d b' |
  c4 r4 r2 |
  r1 |
  c,4 d8( b c d e f) |
  g4 f e8 d c4 |
  c'8-. e,-. g-. c,-. e-. g,-. c-. e-. |
  f8 b, e b c g' e c |
  c16 d e f d e f g e f g a f g a b |
  c8 d ef4 f bf, |
  c bf8( df c bf af g) |
  f4 g af8 bf c4 |
  c,8 af' f bf af f' c f, |
  f8 df' bf f' c f, af c |
  bf16 df e bf' df, bf' e, bf' df, bf' e, bf' df, bf' e, bf' |
  c8 bf af g af g f ef |
  df8 c bf af bf c bf4 |
  df16 f ef f df f ef f e g f g e g f g |
  f16 a g a f a g a df,8 bf f e' |
  f4 g <a, c f>2
  \bar "|."
}

inventionsecond = \relative c {
  r1 |
  r1 |
  f4 g8( e f g a bf) |
  c4 bf a8 g f4 |
  f'8-. a,-. c-. g-. a-. c,-. f-. c'-. |
  bf-. e,-. g-. e-. f-. c'-. a-. f-. |
  f4-> g8 d bf c d e |
  c'8-> d ef f c bf af d |
  ef4 g,8 bf ef, f bf ef |
  df8 c g af bf c c af |
  f16 af gf af f af g af f8 d g16-. g,-. g'-. g,-. |
  c4 d8( b c d e f) |
  g4 f e8 d c4 |
  c'8-. e,-. g-. d-. e-. g,-. c-. g'-. |
  f8-. b,-. d-. b-. c-. g'-. e-. c-. |
  a'8 c, e a, c e, a c |
  d16 g g, d' c g' g, d' c e e b' c g a e |
  a,8 c16 d f8 a16 b c,8 e16 f a8 c16 g |
  af16 f g b, c8 g af df, gf df' |
  c8 ef df f e df c bf |
  af8 df bf ef c d ef af, |
  c4 bf8( df c bf af g) |
  f4 g af8 bf c4 |
  \pitchedTrill g'1\startTrillSpan af |
  af8\stopTrillSpan g f ef c bf af a |
  bf8 af df c df ef f ef |
  f16 af gf af f af gf af c, ef d ef c ef d ef |
  af,16 c bf c af c bf c bf8 g c16 c, c' c, |
  df4 c f2 |



}

%{
convert-ly (GNU LilyPond) 2.18.0  convert-ly: Processing `'...
Applying conversion: 2.15.7, 2.15.9, 2.15.10, 2.15.16, 2.15.17,
2.15.18, 2.15.19, 2.15.20, 2.15.25, 2.15.32, 2.15.39, 2.15.40,
2.15.42, 2.15.43, 2.16.0, 2.17.0, 2.17.4, 2.17.5, 2.17.6, 2.17.11,
2.17.14, 2.17.15, 2.17.18, 2.17.19, 2.17.20, 2.17.25, 2.17.27,
2.17.29, 2.17.97
%}
