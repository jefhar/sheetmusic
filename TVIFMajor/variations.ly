%{ SVN FILE: $Id$
  * vim: set expandtab tabstop=4 softtabstop=4 shiftwidth=4: *
  @author $Author$
  @version $Revision$
  @lastrevision $Date$
  @filesource $URL$

  Theme, Variations, and Invention in F Major: Variations
%}

\version "2.17.97"

\include "english.ly"
\include "articulate.ly"

variationsglobal = {
  \key f \major
  \time 3/4
  % \autoBeamOff
  % \set Score.tempoHideNote = ##t
}

variationOneUpper = \relative c'' {
  \tempo 4=96
  \partial 4 c4 |
  a4 d c |
  a4 f e |
  g4 f e |
  f2 \bar "||" e4 |
  f2 g8 e |
  f4 a c |
  bf4 a g |
  f2 \bar "|."
}

variationOneLower = \relative c {
  \partial 4 e4 |
  f2 g8 e |
  f4 a c |
  bf4 a g |
  f2 \bar "||" c'4 |
  a4 bf~ bf8 g |
  a8 bf c d e f |
  g4 f e |
  f2_\markup {\italic attacca}
}

variationTwoUpper = \relative c' {
  \partial 4 e4 |
  f4 g e |
  f4 a4 c
  bf4 a g
  f2 \bar "||" e'8 d |
  d8 c e f g f |
  a,8 bf c d e f |
  g8 f~ f e bf4 |
  a2 \bar "||" bf8 a
  a8 g bf8 c d c16 d |
  e8 f g a bf c |
  d8 c~ c bf c,4 |
  f2 \bar "|."
}

variationTwoLower = \relative c {
  \partial 4 e8 d |
  d8 c e f g f |
  a,8 bf c d e f |
  g8 f f e c e |
  d2 \clef "treble" e'4 |
  f4 g e |
  f a c |
  bf4 a g |
  f2 e4 |
  f4 g e |
  f a c |
  bf4 a g |
  f2
}

variationThreeUpperOne = \relative c' {
  \tempo 4=90
  \partial 4 e4
  f2 g8 e |
  f4 a c |
  bf4 a g |
  f2_\markup {\italic attacca} \bar "|."
}

variationThreeUpperTwo = \relative c' {
  \partial 4 c4 |
  c4 d c |
  c2 e4 |
  d4 c~ c8. bf16 |
  a2
}

variationThreeLowerOne = \relative c' {
  \partial 4 g4 |
  a4 bf g |
  a2 g4 |
  f4 f~ f8 e |
  f2
}

variationThreeLowerTwo = \relative c {
  \partial 4 c4 |
  f,4 bf c |
  f,2 c'4 |
  bf4 c2 |
  f,2
}

variationFourUpperOne = \relative c' {
  \partial 4 e4
  f2 g8 e |
  f4 a c |
  bf4 a g |
  f2 \bar "|."
}

variationFourUpperTwo = \relative c' {
  \partial 4 e4 |
  df4 c2 |
  ef4 c2 |
  df2 e4 |
  f2
}

variationFourLowerOne = \relative c' {
  \partial 4 g4 |
  af4 g2 |
  g4 e a |
  f8 g e4 c'
  af2
}

variationFourLowerTwo = \relative c {
  \partial 4 c4 |
  f4 c2 |
  bf4 a f |
  bf4 g c |
  df2
}


%{
convert-ly (GNU LilyPond) 2.18.0  convert-ly: Processing `'...
Applying conversion: 2.15.7, 2.15.9, 2.15.10, 2.15.16, 2.15.17,
2.15.18, 2.15.19, 2.15.20, 2.15.25, 2.15.32, 2.15.39, 2.15.40,
2.15.42, 2.15.43, 2.16.0, 2.17.0, 2.17.4, 2.17.5, 2.17.6, 2.17.11,
2.17.14, 2.17.15, 2.17.18, 2.17.19, 2.17.20, 2.17.25, 2.17.27,
2.17.29, 2.17.97
%}
