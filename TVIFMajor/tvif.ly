%{ SVN FILE: $Id$
  * vim: set expandtab tabstop=4 softtabstop=4 shiftwidth=4: *
  @author $Author$
  @version $Revision$
  @lastrevision $Date$
  @filesource $URL$

  Theme, Variations, and Invention in F Major
%}

\version "2.17.97"

\include "../letterpaper.ly"

\include "../TVIFMajor/theme.ly"
\include "../TVIFMajor/variations.ly"
\include "../TVIFMajor/invention.ly"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%       STOP EDITING HERE.    STOP EDITING HERE.       %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% showLastLength = R2*48
\bookpart {
  \header {
    piece = "Theme"
  }
  \score{
    %\unfoldRepeats \articulate <<
    \new PianoStaff <<
      % \set PianoStaff.instrumentName = #"Piano "
      \new Staff = "upper" <<
        \clef "treble"
        \themeglobal
        \themefirst
      >>
      \new Staff = "lower" <<
        \clef "bass"
        \themeglobal
        \themesecond
      >>
    >>
    \layout { }
    \midi { }
  }
}

\bookpart {
  \header {
    piece = "Variation One"
  }
  \score{
    %\unfoldRepeats \articulate <<
    \new PianoStaff <<
      % \set PianoStaff.instrumentName = #"Piano "
      \new Staff = "upper" <<
        \clef "treble"
        \variationsglobal
        \variationOneUpper
      >>
      \new Staff = "lower" <<
        \clef "bass"
        \variationsglobal
        \variationOneLower
      >>
    >>
    \layout { }
    \midi { }
  }
}

\bookpart {
  \header {
    piece = "Variation Two"
  }
  \score{
    %\unfoldRepeats \articulate <<
    \new PianoStaff <<
      % \set PianoStaff.instrumentName = #"Piano "
      \new Staff = "upper" <<
        \clef "treble"
        \variationsglobal
        \variationTwoUpper
      >>
      \new Staff = "lower" <<
        \clef "bass"
        \variationsglobal
        \variationTwoLower
      >>
    >>
    \layout { }
    \midi { }
  }
}

\bookpart {
  \header {
    piece = "Variation Three"
  }
  \score{
    %\unfoldRepeats \articulate <<
    \new PianoStaff <<
      % \set PianoStaff.instrumentName = #"Piano "
      \new Staff = "threeupper" <<
        \clef "treble"
        \new Voice = "varthreea" {
          \voiceOne <<
            \variationsglobal
            \variationThreeUpperOne
          >>
        }
        \new Voice = "varthreeb" {
          \voiceTwo <<
            \variationsglobal
            \variationThreeUpperTwo
          >>
        }

      >>
      \new Staff = "threelower" <<
        \clef "bass"
        \new Voice = "varthreec" {
          \voiceOne <<
            \variationsglobal
            \variationThreeLowerOne
          >>
        }
        \new Voice = "varthreed" {
          \voiceTwo <<
            \variationsglobal
            \variationThreeLowerTwo
          >>
        }

      >>
    >>
    \layout { }
    \midi { }
  }
}

\bookpart {
  \header {
    piece = "Variation Four"
  }
  \score{
    %\unfoldRepeats \articulate <<
    \new PianoStaff <<
      % \set PianoStaff.instrumentName = #"Piano "
      \new Staff = "fourupper" <<
        \clef "treble"
        \new Voice = "varfoura" {
          \voiceOne <<
            \variationsglobal
            \variationFourUpperOne
          >>
        }
        \new Voice = "varfourb" {
          \voiceTwo <<
            \variationsglobal
            \variationFourUpperTwo
          >>
        }

      >>
      \new Staff = "fourlower" <<
        \clef "bass"
        \new Voice = "varfourc" {
          \voiceOne <<
            \variationsglobal
            \variationFourLowerOne
          >>
        }
        \new Voice = "varfourd" {
          \voiceTwo <<
            \variationsglobal
            \variationFourLowerTwo
          >>
        }

    >>  >>
    \layout { }
    \midi { }
  }
}

\bookpart {
  \header {
    piece = "Invention"
  }
  \score{
    %\unfoldRepeats \articulate <<
    \new PianoStaff <<
      % \set PianoStaff.instrumentName = #"Piano "
      \new Staff = "upper" <<
        \clef "treble"
        \inventionglobal
        \inventionfirst
      >>
      \new Staff = "lower" <<
        \clef "bass"
        \inventionglobal
        \inventionsecond
      >>
    >>
    \layout { }
    \midi { }
  }
}

%{
convert-ly (GNU LilyPond) 2.18.0  convert-ly: Processing `'...
Applying conversion: 2.15.7, 2.15.9, 2.15.10, 2.15.16, 2.15.17,
2.15.18, 2.15.19, 2.15.20, 2.15.25, 2.15.32, 2.15.39, 2.15.40,
2.15.42, 2.15.43, 2.16.0, 2.17.0, 2.17.4, 2.17.5, 2.17.6, 2.17.11,
2.17.14, 2.17.15, 2.17.18, 2.17.19, 2.17.20, 2.17.25, 2.17.27,
2.17.29, 2.17.97
%}
