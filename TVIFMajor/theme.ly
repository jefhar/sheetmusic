%{ SVN FILE: $Id$
  * vim: set expandtab tabstop=4 softtabstop=4 shiftwidth=4: *
  @author $Author$
  @version $Revision$
  @lastrevision $Date$
  @filesource $URL$

  Theme, Variations, and Invention in F Major: Theme
%}

\version "2.17.97"

\header {
  % http://lilypond.org/doc/v2.12/Documentation/user/lilypond/Creating-titles
  title = "Theme, Variations and Invention in F"
  subtitle = "(1989)"
  composer = "Jeffrey Harris"
  % poet = "Lyrics by"
  % arranger = "Arranged by"
  copyright = \markup \smaller \column {"© 1989, 2012 Jeff Harris, Barbershop Bass Publishing, ASCAP. All Rights Reserved. No recording use, public performance" "for profit use or any other use requiring authorization, or reproduction or sale of copies in any form shall be made of or from this" "Arrangement unless licensed by the copyright owner or an agent or organization acting on behalf of the copyright owner." }
  tagline = \markup \column {"$Id$" }
}

\include "english.ly"
\include "articulate.ly"

themeglobal = {
  \key f \major
  \time 3/4
  % \autoBeamOff
  % \set Score.tempoHideNote = ##t
}

themefirst = \relative c' {
  \tempo 4=120
  \partial 4 e4 |
  f2 g8 e |
  f4 a c |
  bf4 a g |
  f2 \bar "|."
}


themesecond = \relative c' {
  \partial 4 c4 |
  d4 a b8 c |
  a4 c e |
  d f, c8 c, |
  f2
}


%{
convert-ly (GNU LilyPond) 2.18.0  convert-ly: Processing `'...
Applying conversion: 2.15.7, 2.15.9, 2.15.10, 2.15.16, 2.15.17,
2.15.18, 2.15.19, 2.15.20, 2.15.25, 2.15.32, 2.15.39, 2.15.40,
2.15.42, 2.15.43, 2.16.0, 2.17.0, 2.17.4, 2.17.5, 2.17.6, 2.17.11,
2.17.14, 2.17.15, 2.17.18, 2.17.19, 2.17.20, 2.17.25, 2.17.27,
2.17.29, 2.17.97
%}
