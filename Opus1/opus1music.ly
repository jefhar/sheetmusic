%{ SVN FILE: $Id$
  * vim: set expandtab tabstop=4 softtabstop=4 shiftwidth=4: *
  @author $Author$
  @version $Revision$
  @lastrevision $Date$
  @filesource $URL$

  Opus 1
%}

\version "2.17.97"

\include "english.ly"
\include "articulate.ly"

\header {
  % http://lilypond.org/doc/v2.12/Documentation/user/lilypond/Creating-titles
  title = "Opus 1"
  subtitle = "(1986)"
  composer = "Jeffrey Harris"
  % poet = "Lyrics by"
  % arranger = "Arranged by"
  % subsubtitle = "Rondo"
  copyright = \markup \smaller \column {"© 1986, 2012 Jeff Harris, Barbershop Bass Publishing, ASCAP. All Rights Reserved. No recording use, public performance" "for profit use or any other use requiring authorization, or reproduction or sale of copies in any form shall be made of or from this" "Arrangement unless licensed by the copyright owner or an agent or organization acting on behalf of the copyright owner." }
  tagline = \markup \column {"$Id$" }
  opus = "Opus 1"
}

global = {
  \key f \major
  \time 4/4
  % \autoBeamOff
  \set Score.tempoHideNote = ##t
  \tempo 4=80
  \set Staff.midiInstrument = #"acoustic grand"
}

opustop = \relative c' {
  \tuplet 3/2 {c4( f a)} e2 |
  c8( a) d2 a'4 |
  g2 bf4 d |
  e,2  c8( a) d4~ |
  d <f d'>8 <e g>4. a8 bf |
  d,8 g8~ g d f4 c |
  g'8 e d' b c a d,4 |
  c8( a) d2 a4 |
  g'4 f2 d4 |
  bf4 c2 c'8 f, |
  g4. a8 f4 c |
  g'4. a8 f2~ |
  f1\fermata
}

opusbottom = \relative c {
  \tuplet 3/2 { <c f>4 <f, c'> <f c'>} <c' g'>2 |
  <a c>4 <bf f'>2 <c a'>4 |
  <c g'>2 << {f8[ e]} \\ {bf4} >> d4 |
  <c g'>2 <a c>4 <bf~ f'~>4 |
  <bf f'>4 bf8 c4. <d d'>4 |
  <d g>2 <c f>4 <c g> |
  <c e g>4 <b d f> <f' c'> <c g'> |
  <a c> <bf f'>2 <a c>4 |
  <d b'>4 <d a'>2 <d a'>4 |
  <d f>4 <c f>2 <c f>4 |
  <c g'>4. <c f>8 <c a'>4 <e g> |
  <c g'>4. <c f>8 <f~ a~>4 <c~ f~ a~> |
  <f, c' f a>1\fermata

}

%{
convert-ly (GNU LilyPond) 2.18.0  convert-ly: Processing `'...
Applying conversion: 2.15.7, 2.15.9, 2.15.10, 2.15.16, 2.15.17,
2.15.18, 2.15.19, 2.15.20, 2.15.25, 2.15.32, 2.15.39, 2.15.40,
2.15.42, 2.15.43, 2.16.0, 2.17.0, 2.17.4, 2.17.5, 2.17.6, 2.17.11,
2.17.14, 2.17.15, 2.17.18, 2.17.19, 2.17.20, 2.17.25, 2.17.27,
2.17.29, 2.17.97
%}
