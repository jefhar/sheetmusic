%{ SVN FILE: $Id$
  @author $Author$
  @version $Revision$
  @lastrevision $Date$
  @filesource $URL$

  Sonata #2 in A
%}

\version "2.17.97"

\header {
  % http://lilypond.org/doc/v2.12/Documentation/user/lilypond/Creating-titles
  title = "Sonata No. 2 in A"
  subtitle = "Income Tax Sonatina"
  subsubtitle = "(1991)"
  composer = "Jeffrey Harris"
  % poet = "Lyrics by"
  % arranger = "Arranged by"
  copyright = \markup \smaller \column {"© 1991, 2012 Jeff Harris, Barbershop Bass Publishing, ASCAP. All Rights Reserved. No recording use, public performance" "for profit use or any other use requiring authorization, or reproduction or sale of copies in any form shall be made of or from this" "Arrangement unless licensed by the copyright owner or an agent or organization acting on behalf of the copyright owner." }
  tagline = \markup \column {"$Id$" }
}

\include "english.ly"
\include "articulate.ly"

global = {
  \key a \major
  \time 4/4
  % \autoBeamOff
  \set Score.tempoHideNote = ##t
  \tempo 4=144
  % \set Staff.midiInstrument = #"trombone"
}

voicefirst = \relative c'' {
  \repeat volta 2 {
    a4 cs d e |
    gs,2 fs4 e |
    a4 gs fs8 cs' cs4~ |
    cs4 gs a2 |
    e4 gs2 e4 |
    a2 cs4 e |
    fs4 d a fs |
    e4 gs <cs, a'>2
    a'4 b cs d |
    b,4 cs ds <cs e> |
    e8 fs a b a gs e cs |
    b8 gs' fs e cs b a ds |
    e8 gs b d e ds gs, b |
    e,8 gs a b ds,4 cs8 b |
    e8 gs a b cs ds e cs~ |
    cs4 a8 fs ds b cs fs |
    e8 gs b e b gs e4 | % repeat back to top
  } \repeat volta 2 {
    fs4 a b cs |
    es,2 d4 cs |
    fs8 d' cs es, fs4 gs |
    b4 gs8 as8~ as4 <cs, g'> |
    gs'?4 b cs d |
    fs,2 e4 d |
    b'4 d e fs |
    a,2 gs4 fs8 fs' |
    e8 cs b a b cs e gs |
    a8 cs, d e gs a b f |
    e8 cs a gs e f cs' a |
    e'8 cs b a f'4 gs8 a |
    bs8 gss es4 fss4 gss8 fs |
    <es gs>4 <as,~ fs'>8 <as es'> ds4 <d, b'> |
    a'4 cs d e |
    gs,2 fs4 e |
    a4 gs fs8 d' cs4~ |
    cs4 gs a2 |
    e4 gs2 e4 |
    a2 cs4 e |
    fs4 d a fs |
    e4 gs <cs, a'>2 |
    a'4 b cs d |
    e4 d cs b |
    a8 cs d e d cs a fs |
    e8 cs' b a fs e d gs |
    a8 cs e fs a gs cs, fs |
    a,8 cs d e gs,4 fs8 e |
    a8 cs d e fs gs a fs~ |
    fs4 d8 b gs d fs b |
    a8 cs e a e cs a4 \bar "|."
  }
}

voicesecond = \relative c {
  a8 e' cs e a, e' cs e |
  b8 e gs, e' b e gs, e' |
  a,8 e' cs e d fs e4 |
  e,4 b' a2 |
  e'2 e, |
  r4 a2 cs,4 |
  d1 |
  cs4 e a2 |
  <gs' cs>2 <cs, e>2 |
  <fs a>2 <b, a'>4\staccato <b a'>\staccato |
  <e gs b>1 |
  <b ds fs>1 |
  <e gs b>2 <b ds fs> |
  <e gs b>2 <b ds fs> |
  <a cs e>1 |
  <fs a cs>2 <b ds fs> |
  cs8 e gs cs <b, ds a'>4 e |
  fs1 |
  <cs gs'>1 |
  d4 <cs gs'>4 d e?8 cs~ |
  cs8 fs8 fs2 <bf, e>4 |
  gs8 d' b d gs, d' b d |
  a8 d fs, d' a d fs, d' |
  b8 d a d b d gs, d' |
  a8 e' cs e a, e' b d |
  e4 g a cs, |
  a4 cs e g |
  b,4 d f a |
  cs,4 e df cs |
  es4 as, ds gs, |
  cs4 fs, b e |
  a,8 e' cs e a, e' cs e |
  b8 e gs, e' b e gs, e' |
  a,8 e' cs e cs e e4 |
  e,4 b' a2 |
  e'2 e, |
  r4 a2 cs,4 |
  d1 |
  cs4 e a2 |
  <gs' cs>2 <cs, e> |
  <b d>2 <e gs> |
  <a, cs e>1 |
  <e' gs b>1 |
  <a, cs e>2 <e' gs b> |
  <a, cs e>2 <e' gs b> |
  <d fs a>2 s2 |
  <b d fs>2 <e gs b>
  a,2 <e' gs d'>4 a, |
}


%{
convert-ly (GNU LilyPond) 2.18.0  convert-ly: Processing `'...
Applying conversion: 2.15.7, 2.15.9, 2.15.10, 2.15.16, 2.15.17,
2.15.18, 2.15.19, 2.15.20, 2.15.25, 2.15.32, 2.15.39, 2.15.40,
2.15.42, 2.15.43, 2.16.0, 2.17.0, 2.17.4, 2.17.5, 2.17.6, 2.17.11,
2.17.14, 2.17.15, 2.17.18, 2.17.19, 2.17.20, 2.17.25, 2.17.27,
2.17.29, 2.17.97
%}
