%{ SVN FILE: $Id$
   @author $Author$
   @version $Revision$
   @lastrevision $Date$
   @filesource $URL$


   Arranged for Barbershop Quartet by Kevin Keller.
%}

\version "2.18.2"
\language "english"

\header {
  % http://lilypond.org/doc/v2.12/Documentation/user/lilypond/Creating-titles
  title = "Along the Navaho Trail"
%  subtitle = "(1916)"
%  poet = "GRANT CLARKE."
%  composer = "FRED FISCHER."
%  lastupdated = "11/9/2012"
%  arranger = "Arranged by JEFF HARRIS."
%  copyright = \markup \smaller \column {"Arrangement © 2013 Jeff Harris, Barbershop Bass Publishing, ASCAP. All Rights Reserved. No recording use, public performance" "for profit use or any other use requiring authorization, or reproduction or sale of copies in any form shall be made of or from this" "Arrangement unless licensed by the copyright owner or an agent or organization acting on behalf of the copyright owner." }
%  tagline = \markup \column {"Questions about the contest suitability of this song/arrangement should be directed to the judging community" "and measured against current contest rules. Ask before you sing." "$Id$" }
}

\include "english.ly"

global = {
  \key d \major
  \time 4/4
%  \autoBeamOff
  \set Score.tempoHideNote = ##t
  \tempo 4=72
  \override Score.BarNumber.break-visibility = ##(#t #t #t)
  % \override SpacingSpanner.spacing-increment = #8.0
  

}

xVoice = \markup { \smaller \sans { x } } % print this by using '\xVoice'

tenorMusic = \relative c' {
  % dynamic marking is only for Midi track and should be removed before printing
  % \set midiInstrument = #"clarinet"
  \set midiInstrument = #"trombone"
  % 1 for free:
  r2. fs4  |
  
  % Intro
  r2. g8. \ppp^\markup { "slow rock" } g16
  
  % First Time
  fs2~ fs8 d \tuplet 3/2 {d8 cs cs } |
  d8 fs~ fs2 g8. g16 |
  fs8 fs~ fs4. d8 \tuplet 3/2 {d8 cs cs }
  c4 d2 d4 |
  d2~  \tuplet 3/2 {d8 f e d e f} |
  f8 f~ f2 f8 e |
  
  % first Ending
  d2~ d8. d16 \tuplet 3/2 {d8 es es }
  fs4~ fs8 g4. g8. g16 |
  
  % Second Time
  fs2~ fs8 d \tuplet 3/2 {d8 cs cs } |
  d8 fs~ fs2 g8. g16 |
  fs8 fs~ fs4. d8 \tuplet 3/2 {d8 cs cs }
  c4 d2 d4 |
  d2~  \tuplet 3/2 {d8 f e d e f} |
  f8 f~ f2 f8 e |
  
  % Second Ending
  d2~ d8. d16 \tuplet 3/2 {d8 es es } |
  fs2. r8 fs |
  
  g8. g16 g8. g16 e8. e16 e8. e16 |
  fs4 fs2 fs8. fs16 |
  g8. g16 \tuplet 3/2 {g8 g g } e8 e4 e8 |
  
  fs2. r8 es8 |
  fs8. fs16 fs8. fs16 es8. es16 es8. es16 |
  fs8 ef~ ef2 r8 ef8 |
  d8. d16 e8. f16 d8. d16 e8. f16 |
  cs2 r8 g'8 \tuplet 3/2 {g8 g g} |
  fs2~ fs8 d \tuplet 3/2 {d cs cs} |
  d8 fs~ fs2 g8. g16 |
  fs8 fs~ fs4. d8 \tuplet 3/2 {d cs cs } |
  c4 d2 d4 |
  d2~ \tuplet 3/2 {d8 f e d e f} |
  f8 f~ f2~ f8 e | 
  d2~ d8. d16 \tuplet 3/2 {d8 es es } |
  fs4 d2 d4 |
  d2~ d8. d16 \tuplet 3/2 {d8 es es } |
  fs4 d2 d4 |
  
  % tag
  d2 fs |
  g4~ g4.~ g2 r2 g8 \tuplet 3/2 {g8 g g } |
  fs4. g8 a g4. |
  b1~ b2~ b2 |
  d1 |
  
  
}
tenorWords = \lyricmode {
 
}

leadMusic = \relative c' {
  % dynamic marking is only for Midi track and should be removed before printing
  \set midiInstrument = #"trombone"
  % \set midiInstrument = #"clarinet"
  % 1 for free:
  d1 |
  
  % Intro
  r2. a8.\ppp b16 |
  \set Score.currentBarNumber = #1
  % First time
  d2~ d8 a \tuplet 3/2 { c b a} |
  fs8 a~ a2 a8. b16 |
  d8 d~ d4. a8 \tuplet 3/2 {c b a } |
  fs2. fs4 |
  f2~ \tuplet 3/2 {f8 d e f e d} |
  b8 d~ d2~ d8 e |
  
  % First Ending
  fs2~ fs8. a16 \tuplet 3/2 {f8 d b} |
  d4 a'8~ a4. a8. b16 |
  
  % Second Time
  d2~ d8 a \tuplet 3/2 { c b a} |
  fs8 a~ a2 a8. b16 |
  d8 d~ d4. a8 \tuplet 3/2 {c b a } |
  fs2. fs4 |
  f2~ \tuplet 3/2 {f8 d e f e d} |
  b8 d~ d2~ d8 e |
  
  % Second Ending
  f2~ f8. a16 \tuplet 3/2 {f8 d b} |
  d4 a'2 r8 a |

  \set Score.currentBarNumber = #9
  b8. b16 d8. d16 bf8. bf16 d8. d16 |
  a4 a2 a8. a16 |
  b8. b16 \tuplet 3/2 {b8 b b} bf8 bf4 bf8 |
  a2. r8 gs8 |
  a8. a16 c8. c16 gs8. gs16 a8. b16 |
  a8 fs8~ fs2 r8 fs |
  f8. f16 e8. d16 f8. f16 e8. d16 |
  e2 r8 a \tuplet 3/2 {a8 b a} |
  d2~ d8 a \tuplet 3/2 {c8 b a} |
  fs8 a~ a2 a8. b16 |
  d8 d~ d4. a8 \tuplet 3/2 {c b a }
  fs2. fs4 |
  f2~ \tuplet 3/2 {f8 d e f e d} |
  b8 d~ d2 d8 e |
  fs2~ fs8. a16 \tuplet 3/2 {f8 d b }
  d4 a'2 e4 |
  fs2~ fs8. a16 \tuplet 3/2 {f8 d b } |
  d4 a'2 e4 |
  
  % Tag
  fs2 a |
  a4 e4.~ e2 r2 a8 \tuplet 3/2 {b8 a b} |
  d1 |
  fs4. f8 e es4. |
  fs2 es |
  fs1 |
  
  
  \bar "|."

}
leadWords = \lyricmode {
  % Verse 1

}

bariMusic = \relative c' {
  % dynamic marking is only for Midi track and should be removed before printing
  % \set midiInstrument = #"clarinet"
  \set midiInstrument = #"trombone"
  % Verse 1
  % 1 for free:
  r2 a2 |
  % Intro
  r2. cs8.\ppp
   cs16 |

  % Start repeat
   a2 ~ a8 fs \tuplet 3/2 {af8 g fs} |
   a8 d~ d2 cs8. cs16 |
   a8 a~ a4. fs8 \tuplet 3/2 {af8 g fs} |
   a2 b4 c |
   b2~ \tuplet 3/2 {b8 b b b b b } |
   d8 b~ b2~ b8 b8 |
  
   % First Ending
   a2~ a8. fs16 \tuplet 3/2 {b8 b d} |
   a4 bs8 cs4. cs8. cs16 |
   
   % Second Time
   a2 ~ a8 fs \tuplet 3/2 {af8 g fs} |
   a8 d~ d2 cs8. cs16 |
   a8 a~ a4. fs8 \tuplet 3/2 {af8 g fs} |
   a2 b4 c |
   b2~ \tuplet 3/2 {b8 b b b b b } |
   d8 b~ b2~ b8 b8 |

   % Second Ending
   a2~ a8. fs16 \tuplet 3/2 {b8 b d } |
   a4 c?2 r8 c8 |
   
   % M9
   d8. d16 b8. b16 d8. d16 bf8. bf16 |
   d4 d2 c8. c16 |
   d8. d16 \tuplet 3/2 {d8 d d } d8 d4 d8 |
   
   % M12 (top page 2 in original score)
   d2. r8 b |
   cs8. cs16 a8. a16 b8. b16 cs8. gs16 |
   cs8 a8~ a2 r8 a8 |
   af8. af16 g8. af16 af8. af16 g8. af16 |
   g2 r8 cs8 \tuplet 3/2 {cs8 cs cs }
   a2~ a8 fs \tuplet 3/2 {af8 g fs }
   
   %m18
   a8 d8~ d2 cs8. cs16 |
   a8 a~ a4. fs8 \tuplet 3/2 {af8 g fs }
   a2 b4 c? |
   b2~ \tuplet 3/2 {b8 b b b b b} |
   d8 b~ b2~ b8 b8 |
   a2~ a8. fs16 \tuplet 3/2 {b8 b d}
   a4 fs2 bf4 |
   a2~ a8. fs16 \tuplet 3/2 {b8 b d }
   a4 fs2 bf4
   
   % Tag
   a2 bs cs4~ cs4.~ cs2 r2 cs8 \tuplet 3/2 {cs8 cs cs} |
   a4. b8 cs bf4. |
   d4. cs8 bs8 cs4. |
   d2~ d2 |
   a1 |
  
}
bariWords = \lyricmode {
}

bassMusic = \relative c {
  % dynamic marking is only for Midi track and should be removed before printing
   \set midiInstrument = #"clarinet"
  % \set midiInstrument = #"trombone"
  % Verse 1
  % 1 for free:
  r4 d2. |
  r2. e8.\fffff e16 |
  
  % First Time
  d2~ d8 d \tuplet 3/2 {bf8 a a }
  d8 d~ d2 e8. e16 |
  d8 d~ d4. d8 \tuplet 3/2 {bf8 a a } |
  d4 c b a |
  g2~ \tuplet 3/2 {g8 g g g g g }|
  g8 g~ g2 g8 g |
  
  % First Ending
  d'2~ d8. d16 \tuplet 3/2 {g,8 g g }
  d'4 ds8 e4. e8. e16|
  
    % Second Time
  d2~ d8 d \tuplet 3/2 {bf8 a a } |
  d8 d~ d2 e8. e16 |
  d8 d~ d4. d8 \tuplet 3/2 {bf8 a a }
  d4 c b a |
  g2~ \tuplet 3/2 {g8 g g g g g }
  g8 g~ g2 g8 g |
  
  % Second Ending
  d'2~ d8. d16 \tuplet 3/2 {g,8 g g} |
  d'2. r8 d8 |
  
  g8. g16 g8. g16 g8. g16 g8. g16 |
  d4 d2 d8. d16 |
  g8. g16 \tuplet 3/2 {g8 g g} c,8 c4 c8 |
  d2. r8 d |
  fs8. fs16 fs8. fs16 cs8. cs16 cs8. cs16 |
  fs8 c~ c2  r8 c |
  bf8. bf16 bf8. bf16 bf8. bf16 bf8. bf16 |
  a2 r8 e' \tuplet 3/2 {e8 e e } |
  d2~ d8 d \tuplet 3/2 {bf8 a a } |
  d8 d~ d2 e8. e16 |
  d8 d~ d4. d8 \tuplet 3/2 {bf8 a a } |
  d4 c b a |
  g2~ \tuplet 3/2 {g8 g g g g g } |
  g8 g~ g2 ~ g8 g |
  d'2~ d8. d16 \tuplet 3/2 {g,8 g g } |
  d'2. g,4 |
  d'2~ d8. d16 \tuplet 3/2 {g,8 g g } |
  d'2. g,4 |
  
  % Tag
  d'2 ds |
  e4 a,4.~ a2 r2 e'8 \tuplet 3/2 {e8 e e } |
  d4. e8 fs8 e4. |
  a4. af8 g gs4. |
  a2 g |
  d1 |
  
  
}

bassWords = \lyricmode {
}


\include "../Ring/Ring.ly"


%{
convert-ly (GNU LilyPond) 2.18.0  convert-ly: Processing `'...
Applying conversion: 2.17.0, 2.17.4, 2.17.5, 2.17.6, 2.17.11, 2.17.14,
2.17.15, 2.17.18, 2.17.19, 2.17.20, 2.17.25, 2.17.27, 2.17.29, 2.17.97
%}
