%{ SVN FILE: $Id$
  @author $Author$
  @version $Revision$
  @lastrevision $Date$
  @filesource $URL$

  Serco
%}

\version "2.17.97"

\header {
  % http://lilypond.org/doc/v2.12/Documentation/user/lilypond/Creating-titles
  title = "Serco"
  subtitle = "(Joke)"
  subsubtitle = "(1993)"
  composer = "Jeffrey Harris"
  % poet = "Lyrics by"
  % arranger = "Arranged by"
  copyright = \markup \smaller \column {"© 1993, 2012 Jeff Harris, Barbershop Bass Publishing, ASCAP. All Rights Reserved. No recording use, public performance" "for profit use or any other use requiring authorization, or reproduction or sale of copies in any form shall be made of or from this" "Arrangement unless licensed by the copyright owner or an agent or organization acting on behalf of the copyright owner." }
  tagline = \markup \column {"$Id$" }
}

\include "english.ly"

global = {
  \key c \major
  \time 3/4
  % \autoBeamOff
  \set Score.tempoHideNote = ##f
  \tempo 2.=76
  % \override DynamicTextSpanner.dash-period = #-1.0
}

flute = \relative c'' {
  \compressFullBarRests
  \override MultiMeasureRest.expand-limit = #3
  \set midiInstrument = #"flute"
  \repeat volta 4 {
}
  \alternative {
    {
      r2. |
      r2. | r2. }

  }
  \mark \markup { \musicglyph #"scripts.segno" }
  \repeat volta 2 {
    c2.\p |
    c2. |
    g4 a b~ |
    b c2 |
    d2. |
    c4 fs g |
    e2.~ |
    e2. |
    f2.~ |
    f2.~ |
    f2. |
    d'4 b gs |
    f2 b,4 |
    c fs b |
    e,2-> d4
  }

  \repeat volta 2 {
    e2->\cresc d4 |
    e2-> d4 |
    e'2-> d4 |
    e,2.~ |
    e4. d4.~ |
    d2.\cresc | \mark \markup { \musicglyph #"scripts.coda" }
    c2.\p |
    c2. |
    g4 a b~ |
    b c2 |
    d2 e4~ |
    e4 fs2 |
    g2 af4 |
    d4 ef c~ |
    c2.~ |
    c2 bf4 |
    b,4 f' gf |
    ef2 bf4 |
    a4 ds e |
    cs2 af4 |
    g4 cs d |
    b2 f'4~ |
    f2.~ |
    f2. |
    df'4 bf g |
    e2 bf4 |
    e4 fs2 |
    e4 fs2 |
  }

  g4\crescHairpin cs d\! |
  bf2. |
  b2. |
  f4 af d, |
  b4 gs' a |
  f2.

  \bar "||"
}

pianoupper = \relative c' {
  \set midiInstrument = #"acoustic grand"
  \repeat volta 4 {
    r2. |
    <c e g>4 r r |
 }
  \alternative {

    { <c e g>4 r r | }

  }
  \mark \markup { \musicglyph #"scripts.segno" }
  \repeat volta 2 {
    r4\p <c e g>4 <c e g>4 |
    r4 <c e g>4 <c e g>4 |
    r4 <c e g>4 <c e g>4 |
    r4 <d f g>4 <d f g>4 |
    r4 <d f g>4 <d f g>4 |
    r4 <c ef af>4 <c ef af>4 |
    r4 <cs e g bf>4 <cs e g bf>4 |
    r4 <cs e g bf>4 <cs e g bf>4 |
    r4 <cs es gs>4 <cs es gs>4 |
    r4 <cs es as>4 <cs es as>4 |
    r4 <es gs b>4 <es gs b>4 |
    r4 <es gs b>4 <es gs b>4 |
    r4 <f a c>4 <f a c>4 |
    r4 <f a c>4 <f a c>4 |
    r4 <f c>4 <f c> |
  }

}

pianolower = \relative c {
  \set midiInstrument = #"acoustic grand"
  \clef bass
  \repeat volta 4 {
    c4 r r |
    r2. |
  }
  \alternative {
    { r2. }

  }
  \mark \markup { \musicglyph #"scripts.segno" }

  \repeat volta 2 {
    c4\p r r |
    c4 r r |
    fs,4 r r |
    g4 r r |
    g4 r r |
    af4 r r |
    g4 r r |
    g4 r r |
    cs4 r r |
    cs4 r r |
    cs4 r r |
    d4 r r |
    c4 r r |
    g4 r r |
    g4 r r |
  }
  \repeat volta 2 {
    g4 r r|
    a4 r r |
    e'4 r r |
    g,4 g g |
    g4 g g |
    g4 g g | \mark \markup { \musicglyph #"scripts.coda" }
    c4\!\p r r |
    c4 r r |
    fs,4 r r |
    g4 r r |
    g4 r r |
    cs4 r r |
    ef4 r r |
    bf4 r r |
    f4 r r |
    c'4 r r |
    d4 r r |
    af4 r r |
    g4 r r |
    gf4 r r |
    f4 r r |
    f4 r r |
    f4 r r |
    f4 r r |
    fs4 r r |
    e4 r r |
    d4 r r |
    d4 r r |
  }
  g4 r r |
  fs4 r r |
  g4 r r |
  gs4 r r |
  a4 r r |
  e4 r r |
}

%{
convert-ly (GNU LilyPond) 2.18.0  convert-ly: Processing `'...
Applying conversion: 2.15.7, 2.15.9, 2.15.10, 2.15.16, 2.15.17,
2.15.18, 2.15.19, 2.15.20, 2.15.25, 2.15.32, 2.15.39, 2.15.40,
2.15.42, 2.15.43, 2.16.0, 2.17.0, 2.17.4, 2.17.5, 2.17.6, 2.17.11,
2.17.14, 2.17.15, 2.17.18, 2.17.19, 2.17.20, 2.17.25, 2.17.27,
2.17.29, 2.17.97
%}
