%{ SVN FILE: $Id$
  @author $Author$
  @version $Revision$
  @lastrevision $Date$
  @filesource $URL$

  Serco
%}

\version "2.17.97"

\include "../letterpaper.ly"

\include "../Serco/sercomusic.ly"
\include "articulate.ly"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%       STOP EDITING HERE.    STOP EDITING HERE.       %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% showLastLength = R2*48
\score{
  <<
    \new Staff {
      \set Staff.instrumentName = #"Flute"
      \set Staff.shortInstrumentName = #"Flt."
      \global
      \flute
    }
    \new PianoStaff <<
      \set PianoStaff.instrumentName = #"Piano"
      \set PianoStaff.shortInstrumentName = #"Pno."
      \new Staff = "upper" << \global \pianoupper >>
      \new Staff = "lower" << \global \pianolower >>
    >>
  >>
  \layout { }
}

\score{
  \unfoldRepeats \articulate
  <<
    \new Staff {
      \set Staff.midiInstrument = #"flute"
      \set Staff.instrumentName = #"Flute"
      \set Staff.shortInstrumentName = #"Fl."
      \global
      \flute
    }
    \new PianoStaff <<
      \set Staff.midiInstrument = #"acoustic grand"
      \set PianoStaff.instrumentName = #"Piano"
      \set PianoStaff.shortInstrumentName = #"Pft."
      \new Staff = "upper" << \global \pianoupper >>
      \new Staff = "lower" << \global \pianolower >>
    >>
  >>
  \midi { }
}

%{
convert-ly (GNU LilyPond) 2.18.0  convert-ly: Processing `'...
Applying conversion: 2.15.7, 2.15.9, 2.15.10, 2.15.16, 2.15.17,
2.15.18, 2.15.19, 2.15.20, 2.15.25, 2.15.32, 2.15.39, 2.15.40,
2.15.42, 2.15.43, 2.16.0, 2.17.0, 2.17.4, 2.17.5, 2.17.6, 2.17.11,
2.17.14, 2.17.15, 2.17.18, 2.17.19, 2.17.20, 2.17.25, 2.17.27,
2.17.29, 2.17.97
%}
