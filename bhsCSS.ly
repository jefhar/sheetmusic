\version "2.18.0"

bookTitleMarkup = \markup {
    \override #'(baseline-skip . 3.5)

    \column {
        \fill-line {
            \fromproperty #'header:dedication
        }
        \override #'(baseline-skip . 3.5)
        \column {
            \fill-line {
                \abs-fontsize #22 {
                    \override #'(font-name . "Arial Bold") {
                        \fromproperty #'header:title
                    }
                }
            }
            \fill-line {
                \abs-fontsize #12 {
                    \override #'(font-name . "Arial") {
                        \fromproperty #'header:subtitle
                    }
                }
            }
            \fill-line {
                \abs-fontsize #12 {
                    \override #'(font-name . "Arial") {
                        \fromproperty #'header:subsubtitle
                    }
                }
            }
            \fill-line {
                \abs-fontsize #12 {
                    \override #'(font-name . "Times") {
                        \fromproperty #'header:poet
                    }
                }
                {
                    \abs-fontsize #10 {
                        \override #'(font-name . "Times Italic") {
                            \italic \fromproperty #'header:instrument
                        }
                    }
                } \abs-fontsize #12 {
                    \override #'(font-name . "Times") {
                        \fromproperty #'header:composer
                    }
                }
            }
            \fill-line {
                \fromproperty #'header:meter
                \fromproperty #'header:arranger
            }
        }
    }
}

oddFooterMarkup = \markup {
    \column {
        \fill-line {
            \column {
                " "
                %% Copyright header field only on first page in each bookpart.
                \on-the-fly #part-first-page
                \abs-fontsize #9 {
                    \override #'(line-width . 110)
                    % \override #'(font-name . "Times")
                    \wordwrap-field #'header:copyright
                }
            }
        }
        \fill-line {
            %% Tagline header field only on last page in the book.
            \on-the-fly #last-page
            \column {
\override #'(thickness . 3)
                \draw-hline
                \italic \bold
                \abs-fontsize #18 {
                    %      \override #'(font-name . "Times")
                    "Performance Notes"
                }
                \abs-fontsize #9 {
                    \override #'(line-width . 110)
                    %    \override #'(font-name . "Times")
                    \wordwrap-field #'header:tagline
                }
                \vspace #1
                \abs-fontsize #9 {
                    "$Id: wonderfulgirlmusic.ly 163 2014-10-20 20:46:14Z jeff $"
                }
            }
        }
    }
}


oddHeaderMarkup = \markup

\fill-line {
    %% force the header to take some space, otherwise the
    %% page layout becomes a complete mess.
    " "
    \on-the-fly #not-part-first-page

    \abs-fontsize #9 {
        \override #'(font-name . "Times Italic")
        \fromproperty #'header:title
        \on-the-fly #print-page-number-check-first \italic \fromproperty #'page:page-number-string
    }
}

evenHeaderMarkup = \markup
\fill-line {
    \on-the-fly #print-page-number-check-first
    \abs-fontsize #9 {
        \override #'(font-name . "Times Italic")
        \fromproperty #'page:page-number-string
        \on-the-fly #not-part-first-page \italic \fromproperty #'header:title
        " "
    }
}

