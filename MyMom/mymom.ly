%* SVN FILE: $Id$ *%
%* vim: set expandtab tabstop=4 softtabstop=4 shiftwidth=4: *%
% @author $Author$
% @version $Revision$
% @lastrevision $Date$
% @filesource $URL$

% This is a simple Barbershop template for lilypond.
% If you need something slightly more complex, search for it on the internet.

\version "2.17.97"

\include "english.ly"
%\pointAndClickOff
% #(set-global-staff-size 17.82 )
%{  Note: You should always turn off point and click in any LilyPond files to be
    distributed to avoid including path information about your computer in the
    .pdf file, which can pose a security risk.
%}

\paper {
  #(set-paper-size "letter")
  bottom-margin   = 0.75\in
  top-margin      = 0.50\in
  left-margin     = 0.75\in
  line-width      = 7.00\in
  last-bottom-spacing = 0.25\in
}

%% One, One Two One, One Two Three Two One...
OneOneTwoGlobal = {
  \key c \major
  \autoBeamOff
  \set Score.tempoHideNote = ##f
  \tempo 4=144
  \set Staff.midiInstrument = #"trombone"
}

OneOneTwoMusic = \relative c' {
  \time 1/4 c4 |
  \time 2/4 c8 \( d c4 \) |
  \time 3/4 c8 \( d e d c4 \) |
  \time 4/4 c8 \( d e f e d c4 \) | 
  \time 5/4 c8 \( d e f g f e d c4 \) |
  \time 6/4 c8 \( d e f g a g f e d c4 \) |
  \time 7/4 c8 \( d e f g a b a g f e d c4 \) |
  \time 8/4 c8 \( d e f g a b c b a g f e d c4 \) | \bar "||"
  
  \time 1/4 c'4 |
  \time 2/4 c8 \( b c4 \) |
  \time 3/4 c8 \( b a b c4 \) |
  \time 4/4 c8 \( b a g a b c4 \) |
  \time 5/4 c8 \( b a g f g a b c4 \) |
  \time 6/4 c8 \( b a g f e f g a b c4 \) |
  \time 7/4 c8 \( b a g f e d e f g a b c4 \) |
  \time 8/4 c8 \( b a f f e d c d e f g a b c4 \) | \bar "|."
  
  
  
}

OneOneTwoWords = \lyricmode {
  One,
  One two one,
  One two three two one,
  One two three four three two one,
  One two three four five four three two one,
  One two three four five six five four three two one,
  One two three four five six sev'n six five four three two one,
  One two three four five six sev'n eight sev'n six five four three two one,
  Eight,
  Eight sev'n eight,
  Eight sev'n six sev'n eight,
  Eight sev'n six five six sev'n eight,
  Eight sev'n six five four five six sev'n eight,
  Eight sev'n six five four three four five six sev'n eight,
  Eight sev'n six five four three two three four five six sev'n eight,
  Eight sev'n six five four three two one two three four five six sev'n eight.
}


%% One Two Three Four Five
OneTwoGlobal = {
  \key c \major
  \time 4/4
  \autoBeamOff
  \set Score.tempoHideNote = ##F
  \tempo 4=144
  \set Staff.midiInstrument = #"trombone"
}

OneTwoMusic = \relative c' {
  c8\( d e f g2 |
  g8 f e d c2\) |
  c8\( g' f g e g d g |
  c, g' f g e d c4\) |
  c8\( g' f g e g d g |
  % c, g' f g <a f> <f' b, g d> <e c g c,>4~ | <e c>1 \) |
  c, g' f g a b c4~ |
  c1 \) |  \bar "|."
}

OneTwoWords = \lyricmode {
  One two three four five,
  Five four three two one,
  One five four five three five two five,
  One five four five three two one,
  One five four five three five two five,
  One five four five six sev'n eight. __
}

%% Cascade
Cascadeglobal = {
  \key c \major
  \time 4/4
  \autoBeamOff
  \set Score.tempoHideNote = ##t
  \tempo 4=96
  \set Staff.midiInstrument = #"trombone"
}

tenorCascadeMusic = \relative c' {
  c4 c4 c2 |
  c4 \( b a g 
  f e d c~ |
  c1\)\fermata \bar "||" |
  c'1\(~ | c1 ~ |
  c4 d e2\)\fermata \bar "||" |
  e4 e f f |
  e1\fermata \bar "||" |
  e4 e f f |
  e1\fermata
  
  \bar "|."
}

tenorCascadeWords = \lyricmode {
  _
}

leadCascadeMusic = \relative c' {
  c4 c4 c2 |
  c4 \( b a g f e d c~ |
  c1\) |
  c'1 ~ |
  c1 ~ | c1
  c4 c c b |
  c1 |
  g4 bf a g |
  g1 |
  
}

leadCascadeWords = \lyricmode {
  We, We, We, __
  We, __
  _ _ _ _ _ _ _ We, __
  We, We, We, We, We, __
  We, We, We, We, We, __
}

bariCascadeMusic = \relative c' {
  c4 c4 c2 |
  c4 \( b a g 
  f e d c~ |
  c1\) |
  c'4 \( b a g~ g1~ |
  g1 \) |
  g4 bf a g |
  g1 |
  
  c4 c c b |
  c1 |
  
}

bariCascadeWords = \lyricmode {
  _
}

bassCascadeMusic = \relative c' {
  c4 c4 c2 |
  c4 \( b a g 
  f e d c~ |
  c1\)\fermata \bar "||"
  c'4 \(b a g f e d c ~ |
  c1\)\fermata |
  c4 g' f d |
  c1\fermata
  c4 g' f d |
  c1\fermata
}


bassCascadeWords = \lyricmode {
  _
}

%% My Mom
Momglobal = {
  \key a \major
  \time 4/4
  \autoBeamOff
  \set Score.tempoHideNote = ##t
  \tempo 4=96
  \set Staff.midiInstrument = #"trombone"
}

tenorMomMusic = \relative c' {
  \partial 4 d4 |
  e\( es fs\) gs8 gs |
  fs4 e ds d |
  cs1~ |
  cs | \bar "|."
}

tenorMomWords = \lyricmode {
  _
}

leadMomMusic = \relative c {
  \partial 4 e4 |
  cs'2. b8 a |
  d4 cs b e, |
  a1~ | a1 |
}

leadMomWords = \lyricmode {
  My Mom, __ there is no one like my Mom. __
}

bariMomMusic = \relative c' {
  \partial 4 gs4 |
  a4 \( b a \) cs8 cs |
  a4 as a gs |
  e1~ | e1 |
}

bariMomWords = \lyricmode {
  _
}

bassMomMusic = \relative c {
  \partial 4 b4 |
  a4 \(gs' fs \) e8 e |
  d4 fs fs b, |
  a1 ~ | a1 |
}

bassMomWords = \lyricmode {
  _
}


%% The Fire
Fireglobal = {
  \key c \major
  \time 4/4
  \autoBeamOff
  \set Score.tempoHideNote = ##t
  \tempo 4=96
  \set Staff.midiInstrument = #"trombone"
}

tenorFireMusic = \relative c' {
  \partial 4 f4 |
  e2 e4 e |
  ds2 ds4 ds |
  e2 e4 e4 |
  g2 fs4 f4 |
  fs4\( f\) ef d |
  e1 \bar "|."
}

tenorFireWords = \lyricmode {
  _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ \markup {\tiny sang  } \markup {\tiny the} \markup {\tiny choir._} 
}

leadFireMusic = \relative c' {
  \partial 4 g4 |
  c2 d4 c |
  b2 a4 b |
  c2 c4 d |
  cs2 c4 b |
  c1~ | c1
}

leadFireWords = \lyricmode {
  "\"The"fire in our flower was de -- voured by the  "shower,\"" sang the choir.
}

bariFireMusic = \relative c' {
  \partial 4 b4 |
  g2 g4 g |
  a2 fs4 a |
  g2 a4 g |
  e2 a4 g |
  af2 af4 af |
  g1 |
}

bariFireWords = \lyricmode {
  _ _ _ _ _ _ _ _ _ _ _ _ _ _ \markup {\tiny sang  } \markup {\tiny the} \markup {\tiny choir._}
}

bassFireMusic = \relative c {
  \partial 4 d4
  c2 c4 c |
  fs2 b,4 fs' |
  c2 c4 bf |
  a2 d4 d |
  ef4\( d\) ef f |
  c1 |
}

bassFireWords = \lyricmode {
  _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ \markup {\tiny sang  } \markup {\tiny the} \markup {\tiny choir._} 
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%       STOP EDITING HERE.    STOP EDITING HERE.       %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% showLastLength = R2*48

\book {
  \paper {
    #(set-paper-size "letter")
    bottom-margin   = 0.75\in
    top-margin      = 0.50\in
    left-margin     = 0.75\in
    line-width      = 7.00\in
    last-bottom-spacing = 0.25\in
  }
  \header {
    % http://lilypond.org/doc/v2.12/Documentation/user/lilypond/Creating-titles
    title = "Warm Up Exercises"
    subtitle = "(Tuning)"
  }
  
  %% One, One, Two One
  \score {
    \new Staff
    <<
      \OneOneTwoGlobal
      \OneOneTwoMusic
      \addlyrics { \OneOneTwoWords }
      
    >> 
    \header {
      piece = \markup{
        \bold \larger \column { "One, One Two One" }
      }
    }
  }
  
  %% One Two Three
  \score {
    \new Staff
    <<
      \OneTwoGlobal
      \OneTwoMusic
      \addlyrics { \OneTwoWords }
      
    >> 
    \header {
      piece = \markup{
        \bold \larger "One Two Three Four Five"
      }
    }
  }
  
  %% Cascade
  \score {
    \new ChoirStaff <<
      \new Lyrics = "tenors" \with {
        % this is needed for lyrics above a staff
        \override VerticalAxisGroup.staff-affinity = #DOWN
      }{ s1 }
      \new Staff = topstaff <<
        \clef "treble_8"
        \new Voice = "tenors" {
          \voiceOne
          <<
            \Cascadeglobal
            \set midiInstrument = #"clarinet"
            \tenorCascadeMusic
          >>
        }
        \new Voice = "leads" {
          \voiceTwo
          << \Cascadeglobal \leadCascadeMusic >>
        }
      >>
      \new Lyrics = "leads" { s1 }
      \new Lyrics = "baris" \with {
        % this is needed for lyrics above a staff
        \override VerticalAxisGroup.staff-affinity = #DOWN
      } { s1 }
      \new Staff = bottomstaff <<
        \clef bass
        \new Voice = "baris" {
          \voiceOne
          << \Cascadeglobal \bariCascadeMusic >>
        }
        \new Voice = "basses" {
          \voiceTwo << \Cascadeglobal \bassCascadeMusic >>
        }
      >>
      \new Lyrics = basses { s1 }
      \context Lyrics = tenors \lyricsto tenors \tenorCascadeWords
      \context Lyrics = leads \lyricsto leads \leadCascadeWords
      \context Lyrics = baris \lyricsto baris \bariCascadeWords
      \context Lyrics = basses \lyricsto basses \bassCascadeWords
    >>
    \header {
      piece = \markup{
        \bold \larger "Cascade with Chords"
      }
    }
    \layout {
      \context {
        \Staff
      }
      \context {
        \Lyrics
        \override LyricSpace.minimum-distance = #2.0
      }
    }
    \midi {
      \context {
        \ChoirStaff
        \remove "Staff_performer"
      }
      \context {
        \Voice
        \consists "Staff_performer"
      }
    }
  }
  
  \markup {
    Repeat as necessary up or down a half-step and/or with other vowel targets.
  }
  
  %% My Mom
  \score
  {
    
    \new ChoirStaff <<
      \new Lyrics = "tenors" \with {
        % this is needed for lyrics above a staff
        \override VerticalAxisGroup.staff-affinity = #DOWN
      }{ s1 }
      \new Staff = topstaff <<
        \clef "treble_8"
        \new Voice = "tenors" {
          \voiceOne
          <<
            \Momglobal
            \set midiInstrument = #"clarinet"
            \tenorMomMusic
          >>
        }
        \new Voice = "leads" {
          \voiceTwo
          << \Momglobal \leadMomMusic >>
        }
      >>
      \new Lyrics = "leads" { s1 }
      \new Lyrics = "baris" \with {
        % this is needed for lyrics above a staff
        \override VerticalAxisGroup.staff-affinity = #DOWN
      } { s1 }
      \new Staff = bottomstaff <<
        \clef bass
        \new Voice = "baris" {
          \voiceOne
          << \Momglobal \bariMomMusic >>
        }
        \new Voice = "basses" {
          \voiceTwo << \Momglobal \bassMomMusic >>
        }
      >>
      \new Lyrics = basses { s1 }
      \context Lyrics = tenors \lyricsto tenors \tenorMomWords
      \context Lyrics = leads \lyricsto leads \leadMomWords
      \context Lyrics = baris \lyricsto baris \bariMomWords
      \context Lyrics = basses \lyricsto basses \bassMomWords
    >>
    \header {
      piece = \markup{
        \bold \larger "My Mom (singable consonants}"
      }
    }
    \layout {
      \context {
        \Staff
      }
      \context {
        \Lyrics
        \override LyricSpace.minimum-distance = #2.0
      }
    }
    \midi {
      \context {
        \ChoirStaff
        \remove "Staff_performer"
      }
      \context {
        \Voice
        \consists "Staff_performer"
      }
    }
  }

  %% The Fire
  \score
  {
    
    \new ChoirStaff <<
      \new Lyrics = "tenors" \with {
        % this is needed for lyrics above a staff
        \override VerticalAxisGroup.staff-affinity = #DOWN
      }{ s1 }
      \new Staff = topstaff <<
        \clef "treble_8"
        \new Voice = "tenors" {
          \voiceOne
          <<
            \Fireglobal
            \set midiInstrument = #"clarinet"
            \tenorFireMusic
          >>
        }
        \new Voice = "leads" {
          \voiceTwo
          << \Fireglobal \leadFireMusic >>
        }
      >>
      \new Lyrics = "leads" { s1 }
      \new Lyrics = "baris" \with {
        % this is needed for lyrics above a staff
        \override VerticalAxisGroup.staff-affinity = #DOWN
      } { s1 }
      \new Staff = bottomstaff <<
        \clef bass
        \new Voice = "baris" {
          \voiceOne
          << \Fireglobal \bariFireMusic >>
        }
        \new Voice = "basses" {
          \voiceTwo << \Fireglobal \bassFireMusic >>
        }
      >>
      \new Lyrics = basses { s1 }
      \context Lyrics = tenors \lyricsto tenors \tenorFireWords
      \context Lyrics = leads \lyricsto leads \leadFireWords
      \context Lyrics = baris \lyricsto baris \bariFireWords
      \context Lyrics = basses \lyricsto basses \bassFireWords
    >>
    \header {
      piece = \markup{
        \bold \larger "The Fire in our Flower (Triphthongs)"
      }
    }
    \layout {
      \context {
        \Staff
      }
      \context {
        \Lyrics
        \override LyricSpace.minimum-distance = #2.0
      }
    }
    \midi {
      \context {
        \ChoirStaff
        \remove "Staff_performer"
      }
      \context {
        \Voice
        \consists "Staff_performer"
      }
    }
  }
}


%{
convert-ly (GNU LilyPond) 2.18.0  convert-ly: Processing `'...
Applying conversion: 2.17.0, 2.17.4, 2.17.5, 2.17.6, 2.17.11, 2.17.14,
2.17.15, 2.17.18, 2.17.19, 2.17.20, 2.17.25, 2.17.27, 2.17.29, 2.17.97
%}
