%* SVN FILE: $Id$ *%
%* vim: set expandtab tabstop=4 softtabstop=4 shiftwidth=4: *%
% @author $Author$
% @version $Revision$
% @lastrevision $Date$
% @filesource $URL$

% Fugue #1 in D Minor

\version "2.17.97"

\header {
  % http://lilypond.org/doc/v2.12/Documentation/user/lilypond/Creating-titles
  title = "Fugue No. 1"
  subtitle = "(1992)"
  lastupdated = "10/27/2007"
  composer = "Jeffrey Harris"
  copyright = \markup \smaller \column {"© 1992, 1995, 2012 Jeff Harris, Barbershop Bass Publishing, ASCAP. All Rights Reserved. No recording use, public performance" "for profit use or any other use requiring authorization, or reproduction or sale of copies in any form shall be made of or from this" "Arrangement unless licensed by the copyright owner or an agent or organization acting on behalf of the copyright owner." }
  tagline = \markup \column {"$Id$" }
}

\include "english.ly"
\include "articulate.ly"

global = {
  \key d \minor
  \time 4/4
  % \autoBeamOff
  \set Score.tempoHideNote = ##t
  \tempo 4=80
  % \set Staff.midiInstrument = #"trombone"
}

voicefirst = \relative c' {
  d8 e16 cs d e f g a8 g f16 e d e |
  c16 e d8 f e16 a gs a b g a8. ds,16 |
  e16 fs gs e gs a b gs a b cs g e bf' g c |
  d8. e16 d8. f16 e bf a8 d4\mordent |
  cs16 e d8 f e16 a gs a b gs a8. e16 |
  a8 g f16 e d f g8 d bf d |
  g8 f e16 d c e f8 c a  c |
  f8 ef d16 c bf d c8. d16 e g bf8 |
  a8 s8 s4 s2 |
  c,8 d16 b c d e f g8. f16 e d c d |
  g8 a16 fs g8 f e s s e |
  f8 g16 e f8 ef d s s d |
  ef8 f16 d ef8 d c s s c |
  d8 ef16 cs d8 c bf g' c, fs |
  g16 bf, d a bf8 g fs16 e g fs g8 e'16 cs |
  d8 bf a8. g'16 f8 e d8. cs16 |
  d8 e16 cs d e f g a8 g f16 e d g |
  f16 d cs d a8. e'16 <fs, a d>2 \bar "|."
}


voicesecond = \relative c' {
  r1 |
  a8 b16 gs a b c d e8 d c16 b a b |
  gs8 d' b e cs a g e' |
  d,8 e16 cs d e f g a8 g f16 e d e |
  a,8. b16 a8. f'16 e4\mordent a,8 cs |
  d16 e f g a8 a, bf16 d e fs g f e d |
  c16 d e f g8 g, a16 c d e f e d c |
  bf16 c d ef f8 f, e16 g a bf c4 |
  f16 d c g' a8 f e16 f g e f8 d16 b |
  c8 g c8. a16 g8 b c a16 fs |
  g16 b c d g f e d c e f g c, bf a g |
  f16 a bf c f ef d c bf d e f bf, af g f |
  ef16 g af bf ef d c bf a c d ef a, g f ef |
  d16 fs g a d c bf a g bf c d c a d d, |
  g8 a16 fs g a bf c d8 c bf16 a g a |
  d16 f g e f8. e16 d8 e16 cs d e f g |
  a8. g16 f e d e d f g e f8 a, |
  bf16 bf' a bf a g a a, <d d,>2
}


voicethird = \relative c' {
  s1
  s1
  s1
  f16 a g8 bf a16 d, cs d e cs d8. gs16 |
  a8 b16 gs a b c8 e d cs16 b a bf |
  a2 g2~ |
  g2 f2~ |
  f4~ f8 d g4~ g8. e16 |
  f8 g16 e f g a bf c8 bf a16 g f g |
  e16 g f8 e g16 c b c d8 c4 |
  b8 s s b c d16 b c8 bf |
  a8 s s a bf c16 a bf8 af |
  g8 s s g a bf16 g a8 a |
  fs8 s s fs g4 a |
  bf8 s8 s4 s2 |
  d,8 e16 cs d e f g a8 g f16 e d e |
  f8 g16 e f g a g f8 bf a16 g f e |
  d4 d16 e cs8 d2 |
}


%{
convert-ly (GNU LilyPond) 2.18.0  convert-ly: Processing `'...
Applying conversion: 2.15.7, 2.15.9, 2.15.10, 2.15.16, 2.15.17,
2.15.18, 2.15.19, 2.15.20, 2.15.25, 2.15.32, 2.15.39, 2.15.40,
2.15.42, 2.15.43, 2.16.0, 2.17.0, 2.17.4, 2.17.5, 2.17.6, 2.17.11,
2.17.14, 2.17.15, 2.17.18, 2.17.19, 2.17.20, 2.17.25, 2.17.27,
2.17.29, 2.17.97
%}
