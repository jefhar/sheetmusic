%{ SVN FILE: $Id$
   @author $Author$
   @version $Revision$
   @lastrevision $Date$
   @filesource $URL$


   Arranged for Barbershop Quartet by Kevin Keller.
%}

\version "2.18.2"
\language "english"

\header {
  % http://lilypond.org/doc/v2.12/Documentation/user/lilypond/Creating-titles
  title = "Ring of Fire"
%  subtitle = "(1916)"
%  poet = "GRANT CLARKE."
%  composer = "FRED FISCHER."
%  lastupdated = "11/9/2012"
%  arranger = "Arranged by JEFF HARRIS."
%  copyright = \markup \smaller \column {"Arrangement © 2013 Jeff Harris, Barbershop Bass Publishing, ASCAP. All Rights Reserved. No recording use, public performance" "for profit use or any other use requiring authorization, or reproduction or sale of copies in any form shall be made of or from this" "Arrangement unless licensed by the copyright owner or an agent or organization acting on behalf of the copyright owner." }
%  tagline = \markup \column {"Questions about the contest suitability of this song/arrangement should be directed to the judging community" "and measured against current contest rules. Ask before you sing." "$Id$" }
}

\include "english.ly"

global = {
  \key g \major
  \time 2/2
%  \autoBeamOff
  \set Score.tempoHideNote = ##t
  \tempo 2=92
  \override Score.BarNumber.break-visibility = ##(#t #t #t)
  % \override SpacingSpanner.spacing-increment = #8.0
  

}

xVoice = \markup { \smaller \sans { x } } % print this by using '\xVoice'

tenorMusic = \relative c'' {
  % dynamic marking is only for Midi track and should be removed before printing
  \set midiInstrument = #"clarinet"
  % Intro
  \partial 2. r4\mf^\markup { "Intro" } r2
  g4 g8 g~ g g g4 |
  g4 g |
  g1~ |
  g2. r4 |

  d4 d8 d~ d d d4 |
  d4 d |
  d1~ |
  d2. r4 |
  
  % Verse 1
  d2 d |
  g2. r4 |
  g4 g8 g~ g g g4 |
  g4 g |
  g1~ |
  g2. r4 |
  
  d2 g | %15
  d d4 d |
  d d8 d~ d d d4 |
  
  d4 d |
  d1~ |
  d2. r4 |
  b2 d | %21
  d g |
  g4 g8 g~ g  g g4 |
  g4 g |
  g1~ |
  g2. r4 |
  d1~ |
  d2 |
  d2 ef |
  g2 r |
  
  % Chorus 1
  d1 |
  fs |
  g~ |
  g2. r4 |
  
  d1 |
  fs |
  g~ |
  g2. r4 |
  
  b,2 b |
  d g |
  g1 |
  g2 d |
  e d |
  d1 |
  r1 |
  
  % Bridge
  g4 g8 g~ g g g4 |
  g4 g |
  g1~ |
  g2. r4 |
  
  d4 
  d8 d~ d d d4 |
  d4 d |
  d1~ |
  d2. r4 |
  
  % Chorus 2
  d1 |
  fs |
  g~ |
  g2. r4 |
  
  d1 |
  fs |
  g~ |
  g2. r4 |
  
  b,2 b |
  d g |
  g1 |
  g2 d |
  
  e2 d |
  d ef |
  g2. r4
  
  % Verse 2
  g2 g2 |
  g g |
  g g |
  g g |
  
  g4 g8 g g4 g8 g8 |
  g4 g8 g g4 g8 g8 |
  g4 g8 g g4 g8 g8 |
  g4 g8 g g4 g8 g8 |
  
  g4 g r g |
  r g |
  r g r g |
  g g |
  g g r g |
  r g r g |
  
  d2 e |
  fs1 |
  r4 g8 g bf4 a |
  g1
  
  % Chorus 3
  d1 |
  fs |
  g~ |
  g2. r4 |
  
  d1 |
  r2 fs4 fs |
  e fs e fs |
  g b2.~ |
  b1~ |
  b2 r2 |
  
  g2 d |
  d g |
  b g |
  g d |
  e d |
  d e |
  d2. r4 |
  
  d2 e |
  g2. r4 |
  r2 g |
  a r |
  r1 |
  d,4 d8 d~ d d d4 |
  d4 d |
  d1~ |
  d1 |
  
}
tenorWords = \lyricmode {
 
}

leadMusic = \relative c' {
  % dynamic marking is only for Midi track and should be removed before printing
  % \set midiInstrument = #"trombone"
  \set midiInstrument = #"clarinet"
  % Verse 1
  \partial 2. b4\mf c cs |
  d4 d8 d~ d d d4 | \bar "||"
  \time 1/2 e4 c | \bar "||"
  \time 2/2 d1~ |
  d2. r4 |
  
  b4 b8 b~ b b b4 | \bar "||"
  \time 1/2 c4 a | \bar "||"
  \time 2/2 b1~ |
  b2. r4 |
  
  % Verse 1
  b2^\markup { "Verse 1" } b2 |
  d2. r4 |
  d4 d8 d~ d d d4 | \bar "||"
  \time 1/2 e4 c | \bar "||"
  \time 2/2 d1~ |
  d2. r4 |
  
  b2 d |
  b2 c4 c |
  b4 b8 b~ b b b4 | \bar "||"
  \time 1/2 c4 a | \bar "||"
  \time 2/2 b1~ |
  b2. r4 |
  
  g2 b |
  b e |
  d4 d8 d~ d d d4 | \bar "||"
  \time 1/2 e4 c | \bar "||"
  \time 2/2 d1~ |
  d2. r4 |
  b1 |
  \time 1/2 c2|
  \time 2/2 b c |
  d2 r |
  
  % Chorus 1
  a1^\markup { "Chorus 1" } |
  d |
  e |
  d2. r4
  a1 |
  d |
  e |
  d2. r4 |
  g,2 g |
  b d |
  d e |
  d b |
  c c |
  b1 |
  r4 g a as
  
  % Bridge
  \repeat volta 2 {
    d4 d8 d~ d d d4
    \time 1/2 e4 c 
    \time 2/2 d1~
    d2. r4 |
    b4 b8 b~ b b b4 |
    \time 1/2 c4 a |
    \time 2/2 b1~ |
    b2. r4 |
  }
  
  % Chorus 2
  a1^\markup { "Chorus 2" } |
  d |
  e |
  d2. r4 |
  a1 |
  d |
  e |
  d2. r4 |
  g,2 g |
  b d |
  d e |
  d b |
  c1 |
  b2 c |
  d2. r4 |
  
  % Verse 2
  d2^\markup { "Verse 2" } d |
  d c |
  d d |
  d d |
  
  d4 d8 d d4 d8 d |
  d4 d8 d d4 d8 d |
  d4 d8 d d4 d8 d |
  d4 d8 d d4 d8 d |
  d4 d r d |
  \time 1/2 r4 e |
  \time 2/2 r4 d r d |
  \time 1/2 e4 c |
  \time 2/2 d4 d r d |
  r d r d |
  b1 |
  d1 |
  r4 d8 d e4 ef |
  d1 \bar "||"
  
  % Chorus 3
  a1^\markup { "Chorus 3" } |
  d |
  e |
  d2. r4 |
  a1 |
  r2 d4 d |
  c4 d c d |
  d g2.~ |
  g1~ |
  g2 r |
  d2 b |
  b d |
  g e |
  d b |
  c1 |
  b2 c |
  b2. r4 |
  b2 c |
  d2. r4 |
  r2 d2 |
  fs2 r |
  r1
  b,4 b8 b~ b b b4 |
  \time 1/2 c4 a |
  \time 2/2 b1~ b |
  
  
  \bar "|."

}
leadWords = \lyricmode {
  % Verse 1

}

bariMusic = \relative c' {
  % dynamic marking is only for Midi track and should be removed before printing
  %\set midiInstrument = #"clarinet"
  \set midiInstrument = #"trombone"
  % Verse 1
  \partial 2. g4\ff a as |
  b4 b8 b~ b b b4 |
  c4 a |
  b1~ |
  b2. r4 |
  g4 g8 g~ g g g4 |
  a4 fs |
  g1~ |
  g2. r4 |
  
  % Verse 1
  g2 g | 
  b2. r4 |
  b4 b8 b~ b b b4 
  c a |
  b1~ |
  b2. r4 |
  g2 b |
  g g4 fs |
  g4 g8 g~ g g g4 |
  
  
  a4 fs |
  g1~ |
  g2. r4
  
  d2 g |
  g c |
  b4 b8 b~ b b b4 |
  c4 a |
  b1~ |
  b2. r4 |
  g1 |
  fs2 |
  g a |
  b r |
  
  % Chorus 1
  
  fs1 |
  a |
  c |
  b2. r4 |
  fs1 |
  a |
  c |
  b2. r4 |

  d,2 d |
  g b |
  b c |
  b g |
  g fs |
  g1 |
  r4 g a as  |
  
  % Bridge
  b4 b8 b~ b b b4 |
  c4 a |
  b1~ |
  b2. r4 |
  g4 g8 g~ g g g4 |
  a4 fs |
  g1~ |
  g2. r4 |
  
  % Chorus 2
  fs1 |
  a |
  c |
  b2. r4 |
  fs1 |
  a |
  c |
  b2. r4 |
  d,2 d |
  g b |
  b c |
  b g |
  g fs |
  g a |
  b2. r4 |
  
  % Verse 2
  g2 g |
  g g |
  g g |
  g g |
  
  g4 g8 g g4 g8 g8 |
  g4 g8 g g4 g8 g8 |
  g4 g8 g g4 g8 g8 |
  g4 g8 g g4 g8 g8 |
  
  b4 b r b |
  r c |
  r b r b |
  c a |

  b4 b r b |
  r b r b |
  
  g1 |
  c |
  r4 b8 b c4 c |
  b1 |
  
  % Chorus 3
  fs1 |
  a |
  c |
  b2. r4 |
  fs1 |
  r2 a4 a |
  g4 a g a |
  b d2.~ |
  d1~ |
  d2 r |
  
  b g |
  g b |
  d c |
  b g |
  g fs |
  g a |
  b2. r4 |
  g2 a |
  b2. r4 |
  
  r2 b |
  d2 r |
  r1 |

  g,4 g8 g~ g g g4 |
  a4 fs |
  g1~ |
  g
  
  
}
bariWords = \lyricmode {
}

bassMusic = \relative c {
  % dynamic marking is only for Midi track and should be removed before printing
  \set midiInstrument = #"clarinet"
  % Verse 1
  \partial 2. r4\mf r2
  r1 |
  r2 |
  r1 |
  r |
  r |
  r2 |
  r1 |
  r1 |
  d1~ |
  d4 d8 d e4 c |
  d1~ |
  d2~ |
  d1 |
  r2 r4 b8 b |
  b1~ |
  b4 b c a |
  b1~ |
  b2~ |
  b1 |
  r1 | %%%%%
  d1~ |
  d4 d e c |
  d1~ |
  d2~ |
  d1 |
  r |
  b4 b8 b~ b b b4 |
  a4 fs |
  g1~ |
  g2 r |
  d'2 fs |
  a2 a4 a |
  g8 g4 g8~ g4 g |
  e4 d r8 d d4 |
  d2 fs |
  a2. a8 a |
  g2 g |
  e4 d r8 b a4 |
  g2 b |
  d1 |
  r4 b c a |
  b4 d2. |
  r4 g,4 a fs |
  g1 |
  r1 |
  
  % Bridge
  r1 r2 r1 r r r2 r1 r
  
  % Chorus 2 - see if this is after the repeat
  d'2 fs |
  a2 a4 a |
  g8 g4 g8~ g4 g |
  e4 d r8 d d4
  d2 fs |
  a2. a8 a |
  g2 g |
  e4 d r8 b a4 |
  g2 b |
  d1 |
  r4 b4 c a |
  b d2. |
  r4 g a fs |
  g1 |
  r2 r4 d4 ||
  
  % Verse 2
  d1~ |
  d4. d8 e c4. |
  d1~ |
  d2 r4 b |
  b1~ |
  b4 b c a |
  b1~ |
  b2 r |
  d8 d4. d4 d |
  e8 c4 d8~ |
  d1~ |
  d2~ |
  d1 |
  r1 |
  b1~ |
  b4 b8 b a4 fs |
  g1 |
  r4 g b c |
  
  % Chorus 3
  d2 fs |
  a2 a4 a |
  g8 g4 g8~ g4 g |
  e4 d r8 d d4 |
  d2 d |
  a r |
  r1 |
  r |
  r |
  r2 r8 b8 a4 |
  g2 b |
  d1 |
  r4 b c a |
  b d2. |
  r4 g,4 a fs |
  g1 |
  r4 g a fs |
  g1 |
  r2 b |
  d1 |
  r2 d |
  g,1~ |
  g~ |
  g2~ |
  g1~ |
  g
  
}

bassWords = \lyricmode {
}


\include "../Ring/Ring.ly"


%{
convert-ly (GNU LilyPond) 2.18.0  convert-ly: Processing `'...
Applying conversion: 2.17.0, 2.17.4, 2.17.5, 2.17.6, 2.17.11, 2.17.14,
2.17.15, 2.17.18, 2.17.19, 2.17.20, 2.17.25, 2.17.27, 2.17.29, 2.17.97
%}
