%{ SVN FILE: $Id$
  * vim: set expandtab tabstop=4 softtabstop=4 shiftwidth=4: *
  @author $Author$
  @version $Revision$
  @lastrevision $Date$
  @filesource $URL$

  Electric Batteries
%}

\version "2.17.97"

\header {
  % http://lilypond.org/doc/v2.14/Documentation/user/lilypond/Creating-titles
  title = "Electric Batteries"
  subtitle = "(1990)"
  composer = "Jeffrey Harris (b1970)"
  % poet = "Lyrics by"
  copyright = \markup \smaller \column {"Copyright © 1990, 2012 Jeff Harris, Barbershop Bass Publishing, ASCAP. All Rights Reserved. No recording" "use, public performance for profit use or any other use requiring authorization, or reproduction or sale of copies in" "any form shall be made of or from this Creation unless licensed by the copyright owner or an agent or organization" "acting on behalf of the copyright owner." }
  tagline = \markup \tiny \italic \column { "$Id$"}

}

\include "english.ly"

#(define (calc-finger-without-warning grob)
         (let* ((event (event-cause grob))
                 (digit (ly:event-property event 'digit)))
                     (number->string digit 10)))

global = {
  \key c \major
  \time 4/4
  %\autoBeamOff
  \set Score.tempoHideNote = ##f
  \tempo 4=116
  \numericTimeSignature
  \partial 4
  \clef bass
  \override Fingering.text = #calc-finger-without-warning
}

subP = \markup { \italic {sub.} \dynamic p }

tuba = \relative c, {
  \set Staff.midiInstrument = #"tuba"
  d4 |
  \mark \markup { \musicglyph #"scripts.segno" } g4.\f g8 g4 a |
  bf2 a |
  d,2.. a'8-> |
  d,2..\> a'8-> |
  d,2.. a'8-> |
  d,4\! g a bf~ |
  bf2 f |
  g4 gf f e |
  ef4 d df c |
  d1~ |
  d1 |
  \mark \markup { \musicglyph #"scripts.coda" } r2 a'4 g-^ |
  g4 a2 gf4 |
  e4. d4. d4 |
  g4 a bf a |
  d1 |
  d,1 |
  g4-- \mark \markup { \italic {D.S. al Coda}} f ef d  \bar "||"

  \stopStaff s1 \startStaff \bar ""
  \mark \markup { \musicglyph #"scripts.coda" }

  \override Score.Clef.space-alist = #'(
    (cue-clef extra-space . 2.0)
    (staff-bar extra-space . 0.0)
    (key-cancellation minimum-space . 3.5)
    (key-signature minimum-space . 3.5)
    (time-signature minimum-space . 4.2)
    (first-note minimum-fixed-space . 5.0)
    (next-note extra-space . 1.0)
    (right-edge extra-space . 0.5)
    )

    \override Staff.Clef.full-size-change = ##t
    \set Staff.forceClef = ##t
    r2 a'4 g\sfz

    \bar "|."
}

tromboneOne = \relative c'' {
  \clef "bass^8"
  \set Staff.midiInstrument = #"trombone"
  r4\mf \mark \markup { \musicglyph #"scripts.segno" } |
  r2  g4 a |
  bf1 |
  r8 g4 g8 g4 g4 |
  r8\> g4 g8 g4 g4 |
  r8 g4 g8 g4 g4\! |
  r8 g4 g8 g4 ef~ |
  ef2. r4 |
  b'8 bf-> a af-> g gf-> f e-> |
  ef8 d-> df c-> b bf-> a4 |
  b,1 |
  e'8 fs a fs g2 \mark \markup { \musicglyph #"scripts.coda" } |
  e8\f c r4 r d |
  d4 g4. a8 bf4 |
  a4. d,?4. d4 |
  g4.\mf gf8 f4 d4~ |
  d4 g8 a bf a a bf |
  e,4~-7 e\glissando bf'4 af8 g |
  g4-- \mark \markup { \italic {D.S. al Coda}} r4 r2  \bar "||"

  \stopStaff s1 \startStaff \bar ""
  \mark \markup { \musicglyph #"scripts.coda" }

  \override Score.Clef.space-alist = #'(
    (cue-clef extra-space . 2.0)
    (staff-bar extra-space . 0.0)
    (key-cancellation minimum-space . 3.5)
    (key-signature minimum-space . 3.5)
    (time-signature minimum-space . 4.2)
    (first-note minimum-fixed-space . 5.0)
    (next-note extra-space . 1.0)
    (right-edge extra-space . 0.5)
  )

  \override Staff.Clef.full-size-change = ##t
  \set Staff.forceClef = ##t
  \ottava #1
  e8 c r4 r g'\sfz

  \bar "|."

}

tromboneTwo = \relative c' {
  \set Staff.midiInstrument = #"trombone"
  r4\mf \mark \markup { \musicglyph #"scripts.segno" } |
  r2 d4 e |
  d1 |
  r8 e4 e8 e4 e4 |
  r8\> e4 e8 e4 e4 |
  r8 e4 e8 e4 e4\! |
  r8 e4 e8 e4 c4~ |
  c2. r4 |
  r1 |
  r1 |
  r2 r4 c8\f b |
  a8 c b4. c8 d4 \mark \markup { \musicglyph #"scripts.coda" } |
  r2 r4 cs4\mf |
  bf4 r4 r8 df4.~ |
  df4 r r a |
  bf4. b8 bf4 b |
  bf4. ef8 f fs f4 |
  b,?4-7~ b\glissando f' c |
  d4-- \mark \markup { \italic {D.S. al Coda}} r r2
  \bar "||"

  \stopStaff s1 \startStaff \bar ""
  \mark \markup { \musicglyph #"scripts.coda" }

  \override Score.Clef.space-alist = #'(
    (cue-clef extra-space . 2.0)
    (staff-bar extra-space . 0.0)
    (key-cancellation minimum-space . 3.5)
    (key-signature minimum-space . 3.5)
    (time-signature minimum-space . 4.2)
    (first-note minimum-fixed-space . 5.0)
    (next-note extra-space . 1.0)
    (right-edge extra-space . 0.5)
    )

    \override Staff.Clef.full-size-change = ##t
    \set Staff.forceClef = ##t
    r2 r4 d\sfz

    \bar "|."

}

tromboneThree = \relative c' {
  \set Staff.midiInstrument = #"trombone"
  r4\mf \mark \markup { \musicglyph #"scripts.segno" } |
  r2 bf4 c |
  bf1 |
  r8 c4 c8 c4 c4 |
  r8\< c4 c8 c4 c4 |
  r8 c4 c8 c4 c4\! |
  r8 c4 c8 c4 af4~ |
  af2. r4 |
  r1 |
  r2 r4 e |
  r2 a8\mf g fs a |
  g1 \mark \markup { \musicglyph #"scripts.coda" } |
  r2 r4 g |
  a4 g f gf |
  f4. af4 g8 fs4 |
  g4. gf8 f4 a |
  g4 a a8 b? c b |
  b,4-7~ b\glissando d bf'8 a |
  b?4-- \mark \markup { \italic {D.S. al Coda}} r r2
  \bar "||"

  \stopStaff s1 \startStaff \bar ""
  \mark \markup { \musicglyph #"scripts.coda" }

  \override Score.Clef.space-alist = #'(
    (cue-clef extra-space . 2.0)
    (staff-bar extra-space . 0.0)
    (key-cancellation minimum-space . 3.5)
    (key-signature minimum-space . 3.5)
    (time-signature minimum-space . 4.2)
    (first-note minimum-fixed-space . 5.0)
    (next-note extra-space . 1.0)
    (right-edge extra-space . 0.5)
    )

  \override Staff.Clef.full-size-change = ##t
  \set Staff.forceClef = ##t
  r2 r4 b\sfz

  \bar "|."
}

tromboneFour = \relative c' {
  \set Staff.midiInstrument = #"trombone"
  r4\mf \mark \markup { \musicglyph #"scripts.segno" } |
  r2 g4 e |
  f1 |
  r8 a4 a8 a4 a4 |
  r8\> a4 a8 a4 a4 |
  r8 a4 a8 a4 a4\! |
  r8 a4 a8 a4 f~ |
  f2. r8 d\f |
  g4. a8 bf4. a8 |
  d4 d,-> g2 |
  r4 e8 d c d~ d4~ |
  d1 \mark \markup { \musicglyph #"scripts.coda" } |
  r4 b4 r2 |
  fs'4 a,2 df4 |
  e4 d8 cs r4 c |
  d4. ef8 d4 f8 ef~ |
  ef2 bf'4 a |
  g,4 a bf af |
  g'4-- \mark \markup { \italic {D.S. al Coda}} r r2
  \bar "||"

  \stopStaff s1 \startStaff \bar ""
  \mark \markup { \musicglyph #"scripts.coda" }

  \override Score.Clef.space-alist = #'(
    (cue-clef extra-space . 2.0)
    (staff-bar extra-space . 0.0)
    (key-cancellation minimum-space . 3.5)
    (key-signature minimum-space . 3.5)
    (time-signature minimum-space . 4.2)
    (first-note minimum-fixed-space . 5.0)
    (next-note extra-space . 1.0)
    (right-edge extra-space . 0.5)
    )

  \override Staff.Clef.full-size-change = ##t
  \set Staff.forceClef = ##t
  r4 b, r4 g'\sfz

  \bar "|."
}


%{
convert-ly (GNU LilyPond) 2.18.0  convert-ly: Processing `'...
Applying conversion: 2.15.7, 2.15.9, 2.15.10, 2.15.16, 2.15.17,
2.15.18, 2.15.19, 2.15.20, 2.15.25, 2.15.32, 2.15.39, 2.15.40,
2.15.42, 2.15.43, 2.16.0, 2.17.0, 2.17.4, 2.17.5, 2.17.6, 2.17.11,
2.17.14, 2.17.15, 2.17.18, 2.17.19, 2.17.20, 2.17.25, 2.17.27,
2.17.29, 2.17.97
%}
