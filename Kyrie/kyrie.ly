%* SVN FILE: $Id$ *%
%* vim: set expandtab tabstop=4 softtabstop=4 shiftwidth=4: *%
% @author $Author$
% @version $Revision$
% @lastrevision $Date$
% @filesource $URL$

% Kyrie Eleison

\version "2.17.97"

\include "../letterpaper.ly"

\include "../Kyrie/kyriemusic.ly"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%       STOP EDITING HERE.    STOP EDITING HERE.       %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\score {
  \context ChoirStaff
  <<
    \context Staff = soprano <<
      \context Voice = sop { << \global \sopranoNotes >> }
      \lyricsto "sop" \new Lyrics \sopranoWords
    >>
    \context Staff = alto <<
      \context Voice = alt { << \global \altoNotes >> }
      \lyricsto "alt" \new Lyrics \altoWords
    >>
    \context Staff = tenor <<
      \context Voice = ten { << \global \tenorNotes >> }
      \lyricsto "ten" \new Lyrics \tenorWords
    >>
    \context Staff = bass <<
      \context Voice = bas { << \global \bassNotes >> }
      \lyricsto "bas" \new Lyrics \bassWords
    >>

    \new PianoStaff \with {
      fontSize = #-4
      \override StaffSymbol.staff-space = #(magstep -4)
      \override StaffSymbol.thickness = #(magstep -3)
    }
    <<
      \new Staff  <<
        \new Voice \with { \remove Dynamic_engraver } {\voiceOne
          \sopranoNotes}
        \new Voice \with { \remove Dynamic_engraver } {\voiceTwo \altoNotes }
      >>
      \new Staff  <<
        \new Voice \with { \remove Dynamic_engraver } {\voiceOne
          \tenorNotes }
        \new Voice \with { \remove Dynamic_engraver } {\voiceTwo \bassNotes}
      >>
    >>
  >>

  \layout {
    \context {
      \Lyrics
      \override LyricSpace.minimum-distance = #2.0
    }
  }
  \midi { }
}



%{
convert-ly (GNU LilyPond) 2.18.0  convert-ly: Processing `'...
Applying conversion: 2.15.7, 2.15.9, 2.15.10, 2.15.16, 2.15.17,
2.15.18, 2.15.19, 2.15.20, 2.15.25, 2.15.32, 2.15.39, 2.15.40,
2.15.42, 2.15.43, 2.16.0, 2.17.0, 2.17.4, 2.17.5, 2.17.6, 2.17.11,
2.17.14, 2.17.15, 2.17.18, 2.17.19, 2.17.20, 2.17.25, 2.17.27,
2.17.29, 2.17.97
%}
