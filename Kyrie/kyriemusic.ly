%* SVN FILE: $Id$ *%
%* vim: set expandtab tabstop=4 softtabstop=4 shiftwidth=4: *%
% @author $Author$
% @version $Revision$
% @lastrevision $Date$
% @filesource $URL$

% Kyrie Eleison

\version "2.17.97"

\header {
  % http://lilypond.org/doc/v2.12/Documentation/user/lilypond/Creating-titles
  title = "Kyrie Eleison"
  subtitle = "(1992)"
  composer = "Jeffrey Harris (b1970)"
  % poet = "Lyrics by"
  copyright = \markup \smaller \column {"Copyright © 1992, 1995, 2009 Jeff Harris, Barbershop Bass Publishing, ASCAP. All Rights Reserved. No recording" "use, public performance for profit use or any other use requiring authorization, or reproduction or sale of copies in" "any form shall be made of or from this Creation unless licensed by the copyright owner or an agent or organization" "acting on behalf of the copyright owner." }
  tagline = \markup \tiny \italic \column { "For the Los Medanos College Choir, Jerry Smith, and Nancy Bachmann." "$Id$"}
}

\include "english.ly"

global = {
  \key f \minor
  \time 4/4
  %\autoBeamOff
  \set Score.tempoHideNote = ##f
  \tempo 4=84
  \set Staff.midiInstrument = #"trombone"
  \numericTimeSignature

}

subP = \markup { \italic {sub.} \dynamic p }


sopranoNotes = \relative c' {
  \partial 4 c4(^\p
  af'2^\mf) bf |
  ef4 df8( bf) c4( bf8 g8 |
  af4)( ^\< g8 e) f2\! |
  f(^\mf \deprecatedcresc bf) |
  c4(^\f ef8 df) c(^\ff e f4) |

  ef4(^\subP df8 c bf2) |
  df4( c8 bf af2) |
  bf4( af8 g f2 |
  g4^\< f8 e f2) |

  g4(^\mf af) bf g |
  af bf8( c) df8( c16 bf) af4 |
  r2 ef'4^\f af, |
  g( c8 bf) af2 |
  g4^\f bf c df( |

  bf2) \breathe ef4^\ff ef, |
  af4(^\f bf) df( c) |
  bf2^\markup { \italic Rall. } f |
  af^\< af |
  r2^\f ef'4^\markup { \italic {A Tempo} } af, |
  g4(^\< c8 bf) af2\! |

  ef'4(^\subP df8 c bf2) |
  df4( c8 bf af2)
  bf4( af8 g f2 |
  g4^\markup { \italic Rall. } f8 e f2^\<) |
  af2^\mf bf^\markup { \italic {A Tempo} } |

  ef4( bf8 g) d'4( b8 g) |
  c4( bf8 g) af4^\<( g8 e)\! |
  f2 \breathe c'^\> |
  df4 bf\! af( bf8 c) |
  df8( c4 bf8) af2

  \time 5/4
  g2.--\deprecatedcresc \breathe bf4 c |
  \time 4/4
  df2 c4( e) |
  g2( f) |
  e4( c bf g8 e) |
  f1^\ff \bar "||"


  \key f \major
  e?4^\p( g8 a8 bf4) a |
  a8(^\mp d?4 e8 f4) f( |
  e4^\mf d8 e) f4 f |
  a8^\f( g~g4) d-^\ff( f-^) \breathe |

  f4~^\fff f2.^\ppp \fermata ^\markup{ \italic {louden hugely} } \bar "|."

}
sopranoWords = \lyricmode {
  Ky- ri- e E- le-2 i- son,
  Ky- ri- e E- le- i- son,
  Ky- ri- e E- le- i- son,
  Chri- ste Chri- ste E- le- i- son,
  Chri- ste E- le- i- son,
  Chri- ste Chri- ste E- le- i- son.
  Ky- ri- e E- le- i- son,
  Ky- ri- e E- le- i- son,
  Ky- ri- e E- le- i- son!
  A- men a- men a- men a- men a- men a-4 men!
}

altoNotes = \relative c' {
  \partial 4 c4(^\p
  f2)^\mf e |
  ef?4 ef f8( e~ e4) |
  f4(^\< e8 c) c2\! |
  c2(^\mf \deprecatedcresc df) |

  f4(^\f af) g(^\ff f) |
  r1 |
  r1 |
  g4^\mp( ef) c2( |
  df4)\< c c2 |

  ef2^\mf ef4 ef4 |
  c d ef ef |
  r2 g4^\f ef4 |
  ef4( g) f2 |
  df4^\f ef2 f4 |

  ef2 \breathe r2 |
  ef4^\f( df) ef2 |
  df2^\markup { \italic Rall. } df |
  df2\< c |
  r2^\f g'4^\markup {\italic {A Tempo} } ef |
  ef4(^\< g) f2\! |

  r1 |
  r1 |
  g4(^\p e) df2 |
  df4(^\markup { \italic Rall. } c) c2^\< |
  f2^\mf e^\markup {\italic {A Tempo} } |

  af4( g) g2 |
  f8( e~ e4) f4(^\< e8 c)\! |
  c2 \breathe ef^\> |
  f4 df\! ef2 |
  f2 ef|

  \time 5/4
  e2.--\deprecatedcresc \breathe f4 af |
  \time 4/4
  g2 e4( g) |
  c2( af4 g8 f) |
  g4( e g e8 c) |
  c1^\ff |

  \key f \major
  c4(^\p bf) f'4 f8( g16 e) |
  f4(^\mp g8. a16) bf4( c) |
  a4(^\mf bf) a d( |
  c4~^\f c8 bf8^\ff) bf4-^ bf-^ \breathe |

  bf4^\fff( a2.)^\ppp \fermata ^\markup{ \italic {louden hugely} }

}
altoWords = \lyricmode {
  Ky- ri- e E- le- i- son,
  Ky- ri- e E- le- i- son,
  Ky- ri- e E- le- i- son,
  Chri- ste Chri- ste E- le- i- son,
  Chri- ste E- le- i- son,
  Chri- ste Chri- ste E- le- i- son.
  Ky- ri- e E- le- i- son,
  Ky- ri- e E- le- i- son,
  Ky- ri- e E- le- i- son!
  A- men a- men a- men a- men a- men a-4 men!
}

tenorNotes = \relative c' {
  \clef "G_8"
  \partial 4 c4~^\p
  c2^\mf c |
  c4 c c2 |
  df4(^\< c8. bf16 af2) |
  af2^\mf(\deprecatedcresc g) |

  af4(^\f c) c(^\ff af) |
  af2^\subP( bf4 af8 g) |
  f2( af8 g f4) |
  ef2( f8. g16 af4 |
  bf4^\< g) af2 |

  bf4^\mf( c8 bf16 af) g4 c8( bf) |
  af4 f g c |
  r2 bf4^\f c |
  g2 c |
  bf4^\f bf2 bf8( af) |

  g2 \breathe r2 |
  c4(^\f g) af2 |
  g2^\markup {\italic Rall.} af |
  ff2^\< ef |
  r2^\f bf'4^\markup {\italic {A Tempo} } c |
  g2^\< c2\! |

  af2(^\subP bf4 af8 g) |
  f2( af8 g f4) |
  e2( f8. g16 af4 |
  bf4^\markup {\italic Rall.} g) af2^\< |
  c2^\mf df^\markup {\italic {A Tempo} } |

  c4( ef) d2 |
  c2 df4^\<( c8. bf16)\! |
  af2 \breathe g^\> |
  f4 f8( g)\! af2 |
  bf2 c2 |

  \time 5/4
  c2.--\breathe \deprecatedcresc df4 c
  \time 4/4
  bf2 c |
  e2( f) |
  c4( af df c8. bf16) |
  a1^\ff |

  \key f \major
  g8(^\p a?) bf4 bf c |
  a4^\mp bf d c |
  c^\mf f d8( e) d4 |
  f4^\f e f-^ ^\ff( df-^ ) \breathe

  c^\fff( c2.^\ppp) |
}
tenorWords = \lyricmode {
  Ky- ri- e E- le- i- son,
  Ky- ri- e E- le- i- son,
  Ky- ri- e E- le- i- son,
  Chri- ste Chri- ste E- le- i- son,
  Chri- ste E- le- i- son,
  Chri- ste Chri- ste E- le- i- son.
  Ky- ri- e E- le- i- son,
  Ky- ri- e E- le- i- son,
  Ky- ri- e E- le- i- son!
  A- men a- men a- men a- men a- men a-4 men!
}

bassNotes = \relative c' {
  \clef bass
  \partial 4 c4(^\p
  f,2)^\mf g |
  af4 af g2 |
  f4(^\< c) f2 |
  f(^\mf\deprecatedcresc ef) |

  df4^\f( af) e'(^\ff df)
  r1
  r1
  g,2^\p( af) |
  g4(^\< c) f,2 |

  df'4(^\mf c) df ef |
  f4 bf, ef af, |
  af'4^\ff af, g^\f c |
  bf2 af |
  bf4^\f g2 f4

  g2\breathe r2
  ef'4^\f( bf) af2 |
  ef'2^\markup {\italic Rall.} df |
  df^\< af |
  af'4^\ff af, g^\markup {\italic {A Tempo} } c |
  bf2^\< af\! |

  r1
  r1
  g2^\p bf
  g4(^\markup {\italic Rall.} c) ^\< f,2 |
  f^\mf g^\markup {\italic {A Tempo} } |

  c4( bf) b2 |
  g2 f4(^\< c')
  f,2\! \breathe c'2^\> |
  af4 bf\! c2 |
  bf2 af |
  \time 5/4

  c2.--\breathe \deprecatedcresc b4 f'
  \time 4/4
  bf,2 g' |
  c,( af)
  g4( af g c) |
  f,1^\ff \bar "||"

  \key f \major
  c'4^\p d? df( c)
  d?(^\mp bf) bf( a8 bf
  c4^\mf bf f') bf
  c2^\f bf4-^ ^\ff bf,-^ \breathe

  f~^\fff f2.^\ppp \fermata ^\markup{ \italic {louden hugely} }

}
bassWords = \lyricmode {
  Ky- ri- e E- le- i- son,
  Ky- ri- e E- le- i- son,
  Ky- ri- e E- le- i- son,
  Chri- ste Chri- ste E- le- i- son,
  Chri- ste E- le- i- son,
  Chri- ste Chri- ste E- le- i- son.
  Ky- ri- e E- le- i- son,
  Ky- ri- e E- le- i- son,
  Ky- ri- e E- le- i- son!
  A- men a- men a- men a- men a- men a-4 men!
}


%{
convert-ly (GNU LilyPond) 2.18.0  convert-ly: Processing `'...
Applying conversion: 2.15.7, 2.15.9, 2.15.10, 2.15.16, 2.15.17,
2.15.18, 2.15.19, 2.15.20, 2.15.25, 2.15.32, 2.15.39, 2.15.40,
2.15.42, 2.15.43, 2.16.0, 2.17.0, 2.17.4, 2.17.5, 2.17.6, 2.17.11,
2.17.14, 2.17.15, 2.17.18, 2.17.19, 2.17.20, 2.17.25, 2.17.27,
2.17.29, 2.17.97
%}
