%* SVN FILE: $Id$ *%
%* vim: set expandtab tabstop=4 softtabstop=4 shiftwidth=4: *%
% @author $Author$
% @version $Revision$
% @lastrevision $Date$
% @filesource $URL$

% This is a simple Barbershop template for lilypond.
% If you need something slightly more complex, search for it on the internet.

\version "2.17.97"

\header {
	% http://lilypond.org/doc/v2.12/Documentation/user/lilypond/Creating-titles
	title = "Could the Dreams Of A Dreamer Come True"
	%  subtitle = "(A Special Tag)"
	%  subsubtitle = "(date)"
	composer = "Arthur Lange"
	poet = "Jeff Branen"
	% arranger = "Arranged by"
	% copyright = \markup \smaller \column {"Arrangement © 2009 Jeff Harris, Barbershop Bass Publishing, ASCAP. All Rights Reserved. No recording use, public performance" "for profit use or any other use requiring authorization, or reproduction or sale of copies in any form shall be made of or from this" "Arrangement unless licensed by the copyright owner or an agent or organization acting on behalf of the copyright owner." }
	% tagline = "This arrangement was not written with the intention of its being used in SPEBSQSA contests."
}

\include "english.ly"
%\pointAndClickOff
#(set-global-staff-size 17.82 )
%{  Note: You should always turn off point and click in any LilyPond files to be
	distributed to avoid including path information about your computer in the
	.pdf file, which can pose a security risk.
%}

\paper {
	#(set-paper-size "letter")
	bottom-margin   = 0.75\in
	top-margin      = 0.50\in
	foot-separation = 0.25\in
	left-margin     = 0.75\in
	line-width      = 7.00\in
}

global = {
	\key g \major
	\time 3/4
	\set Score.tempoHideNote = ##t
	\tempo 4=80
}

melody = \relative c'' {
	\set Staff.midiInstrument = #"trombone"
	\clef treble
	\partial 4
	s4 s2. s s s s s s s2 s8 b8
	\time 4/4
	a g d' g, g fs d' f, |
	fs e c' g b4 r8 b |
	a g d' g, g fs cs' b |
	a e g fs a4 r8 d, |
	d ds e f fs c' d a |
	d, e fs c' b4 r8 b |
	a a g g fs g a a |
	b b cs a d4
	\time 3/4 \bar "||"

	\partial 4
	b8 c \bar "||"
	d2 b8 d |
	e4 b d |
	b2 a8 d |
	b2 a8 b |
	g2 a8 b |
	g2 a8 b |
	e,2.~ |
	e4 r e8 e
	e2 fs8 g |
	a2 b8 c |
	d2 b8 a |
	g2 fs8 g |
	a2 b8 g |
	a2 b8 g |
	a2.~ |
	a4 r b8 c |
	d2 b8 d |
	e2 b8 d |
	b2 a8 d |
	b2 a8 b |
	a2 g8 b |
	a2 g8 b |
	a2.~ |
	a4 r e8 fs |
	g2 e8 g |
	a2 b8 c |
	d2 b8 d |
	e2 g,8 a |
	b4 c cs |
	d a b |
	g2.~ |
	\partial 2 g2 \bar "|."
}

text = \lyricmode {
	Too ma -- ny live in dreams they say, Sweet dreams how dear to me.
}

upper = \relative c'' {
	\clef treble
	\global
	\partial 4
	<<b8 b'>> <<c,8 c'8>> |
	<<d,2 g b d>> <<b,8 b'>> <<d, d'>>
}

lower = \relative c {
	\clef bass
	\global
	\partial 4
	r4
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%       STOP EDITING HERE.    STOP EDITING HERE.       %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\score {
	<<
		\new Voice = "mel" { \global \autoBeamOff \melody }
		\new Lyrics \lyricsto mel \text
		\new PianoStaff <<
			\new Staff = "upper" \upper
			\new Staff = "lower" \lower
		>>
	>>
	\layout {
		\context { \Staff \RemoveEmptyStaves }
	}
	\midi { }
}


%{
convert-ly (GNU LilyPond) 2.18.0  convert-ly: Processing `'...
Applying conversion: 2.13.0, 2.13.1, 2.13.4, 2.13.10, 2.13.16,
2.13.18, 2.13.20, 2.13.27, 2.13.29, 2.13.31, 2.13.36, 2.13.39,
2.13.40,  Not smart enough to convert foot-separation. Adjust settings
for last-bottom-spacing instead. Please refer to the manual for
details, and update manually. 2.13.42, 2.13.44, 2.13.46, 2.13.48,
2.13.51, 2.14.0, 2.15.7, 2.15.9, 2.15.10, 2.15.16, 2.15.17, 2.15.18,
2.15.19, 2.15.20, 2.15.25, 2.15.32, 2.15.39, 2.15.40, 2.15.42,
2.15.43, 2.16.0, 2.17.0, 2.17.4, 2.17.5, 2.17.6, 2.17.11, 2.17.14,
2.17.15, 2.17.18, 2.17.19, 2.17.20, 2.17.25, 2.17.27, 2.17.29, 2.17.97
%}
