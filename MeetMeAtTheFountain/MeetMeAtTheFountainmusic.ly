%{ SVN FILE: $Id$
   @author $Author$
   @version $Revision$
   @lastrevision $Date$
   @filesource $URL$

  Meet Me At The Fountain
  by Mark E. Beam
  copyright 1904.

   Arranged for Barbershop Quartet by Jeff Harris.
%}

\version "2.17.97"
\language "english"

\header {
  % http://lilypond.org/doc/v2.12/Documentation/user/lilypond/Creating-titles
  title = "Meet Me At The Fountain"
  subtitle = "(1904)"
  composer = "MARK E. BEAM."
  lastupdated = "02/05/2014"
  arranger = "Arranged by JEFF HARRIS."
  copyright = \markup \smaller \column {"Arrangement © 2014 Jeff Harris, Barbershop Bass Publishing, ASCAP. All Rights Reserved. No recording use, public performance" "for profit use or any other use requiring authorization, or reproduction or sale of copies in any form shall be made of or from this" "Arrangement unless licensed by the copyright owner or an agent or organization acting on behalf of the copyright owner." }
  tagline = \markup \column {"Questions about the contest suitability of this song/arrangement should be directed to the judging community" "and measured against current contest rules. Ask before you sing." "$Id$" }
}

\include "english.ly"

global = {
  \key f \major
  \time 3/4
  \autoBeamOff
  \set Score.tempoHideNote = ##t
  % \override SpacingSpanner.spacing-increment = #8.0
}

xVoice = \markup { \smaller \sans { x } } % print this by using '\xVoice'

tenorMusic = \relative c' {
  % dynamic marking is only for Midi track and should be removed before printing
  \set midiInstrument = #"clarinet"
  % Verse 1
  f4\mf^\markup { "Verse" } f f | c2 f4 | e2 f4 | f2. |
 
  %5
  e4 e4 e4 | e2 e4 | fs2 fs4 | g2. |
    
  %9
  g4 e e | e e e | g2 a4 | a2. |
  
  %13
  g4 fs g | f2 f4 | e2.~ | e2. |

%{
  %17
  f4 f f | c2 f4 | s2. | s2. |
  
  %21
  e4 e4 e4 | e2 e4 | fs2 fs4 | g2. |
  
  %25
  g4 e e | e e e | g2 a4 | a2. |
  
  %29
  g4 fs g | f2 g4 | g2.~ | g2 s4 |
  
  %33
  f4^\markup { "Verse 2" } f f | c2 f4 | s2. | s2. |

  %37
  e4 e4 e4 | e2 e4 | fs2 fs4 | g2. |
    
  %41
  g4 e e | e e e | g2 a4 | a2. |
  
  %45
  g4 fs g | f2 f4 | e2.~ | e2. |
  
%}
  %49
  f4 f f | c2 f4 | e2 f4 | f2. |
  
  %53
  e4 e4 e4 | e2 e4 | fs2 fs4 | g2. |
  
  %57
  g4 e e | e e e | g2 a4 | a2. |
  
  %61
  g4 fs g | f2 g4 | g2.~ | g2 s4 |
  
  %65
  f2^\markup { "Chorus" }  f4| f2 f4 | g2. | g2. |

  %69
  e2 e4 | e2 e4 | f2.~ | f4 f4 d4 |
    
  %73
  f2 d4 | d2 d4 | e2 g4 | fs2 g4 |
  
  %77
  a2 f4 | fs2 f4 | e2.~ | e4 s4 s |
  
  %81
  a2 a4 | a2 a4 | g2. | g2. |
  
  %85
  fs2 fs4 | g2 g4 | g2\( fs4 | g4\) s4 s |
  
  %89
  f2 f4 | fs2 fs4 | g2. | gs2. |
  
  %93
  b2 b4 | bf2 bf4 | a2.~ | a4 s s |
}

tenorWords = \lyricmode {
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _

}

leadMusic = \relative c' {
  % dynamic marking is only for Midi track and should be removed before printing
  \set midiInstrument = #"trombone"
  \tempo 4=80
  % Verse 1
  a4\f g f | e2 f4 | g2 f4 | a2. | \break
  
  %5
  bf4 a g | fs2 g4 | a2 d,4 | bf'2. | \break
    
  %9
  e4 d c | bf4 a g | a2 g4 | f2. | \break
  
  %13
  b4 c d | g,2 d'4 | c2.~ | c2. | \break
  
%{
  %17
  a4 g f | e2 f4 | g2 f4 | a2. | \break
  
  %21
  bf4 a g | fs2 g4 | a2 d,4 | bf'2. | \break
  
  %25
  e4 d c | bf a g | a2 g4 | f2. | \break
  
  %29
  b4 c d | g,2 f'4 | e2.~ | e2 b4\rest | \bar "||" \break
  
  %33 Verse 2
  a4 g f | e2 f4 | g2 f4 | a2. | \break
  
  %37
  bf4 a g | fs2 g4 | a2 d,4 | bf'2. | \break
    
  %41
  e4 d c | bf4 a g | a2 g4 | f2. | \break
  
  %45
  b4 c d | g,2 d'4 | c2.~ | c2. | \break
  
%}
  %49
  a4 g f | e2 f4 | g2 f4 | a2. | \break
  
  %53
  bf4 a g | fs2 g4 | a2 d,4 | bf'2. | \break
  
  %57
  e4 d c | bf a g | a2 g4 | f2. | \break
  
  %61
  b4 c d | g,2 f'4 | e2.~ | e2 b4\rest | \bar "||" \break

  %65 Chorus
  a2 c4 | d2 c4 | e2. | bf2. | \break

  %69
  bf2 c4 | a2 g4 | c2.~ | c4 a af | \break
    
  %73
  g2 gs4 | a2 b4 | c2 e4 | ds2 e4 | \break
  
  %77
  f2 d4 | a2 b4 | c2.~ | c4 b\rest b\rest | \break
  
  %81
  f'2 f4 | f2 e4 | e2. | d2. | \break
  
  %85
  d2 d4 | e2 f4 | e2.~ | e4 b\rest c | \break
  
  %89
  a2 c4 | d2 a4 | bf2. | f'2. | \break
  
  %93
  g2 d4 | e2 c4 | f2.~ | f4 b,\rest b\rest |
  \bar "|."

}
leadWords = \lyricmode {
  % Verse 1
  I love a maid so sweet and fair,
  
  %5
  With her, the sun -- shines ev' -- ry -- where!
  
  %9
  Down near her home, ev' -- ry night I'm there,

  %13
  Whis -- tling a tune she'll know __  

%{
  %17
  Watch -- ing a light that's up a -- bove,
  
  %21
  From her room there, __ one girl I love
  
  %25
  An -- swers my call, like the coo -- ing dove,
  
  %29
  Sing -- ing so sweet and low; __
  
  %33 Verse 2
  Af -- ter the sun sets in the west,

  %37
  Hid -- den from all in lov -- ers' nest,
    
  %41
  E -- ven the night- birds my se -- cret guessed, __
  
  %45
  Just that I love her so! __

%}  
  %49
  Then, when the time has come to part,
  
  %53
  Tell -- ing my love right from the start,
  
  %57
  Wait for her an -- swer, then my sweet -- heart
  
  %61
  Whis -- pers, so soft and low; __
  
  %65 Chorus
  Meet me at the fount -- ain,

  %69
  O -- ver in the Square; __
    
  %73
  I have some -- thing sweet to tell you, when I
  
  %77
  meet you o -- ver there! __
  
  %81
  In our co -- sey cor -- ner,
  
  %85
  you and I know where! __ So
  
  %89
  meet me at the fount -- ain
  
  %93
  o -- ver in the Square! __

}

bariMusic = \relative c' {
  % dynamic marking is only for Midi track and should be removed before printing
  \set midiInstrument = #"clarinet"
  % Verse 1
  c4 a a | a2 a4 | bf2 a4 | c2. |
  
  %5
  g4 c bf | as2 bf4 | c2 c4 | bf2. |
    
  %9
  bf4 bf bf | g c c | cs2 cs4 | d2. |
  
  %13
  d4 a d | b2 b4 | g2\( a4 | bf2.\) |

%{  
  %17
  c4 g g | a2 a4 | s2. | s2. |
  
  %21
  g4 c bf | as2 bf4 | c2 c4 | bf2. |
  
  %25
  bf4 bf bf | g c c | cs2 cs4 | d2. |
  
  %29
  d4 d d | b2 b4 | c4 s s | s2. |
  
  %33 Verse 2
  c4 g g | a2 a4 | s2. | s2. |

  %37
  g4 c bf | as2 bf4 | c2 c4 | bf2. |
    
  %41
  bf4 bf bf | g c c | cs2 cs4 | d2. |
  
  %45
  d4 a d | b2 b4 | g2\( g4 | bf2.\) |
%}
  
  %49
  c4 a a | a2 a4 | bf2 a4 | c2. |
  
  %53
  g4 c bf | as2 bf4 | c2 c4 | bf2. |
  
  %57
  bf4 bf bf | g c c | cs2 cs4 | d2. |
  
  %61
  d4 d d | b2 b4 | c4 s s | s2. |
  
  %65 Chorus
  c2 a4 | a2 a4 | bf2. | e2. |

  %69
  g,2 bf4 | c2 bf4 | a2.~ | a4 c cf |
    
  %73
  b2 b4 | b2 g4 | g2 bf4 | a2 c4 |
  
  %77
  b2 b4 | c2 d4 | g,2.\( | bf4\) s s |
  
  %81
  c2 c4 | cs2 c4 | bf2. | bf2. |
  
  %85
  c2 c4 | b2 b4 | bf2.~ | bf4 s s |
  
  %89
  c2 a4 | c2 c4 | d2. | d2. |
  
  %93
  d2 g4 | g2 e4 | d4.\( c4.~ | c4\) s s |
}

bariWords = \lyricmode {
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _

}

bassMusic = \relative c {
  % dynamic marking is only for Midi track and should be removed before printing
  \set midiInstrument = #"clarinet"

  % Verse 1
  f4 c c | f,2 c'4 | c2 c4 | f2. |
  
  %5
  c4 c c | cs2 c4 | d2 a4 | g2. |
    
  %9
  c4 g' g  | c,4 c c | e2 e4 | d2. |
  
  %13
  g,4 d' g, | d'2 g,4 | c2\( c4 | g'2.\) |

%{
  %17
  f4 c c | f,2 c'4 | s2. | s2. |
  
  %21
  c4 c c | cs2 c4 | d2 a4 | g2. |
  
  %25
  c4 g' g  | c,4 c c | e2 e4 | d2. |
  
  %29
  g,4 d' g, | d'2 d4 | c4 s s | s2. |
  
  %33 Verse 2
  f4 c c | f,2 c'4 | s2. | s2. |

  %37
  c4 c c | cs2 c4 | d2 a4 | g2. |
    
  %41
  c4 g' g  | c,4 c c | e2 e4 | d2. |
  
  %45
  g,4 d' g, | d'2 g,4 | c2\( c4 | g'2.\) |

%}
  %49
  f4 c c | f,2 c'4 | c2 c4 | f2. |
  
  %53
  c4 c c | cs2 c4 | d2 a4 | g2. |
  
  %57
  c4 g' g  | c,4 c c | e2 e4 | d2. |
  
  %61
  g,4 d' g, | d'2 d4 | c4 s s | s2. |
  
  %65 Chorus
  f2 f4 | f2 f4 | c2. | c2. |

  %69
  c2 g'4 | c,2 c4 | f2.~ | f4 f4 f4 |
    
  %73
  d2 e4 | g,2 g4 | c2 c4 | b2 c4 |
  
  %77
  d2 g4 | d2 g4 | c,2.\( | g'4\) d4\rest d\rest |
  
  %81
  f2 f4 | f2 f4 | g2. | g2. |
  
  %85
  a2 a4 | g2 d4 | c2\( cs4 | c4\) d\rest d\rest |
  
  %89
  f2 f4 | a2 d,4 | g2. | b2. |
  
  %93
  g2 g4 | c2 g4 | f2.~ | f4 d\rest d\rest |

  
}
bassWords = \lyricmode {
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _

}

theChords = \chordmode {
  \set midiInstrument = #"voice oohs"
  % Verse 1
  f4 f:5.9 f | f2:maj7 f4 | c2:7 f4 | f2. |
  
  %5
  c2.:7 | fs2:7 c4:7 | d2.:7 | g2.:m |
    
  %9
  c2.:7 | c2.:7 | a2.:7 | d2.:m |
  
  %13
  g4:7 d:7 g:7 | g2.:7 | c2. | c2.:7 |
%{  
  %17
  f2. | f2. | f2. | f2. |
  
  %21
  c2.:7 | fs2.:7 | d2. | g2.:m |
  
  %25
  c2.:7 | c2. | a2.:7 | d2.:m |
  
  %29
  g2.:7 | g2.:7 | c2.:7 | c2.:7 |
  
  %33 Verse 2
  f2. | f2. | f2. | f2. |

  %37
  c2.:7 | c2.:dim7 | d2.:7 | g2.:m |
    
  %41
  c2.:7 | c2.:7 | a2.:7 | d2.:m |
  
  %45
  g2.:7 | g2.:7 | c2. | c2.:7 |
  
%}
%49
  f4 f:5.9 f | f2:maj7 f4 | c2:7 f4 | f2. |
  
  %53
  c2.:7 | fs2:7 c4:7 | d2.:7 | g2.:m |
  
  %57
  c2.:7 | c2. | a2.:7 | d2.:m |
  
  %61
  g2.:7 | g2.:7 | c2.:7 | c2.:7 |
  
  %65 Chorus
  f2. | f2. | c2.:7 | c2.:7 |

  %69
  c2.:7 | c2:6 c4:7 | f2. | f2 f4:dim7|
    
  %73
  g2:7 e4:7 | g2. | c2 c4:7 | b2:7 c4 |
  
  %77
  d2:m g4:7 | d2:7 g4:7 | c2. | c4:7 s2 |
  
  %81
  f2. | f2.:aug | g2.:min6 | g2.:m |
  
  %85
  d2.:7 | g2:6 g4:7 | c2:7 fs4:7 | c2.:7 |
  
  %89
  f2. | d2.:7 | g2.:m | gs2.:dim7 |
  
  %93
  g2.:7 | c2.:7 | s2. | s2. |
  
}


\include "../MeetMeAtTheFountain/MeetMeAtTheFountain.ly"


%{
convert-ly (GNU LilyPond) 2.18.0  convert-ly: Processing `'...
Applying conversion: 2.17.0, 2.17.4, 2.17.5, 2.17.6, 2.17.11, 2.17.14,
2.17.15, 2.17.18, 2.17.19, 2.17.20, 2.17.25, 2.17.27, 2.17.29, 2.17.97
%}

