%{ SVN FILE: $Id$
   @author $Author$
   @version $Revision$
   @lastrevision $Date$
   @filesource $URL$

  Meet Me At The Fountain
  by Mark E. Beam
  copyright 1904..

   Arranged for Barbershop Quartet by Jeff Harris.
%}

\version "2.17.97"

\include "../letterpaper.ly"

#(ly:set-option 'midi-extension "midi")

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%       STOP EDITING HERE.    STOP EDITING HERE.       %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% showLastLength = R2*48
\score
{
  \new ChoirStaff <<
    %\new ChordNames { \global \set Staff.midiInstrument = #"accordion" \theChords }
    \new Lyrics = "tenors" \with {
      % this is needed for lyrics above a staff
      \override VerticalAxisGroup.staff-affinity = #DOWN
    }{ s1 }
    \new Staff = "topstaff" <<

      \clef "treble_8"
      \new Voice = "tenors" {
        \voiceOne
        <<
          \global
          % \set midiInstrument = #"clarinet"
          \tenorMusic
        >>
      }
      \new Voice = "leads" {
        \voiceTwo
        <<
          \global
          \leadMusic
        >>
      }
    >>
    \new Lyrics = "leads" { s1 }
    \new Lyrics = "baris" \with {
      % this is needed for lyrics above a staff
      \override VerticalAxisGroup.staff-affinity = #DOWN
    } { s1 }
    \new Staff = "bottomstaff" <<
      \clef bass
      \new Voice = "baris" {
        \voiceOne
        <<
          \global
          \bariMusic
        >>
      }
      \new Voice = "basses" {
        \voiceTwo <<
          \global
          \bassMusic
        >>
      }
    >>
    \new Lyrics = "basses" { s1 }
    \context Lyrics = tenors \lyricsto tenors \tenorWords
    \context Lyrics = leads \lyricsto leads \leadWords
    \context Lyrics = baris \lyricsto baris \bariWords
    \context Lyrics = basses \lyricsto basses \bassWords
  >>
  \layout {
    \context {
      \Staff
    }
    \context {
      \Lyrics
      \override LyricSpace.minimum-distance = #2.0
    }
  }
  \midi {
    midiChannelMapping = #'voice
    \context {
      \ChoirStaff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"
    }
  }
}

%{
convert-ly (GNU LilyPond) 2.18.0  convert-ly: Processing `'...
Applying conversion: 2.17.0, 2.17.4, 2.17.5, 2.17.6, 2.17.11, 2.17.14,
2.17.15, 2.17.18, 2.17.19, 2.17.20, 2.17.25, 2.17.27, 2.17.29, 2.17.97
%}


%{
convert-ly (GNU LilyPond) 2.18.0  convert-ly: Processing `'...
Applying conversion:     The document has not been changed.
%}
