%* SVN FILE: $Id$ *%
%* vim: set expandtab tabstop=4 softtabstop=4 shiftwidth=4: *%
% @author $Author$
% @version $Revision$
% @lastrevision $Date$
% @filesource $URL$

% This is a simple Barbershop template for lilypond.
% If you need something slightly more complex, search for it on the internet.

\version "2.17.97"

\header {
	% http://lilypond.org/doc/v2.12/Documentation/user/lilypond/Creating-titles
	title = "There's a Little White Church in the Valley"
	subtitle = ""
	subsubtitle = "(1915)"
	composer = "Music by Arthur Lange (1889-1956)"
	poet = "Lyrics by Jeff Branen (1872-1927)"
	arranger = "Arranged by Jeff Harris (b. 1970)"
	copyright = \markup \smaller \column {"Arrangement © 2011 Jeff Harris, Barbershop Bass Publishing, ASCAP. All Rights Reserved. No recording use, public performance" "for profit use or any other use requiring authorization, or reproduction or sale of copies in any form shall be made of or from this" "Arrangement unless licensed by the copyright owner or an agent or organization acting on behalf of the copyright owner." }
	tagline = \markup \column {"This arrangement was not written with the intention of its being used in SPEBSQSA contests." "Arthur Lange was a United States bandleader and Tin Pan Alley composer of popular music. He composed music" "for over 120 films, including Grand Canary and Woman on the Run. Lange shared an Oscar nomination with" "Hugo Friedhofer for the film The Woman in the Window. All in all he was nominated four times for Oscars but did" "not win any. Lange was a prolific arranger of dance band orchestrations during the 1920s. His \"stock\" orches-" "trations were in use by many bands of the day. Lange wrote \"Arranging for the Modern Dance Orchestra\" which" "was the definitive work of its day (published Robbins Music, 1926)." "$Id$"}
}

\include "english.ly"
% \pointAndClickOff
#(set-global-staff-size 17.82 )
%{  Note: You should always turn off point and click in any LilyPond files to be
	distributed to avoid including path information about your computer in the
	.pdf file, which can pose a security risk.
%}

\paper {
	#(set-paper-size "letter")
	bottom-margin   = 0.75\in
	top-margin      = 0.50\in
	foot-separation = 0.25\in
	left-margin     = 0.75\in
	line-width      = 7.00\in
}

global = {
	\key f \major
	\time 2/2
	\autoBeamOff
	\set Score.tempoHideNote = ##t
	\tempo 2=96
	\set Staff.midiInstrument = #"clarinet"}

tenorMusic = \relative c' {
	%{	f2\p f
	f d
	bf4 bf bf bf
	b1
	c4 c c c
	bf2 e
	f1~ f2 s2

	f2 f
	f d
	bf4 bf bf bf
	ds2 ds
	g4 g g g
	e2 ef
	e1~ e2 s2

	g4 f g f
	e2 e
	ef4 ef ef ef
	g1
%}
}

tenorWords = \lyricmode {
	_
}

leadMusic = \relative c' {
	% verse 1
	a2\fff c |
	g f |
	e4 g e g |
	d1 | \break

	e4 d c d |
	e2 c' |
	a1~ |
	a2 r2 | \break

	a2 c |
	g f |
	e4 g e g |
	b2 a | \break

	g4 d e f |
	g2 a2 |
	c1~ |
	c2 r | \break

	d4 cs d bf |
	a2 g |
	c4 b c a |
	f1 | \break

	e2 g |
	g g |
	g1~ |
	g2 \bar "||"  \break

	% chorus 1
	f4 g |
	a c, f a~ |
	a2 c4 a |
	c2 g | \break

	r4 a c a |
	c2 g |
	r4 fs g a |
	d,1 | \break

	r2 c4 f |
	a c, f a~ |
	a2 c4 a |
	c2 g | \break

	a2 e |
	f4 g a g~ |
	g e2 d4 |
	g1~ | \break

	g4 r c d
	c2 g4 a |
	bf2 c4 d |
	c2 f,4 g | \break

	a2 a4 bf |
	c2 a4 g |
	f2 d4 f |
	g1~ | \break

	g4 r f g |
	a4 c, f a~ |
	a r c a |
	c2 cs | \break

	d a|
	bf c4 d |
	c2 a4 g |
	f1 \break

	% verse 2
	a2 c |
	g f |
	e4 g e g |
	d1 | \break

	e4 d c d |
	e2 c' |
	a1~ |
	a2 r2 | \break

	a2 c |
	g f |
	e4 g e g |
	b2 a | \break

	g4 d e f |
	g2 a2 |
	c1~ |
	c2 r | \break

	d4 cs d bf |
	a2 g |
	c4 b c a |
	f1 | \break

	e2 g |
	g g |
	g1~ |
	g2 \bar "||"  \break

	% chorus 2
	f4 g |
	a c, f a~ |
	a2 c4 a |
	c2 g | \break

	r4 a c a |
	c2 g |
	r4 fs g a |
	d,1 | \break

	r2 c4 f |
	a c, f a~ |
	a2 c4 a |
	c2 g | \break

	a2 e |
	f4 g a g~ |
	g e2 d4 |
	g1~ | \break

	g4 r c d
	c2 g4 a |
	bf2 c4 d |
	c2 f,4 g | \break

	a2 a4 bf |
	c2 a4 g |
	f2 d4 f |
	g1~ | \break

	g4 r f g |
	a4 c, f a~ |
	a r c a |
	c2 cs | \break

	d a|
	bf c4 d |
	c2 c4 c |
	f1 \break
}
leadWords = \lyricmode {
	I'm so hap -- py,
	hap -- py can't you see,
	not be -- cause it's Sun -- day morn.

	I'm de -- light -- ed, nev -- er was ex -- cit -- ed
	half so much since I was born.

	World is ful of sun -- shine; lis -- ten to those bells
	sound -- ing through the dells.

	There's a lit -- tle white church
	in the val -- ley
	where I met Sal -- ly
	one day in June.

	Sal -- ly smiled and the choir start -- ed sing -- ing
	soft -- ly; soon all the world was in tune.

	We've been sweet -- hearts since then,
	we have strolled through the glen,
	gath -- ered flowr's and made love by the way

	In that lit -- tle white church in the val -- ley Sal -- ly
	said she would wed me to -- day.

	I've been wait -- ing for our wed -- ding day
	ev -- er since she won my heart.

	Noth -- ing sweet -- er, soon I'm goin' to meet her
	at the church, no more to part.
	I can pic -- ture Sal -- ly
	strol -- ling through the dells,
	list -- 'ning, to those bells.

	There's a lit -- tle white church
	in the val -- ley
	where I met Sal -- ly
	one day in June.

	Sal -- ly smiled and the choir start -- ed sing -- ing
	soft -- ly; soon all the world was in tune.

	We've been sweet -- hearts since then,
	we have strolled through the glen,
	gath -- ered flowr's and made love by the way

	In that lit -- tle white church in the val -- ley Sal -- ly
	said she would wed me to -- day.
}

bariMusic = \relative c' {
	%{	c2\mp a
	a af
	g4 e g e
	g1

	bf4 bf bf bf
	g2 bf
	c1~ c2 s2

	c2 a
	a af
	g4 e g e
	a2 c

	b4 b b b
	g2 fs
	g1~ g2 s2

	b4 b b d
	g,2 bf
	a4 a a c
	b1
%}
}

bariWords = \lyricmode {
	_
}

bassMusic = \relative c {
	f2\f f2
	c cf
	c4 c c c
	g1

	g4 g g g
	c2 g'
	f1~
	f2 s2

	f2 f
	c cf
	c4 c c c
	fs2 fs

	d4 g, g g
	c2 d
	c1~
	c2 s2

	g'4 gs g g
	c,2 c
	f4 fs f f
	d1

	s1
	s
	s
	s

	s
	s
	s
	s

	s
	s
	s
	s

	s
	s
	s
	s

	s
	s
	s
	s

	s
	s
	s
	s

	s
	s
	s
	s

	s
	s
	s
	s

	s
	s
	s
	s

	s
	s
	s
	s

	s
	s
	s
	s

	s
	s
	s
	s

	s
	s
	s
	s

	s
	s
	s
	s

	s
	s
	s
	s

	s
	s
	s
	s

	s
	s
	s
	s

	s
	s
	s
	s

	s
	s
	s
	s

	s
	s
	s
	s

	s
	s
	s
	s

	s
	s
	s
	s
	s
	s






}

bassWords = \lyricmode {
	_
}

theChords = \chordmode {
	\time 2/2
	f1 |
	f2 f:dim7 |
	c1:7 |
	g1:m |
	c1:7 |
	c2:7 c:5+.7 |
	f1 |
	f
	f1 |
	f2 f:dim7 |
	c1:7 |
	b2 f:dim7 |
	g1 |
	c2 c:dim7 |
	c1 |
	c |
	g4:7 cs:7 g2:7 |
	c1 |
	f:7 |
	d:m |
	c |
	g:7 |
	c:7 |
	c:7 |
	f |
	f |
	c |
	a:dim7 |
	c |
	e |
	d:m |
	s |
	f2 a:dim7 |
	a1:dim7 |
	c |
	a |
	d |
	g |
	c:7 |
	c:7 |
	c |
	e:dim7 |
	f |
	f |
	d:7 |
	g:7 |
	c:7 |
	c:7 |
	f |
	f |
	f2 cs:dim7 |
	d1 |
	g |
	c |
	f |

	f1 |
	f2 f:dim7 |
	c1:7 |
	g1 |
	c1:7 |
	c2:7 c:5+.7 |
	f1 |
	f
	f1 |
	f2 f:dim7 |
	c1:7 |
	b2 f:dim7 |
	g1 |
	c2 c:dim7 |
	c1 |
	c |
	g4 cs g2 |
	c1 |
	f:7 |
	d:m |
	c |
	g:7 |
	c:7 |
	c:7 |
	f |
	f |
	c |
	a:dim7 |
	c |
	e |
	d:m |
	s |
	f2 a:dim7 |
	a1:dim7 |
	c |
	a |
	d |
	g |
	c:7 |
	c:7 |
	c |
	b:7 |
	f |
	f |
	d:7 |
	g:7 |
	c:7 |
	c:7 |
	f |
	f |
	f2 cs:dim7 |
	d1 |
	g |
	c |
	f |

}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%       STOP EDITING HERE.    STOP EDITING HERE.       %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\score{
	\new ChoirStaff <<
		\new ChordNames { \global \set Staff.midiInstrument = #"accordion" \theChords }
		\new Lyrics = tenors { s1 }
		\new Staff = topstaff <<
			\clef "treble_8"
			\new Voice = "tenors" {
				\voiceOne
				<< \global
					\tenorMusic
				>>
			}
			\new Voice = "leads" {
				\voiceTwo
				<< \global
					\set Staff.midiInstrument = #"trombone"
					\leadMusic
				>>
			}
		>>
		\new Lyrics = "leads" { s1 }
		\new Lyrics = "baris" { s1 }
		\new Staff = bottomstaff <<
			\clef bass
			\new Voice = "baris" {
				\voiceOne
				<< \global
					\bariMusic
				>>
			}
			\new Voice = "basses" {
				\voiceTwo
				<< \global
					\bassMusic
				>>
			}
		>>
		\new Lyrics = basses { s1 }
		\context Lyrics = tenors \lyricsto tenors \tenorWords
		\context Lyrics = leads \lyricsto leads \leadWords
		\context Lyrics = baris \lyricsto baris \bariWords
		\context Lyrics = basses \lyricsto basses \bassWords
	>>
	\layout {
		\context {
			% a little smaller so lyrics can be closer to the staff
			\Staff
			% \override VerticalAxisGroup.minimum-Y-extent = #'(-3 . 3)
			% \override SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/1)
			% \override SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/32)
			% \override Score.SeparationItem.padding = #1
		}
	}
	\midi { }
}


%{
convert-ly (GNU LilyPond) 2.18.0  convert-ly: Processing `'...
Applying conversion: 2.13.0, 2.13.1, 2.13.4, 2.13.10,  Not smart
enough to convert minimum-Y-extent. Vertical spacing no longer depends
on the Y-extent of a VerticalAxisGroup. Please refer to the manual for
details, and update manually. 2.13.16, 2.13.18, 2.13.20, 2.13.27,
2.13.29, 2.13.31, 2.13.36, 2.13.39, 2.13.40,  Not smart enough to
convert foot-separation. Adjust settings for last-bottom-spacing
instead. Please refer to the manual for details, and update manually.
2.13.42, 2.13.44, 2.13.46, 2.13.48, 2.13.51, 2.14.0, 2.15.7, 2.15.9,
2.15.10, 2.15.16, 2.15.17, 2.15.18, 2.15.19, 2.15.20, 2.15.25,
2.15.32, 2.15.39, 2.15.40, 2.15.42, 2.15.43, 2.16.0, 2.17.0, 2.17.4,
2.17.5, 2.17.6, 2.17.11, 2.17.14, 2.17.15, 2.17.18, 2.17.19, 2.17.20,
2.17.25, 2.17.27, 2.17.29, 2.17.97
%}
