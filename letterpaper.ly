\version "2.18.0"

\pointAndClickOn
%\pointAndClickOff
#(set-global-staff-size 20 )
%{  Note: You should always turn off point and click in any LilyPond files to be
    distributed to avoid including path information about your computer in the
    .pdf file, which can pose a security risk.
%}

% Display line numbers above every bar


% This needs to be before the \paper block so the functions can be used here
allowGrobCallback =
#(define-scheme-function (parser location syms) (symbol-list?)
     (let ((interface (car syms))
           (sym (cadr syms)))
         #{
             \with {
                 \consists #(lambda (context)
                                `((acknowledgers .
                                      ((,interface . ,(lambda (engraver grob source-engraver)
                                                          (let ((prop (ly:grob-property grob sym)))
                                                              (if (procedure? prop) (ly:grob-set-property! grob sym (prop grob)))
                                                              ))))
                                      ))
                                )
             }
         #}))

absFontSize =
#(define-scheme-function (parser location pt)(number?)
     (lambda (grob)
         (let* ((layout (ly:grob-layout grob))
                (ref-size (ly:output-def-lookup (ly:grob-layout grob) 'text-font-size 12)))
             (magnification->font-size (/ pt ref-size))
             )
         )
     )



\paper {
    #(set-paper-size "letter")
    bottom-margin   = 0.50\in
    top-margin      = 0.50\in
    left-margin     = 0.50\in
    line-width      = 7.50\in
    last-bottom-spacing = 0.5\in
    ragged-last-bottom = ##t
    \include "../bhsCSS.ly"
}