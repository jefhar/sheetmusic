%{ SVN FILE: $Id$
  * vim: set expandtab tabstop=4 softtabstop=4 shiftwidth=4: *
  @author $Author$
  @version $Revision$
  @lastrevision $Date$
  @filesource $URL$

  Tuba Rondo
%}

\version "2.17.97"

\include "english.ly"
\include "articulate.ly"

\header {
  % http://lilypond.org/doc/v2.12/Documentation/user/lilypond/Creating-titles
  title = "A Short Piece for Unaccompanied Tuba by Itself"
  subtitle = "(1993)"
  composer = "Jeffrey Harris"
  % poet = "Lyrics by"
  % arranger = "Arranged by"
  subsubtitle = "Rondo"
  copyright = \markup \smaller \column {"Arrangement © 1993, 2012 Jeff Harris, Barbershop Bass Publishing, ASCAP. All Rights Reserved. No recording use, public performance" "for profit use or any other use requiring authorization, or reproduction or sale of copies in any form shall be made of or from this" "Arrangement unless licensed by the copyright owner or an agent or organization acting on behalf of the copyright owner." }
  tagline = \markup \column {"$Id$" }
}

global = {
  \key f \major
  \time 4/4
  % \autoBeamOff
  \set Score.tempoHideNote = ##t
  \tempo 4=80
  \set Staff.midiInstrument = #"tuba"
}

tubarondo = \relative c, { \clef F \mark \default
  a4^\markup { \italic staccato }  f'8( e f4) a,4 |
  bf4 f'8(e f4) bf,4 |
  c8 a' g f e d c bf |
  a8^\markup { \italic marcato } bf c bf a g f e |
  d4 f'8( e f4) a,4 |
  bf4 f'8(e f4) bf, |
  c8 a' g f e d a b |
  c4-- e8 g c4^\markup { \italic rall. } \override BreathingSign.text = \markup {
    \musicglyph #"scripts.caesura.straight"
  } \breathe a | \mark \default
  af4.^\markup { \italic {a tempo} } c8 ef4 c8 af |
  gf4. bf8 df4. r8 |
  af4. c8 ef4 c8 af |
  bf4. df8 e4. c8 |
  af4. cf8 ef4 cf8 af |
  gf4. bf8 df4. r8 |
  e,4. gs8 b4. r8 |
  ef,4. \grace af16 g8 bf4 e,8 c \mark #1
  a'4^\markup { \italic staccato } f8( e f4) a |
  bf f8( e f4) bf |
  c8 a g f e d c bf |
  a'8 bf c bf a g f e |
  d'4 f,8( e f4) a |
  bf4 f8( e f4) bf | \mark \markup { \musicglyph #"scripts.coda" }
  c8 a g f e d b cs | \mark #3
  d8 e f g a g f e |
  f8 g a bf c bf a g |
  a8 b cs d e d c bf |
  a4 r4 r8 a bf c |
  d4 a8 g a4 d |
  c4 a8 g a4 c |
  bf8 f g a bf4 cs4 |
  d8 a f c bf' g e4 \bar "||"


  \stopStaff s1 \startStaff \bar ""
  \mark \markup { \musicglyph #"scripts.coda" }

  \override Score.Clef.space-alist = #'(
  (cue-clef extra-space . 2.0)
    (staff-bar extra-space . 0.0)
    (key-cancellation minimum-space . 3.5)
    (key-signature minimum-space . 3.5)
    (time-signature minimum-space . 4.2)
    (first-note minimum-fixed-space . 5.0)
    (next-note extra-space . 1.0)
    (right-edge extra-space . 0.5)
    )

  \override Staff.Clef.full-size-change = ##t
  \set Staff.forceClef = ##t

  c'4-- e,8 g c4 \override BreathingSign.text = \markup {
    \musicglyph #"scripts.caesura.straight"
  } \breathe g |
  bf4 f8 d bf4 d |
  bf4 df8 gf bf4 c |
  bf4^\markup { \italic rit. } g c, f,\fermata \bar "|."

}


%{
convert-ly (GNU LilyPond) 2.18.0  convert-ly: Processing `'...
Applying conversion: 2.15.7, 2.15.9, 2.15.10, 2.15.16, 2.15.17,
2.15.18, 2.15.19, 2.15.20, 2.15.25, 2.15.32, 2.15.39, 2.15.40,
2.15.42, 2.15.43, 2.16.0, 2.17.0, 2.17.4, 2.17.5, 2.17.6, 2.17.11,
2.17.14, 2.17.15, 2.17.18, 2.17.19, 2.17.20, 2.17.25, 2.17.27,
2.17.29, 2.17.97
%}
