# How to build:

## One-time setup
1. Install Docker. Start at https://www.docker.com/community-edition and pick the correct OS and install.
2. Add this script to your path. Call it `dockercmd.sh` and make it executable.

```shell
    #!/bin/sh
    exec docker run --rm -i --user="$(id -u):$(id -g)" -v $PWD:/data blang/latex "$@"
```

## Update/Clone the repository.
Enter the bsm folder and run:

```shell
    dockercmd.sh /bin/sh -c "pdflatex music-vol1.tex && pdflatex music-vol1.tex pdflatex music-vol2.tex && pdflatex music-vol2.tex"
```

## Retrieve your files
You now have (among others) `music-vol1.pdf` and `music-vol2.pdf`. Those are your books.
You may delete `*.aux`, `*.log` and `*.out`.

If you can't move them to a USB drive, or other accessible folder,
```shell
    python3 -m http.server 80
```

will get you a temporary HTTP server in your working directory