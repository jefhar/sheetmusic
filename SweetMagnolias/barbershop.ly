%* SVN FILE: $Id$ *%
%* vim: set expandtab tabstop=4 softtabstop=4 shiftwidth=4: *%
% @author $Author$
% @version $Revision$
% @lastrevision $Date$
% @filesource $URL$

% This is a simple Barbershop template for lilypond.
% If you need something slightly more complex, search for it on the internet.

\version "2.17.97"

\header {
  % http://lilypond.org/doc/v2.12/Documentation/user/lilypond/Creating-titles
  title = "I Will Dream of You"
  subtitle = "(A Special Tag)"
  subsubtitle = "(date)"
  composer = "Music by"
  poet = "Lyrics by"
  arranger = "Arranged by"
  copyright = \markup \smaller \column {"Arrangement © 2009 Jeff Harris, Barbershop Bass Publishing, ASCAP. All Rights Reserved. No recording use, public performance" "for profit use or any other use requiring authorization, or reproduction or sale of copies in any form shall be made of or from this" "Arrangement unless licensed by the copyright owner or an agent or organization acting on behalf of the copyright owner." }
  tagline = \markup \column {"Questions about the contest suitability of this song/arrangement should be directed to the judging community" "and measured against current contest rules. Ask before you sing." "$Id$" }
}

\include "english.ly"
%\pointAndClickOff
#(set-global-staff-size 17.82 )
%{  Note: You should always turn off point and click in any LilyPond files to be
  distributed to avoid including path information about your computer in the
  .pdf file, which can pose a security risk.
%}

\paper {
  #(set-paper-size "letter")
  bottom-margin   = 0.75\in
  top-margin      = 0.50\in
  left-margin     = 0.75\in
  line-width      = 7.00\in
  last-bottom-spacing = 0.25\in
}

global = {
  \key c \major
  \time 4/4
  \autoBeamOff
  \set Score.tempoHideNote = ##t
  \tempo 4=96
  \set Staff.midiInstrument = #"trombone"
}

tenorMusic = \relative c' {
  a4 b c f |
  e1 |
  ef2 d |
  e1 \bar "|."
}

tenorWords = \lyricmode {
  _
}

leadMusic = \relative c' {
  a4 b a b |
  c1 |
  c2 c |
  c1
}

leadWords = \lyricmode {
  I will dream of you till I die.
}

bariMusic = \relative c' {
  a4 g f g |
  g1 |
  af2 <
  af
  \parenthesize \tweak font-size #-1 bf
  >  |
  g1
}

bariWords = \lyricmode {
  _
}

bassMusic = \relative c' {
  a4 g f d |
  c1 |
  ef2 f |
  c1
}

bassWords = \lyricmode {
  _
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%       STOP EDITING HERE.    STOP EDITING HERE.       %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% showLastLength = R2*48

\score
{ \new ChoirStaff <<
    \new Lyrics = "tenors" \with {
      % this is needed for lyrics above a staff
      \override VerticalAxisGroup.staff-affinity = #DOWN
    }{ s1 }
    \new Staff = topstaff <<
      \clef "treble_8"
      \new Voice = "tenors" {
        \voiceOne
        << \global
          \set midiInstrument = #"clarinet"
          \tenorMusic >>
      }
      \new Voice = "leads" {
        \voiceTwo
        << \global \leadMusic >>
      }
    >>
    \new Lyrics = "leads" { s1 }
    \new Lyrics = "baris" \with {
      % this is needed for lyrics above a staff
      \override VerticalAxisGroup.staff-affinity = #DOWN
    } { s1 }
    \new Staff = bottomstaff <<
      \clef bass
      \new Voice = "baris" {
        \voiceOne
        << \global \bariMusic >>
      }
      \new Voice = "basses" {
        \voiceTwo << \global \bassMusic >>
      }
    >>
    \new Lyrics = basses { s1 }
    \context Lyrics = tenors \lyricsto tenors \tenorWords
    \context Lyrics = leads \lyricsto leads \leadWords
    \context Lyrics = baris \lyricsto baris \bariWords
    \context Lyrics = basses \lyricsto basses \bassWords
  >>
  \layout {
    \context {
      \Staff
    }
    \context {
      \Lyrics
      \override LyricSpace.minimum-distance = #2.0
    }
  }
  \midi {
    \context {
      \ChoirStaff
      \remove "Staff_performer"
    }
    \context {
      \Voice
      \consists "Staff_performer"
    }
  }
}


%{
convert-ly (GNU LilyPond) 2.18.0  convert-ly: Processing `'...
Applying conversion: 2.15.7, 2.15.9, 2.15.10, 2.15.16, 2.15.17,
2.15.18, 2.15.19, 2.15.20, 2.15.25, 2.15.32, 2.15.39, 2.15.40,
2.15.42, 2.15.43, 2.16.0, 2.17.0, 2.17.4, 2.17.5, 2.17.6, 2.17.11,
2.17.14, 2.17.15, 2.17.18, 2.17.19, 2.17.20, 2.17.25, 2.17.27,
2.17.29, 2.17.97
%}
