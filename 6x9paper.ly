%\pointAndClickOn
\pointAndClickOff
#(set-global-staff-size 12.60 )
%{  Note: You should always turn off point and click in any LilyPond files to be
  distributed to avoid including path information about your computer in the
  .pdf file, which can pose a security risk.
%}

\paper {
  % #(set-paper-size "trade")
  two-sided       = ##t
  % print-page-number = ##f
  %  bottom-margin   = 0.50\in
  %  top-margin      = 0.50\in
  % inner-margin    = 0.1\in
  % outer-margin    = 0\in
  line-width      = 4.2\in
  %% last-bottom-spacing = 0.25\in
  % binding-offset = 0.1\in
}
