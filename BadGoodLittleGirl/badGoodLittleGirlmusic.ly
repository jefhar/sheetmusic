%{ SVN FILE: $Id$
   @author $Author$
   @version $Revision$
   @lastrevision $Date$
   @filesource $URL$

   There's a Little Bit of Bad in Every Good Little Girl
   Lyrics by Grant Clarke, Music by Fred Fischer.
   copyright 1916.

   Arranged for Barbershop Quartet by Jeff Harris.
%}

\version "2.17.97"
\language "english"

\header {
  % http://lilypond.org/doc/v2.12/Documentation/user/lilypond/Creating-titles
  title = "There's A Little Bit Of Bad In Every Good Little Girl"
  subtitle = "(1916)"
  poet = "GRANT CLARKE."
  composer = "FRED FISCHER."
  lastupdated = "11/9/2012"
  arranger = "Arranged by JEFF HARRIS."
  copyright = \markup \smaller \column {"Arrangement © 2013 Jeff Harris, Barbershop Bass Publishing, ASCAP. All Rights Reserved. No recording use, public performance" "for profit use or any other use requiring authorization, or reproduction or sale of copies in any form shall be made of or from this" "Arrangement unless licensed by the copyright owner or an agent or organization acting on behalf of the copyright owner." }
  tagline = \markup \column {"Questions about the contest suitability of this song/arrangement should be directed to the judging community" "and measured against current contest rules. Ask before you sing." "$Id$" }
}

\include "english.ly"

global = {
  \key bf \major
  \time 2/2
  \autoBeamOff
  \set Score.tempoHideNote = ##t
  \tempo 4=92
  % \override SpacingSpanner.spacing-increment = #8.0

}

xVoice = \markup { \smaller \sans { x } } % print this by using '\xVoice'

tenorMusic = \relative c' {
  % dynamic marking is only for Midi track and should be removed before printing
  \set midiInstrument = #"clarinet"
  % Verse 1
  \tuplet 3/2 {bf8\mf^\markup { "Verse 1" } c cs} d8. d16 d8. d16 d8. d16 |
  e4 e4~ e8. e16 ef8. e16 |
  f4 f2 ef4 |
  d2~ d4. e8

  s8. s16 s8. s16 s8. s16 s8. e16 |
  ef4 ef4~ ef8. ef16 ef8. ef16 |
  ef4 ef2 ef4 |
  d2~ d4. s8 |

  cs4 cs cs4. cs8 |
  d8. bf16 d8. bf16 c4. r8 |
  e4 e e4. e8 |
  ef8. d16 ef8. e16 ef4 r4 |

  % Chorus 1
  d8.^\markup { "Chorus 1" } d16 d8. d16 d8. d16 d8. f16
  f8 ef ef ef~ ef2 |
  s4 f4 f ef|
  d2\( ef4.\) s8 |

  f4 d d4. f8 |
  c8. c16 c8. ef16 ef4 ef |
  e4 e e e |
  ef2~ ef4 f |

  es4 f g f |
  e4 e2 ef4 |
  ds4 ef f ef |
  d4 d2 s8. s16 |

  d8. d16 d8. d16 d8. d16 d8. f16
  f8 ef ef ef~ ef2 |
  s4 f4 f ef|
  d2\( d\) |

  % Verse 2
  s1^\markup { "Verse 2" } |
  e4 e4~ e8. e16 ef8. e16 |
  f4 f2 ef4 |
  d2~ d4. e8

  s8. s16 s8. s16 s8. s16 s8. e16 |
  ef4 ef4~ ef8. ef16 ef8. ef16 |
  ef4 ef2 ef4 |
  d2~ d4. s8 |

  cs4 cs cs4. cs8 |
  d8. bf16 d8. bf16 c4. r8 |
  e4 e e4. e8 |
  ef8. d16 ef8. e16 ef4 r4 |

  % Chorus 1
  d8.^\markup { "Chorus 2" } d16 d8. d16 d8. d16 d8. f16
  f8 ef ef ef~ ef2 |
  s4 f4 f ef|
  d2\( ef4.\) s8 |

  f4 d d4. f8 |
  c8. c16 c8. ef16 ef4 ef |
  e4 e e e |
  ef2~ ef4 f |

  es4 f g f |
  e4 e2 ef4 |
  ds4 ef f ef |
  d4 d2 s8. s16 |

  d8. d16 d8. d16 d8. d16 d8. f16
  f8 ef ef ef~ ef2 |
  s4 f4 f ef|
  d2\( d\) |


}
tenorWords = \lyricmode {
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _

}

leadMusic = \relative c {
  % dynamic marking is only for Midi track and should be removed before printing
  \set midiInstrument = #"trombone"
  % Verse 1
  \tuplet 3/2 {d8 ef e} f8. fs16 g8. a16 bf8. b16 |
  d4 d4~ d8. g,16 fs8. g16 |
  d'4 d2 a4 |
  c2~ c4. bf8 |

  c8. bf16 f8. d16 cs8. d16 a'8. g16 |
  g4 g4~ g8. g16 a8. f16 |
  g4 g2 g4 |
  g2~ g4. b8\rest |

  e,4 a a4. e8 |
  f8. e16 f8. g16 a4. r8 |
  g4 c c4. g8 |
  a8. gs16 a8. bf16 c4 \bar "||" \break

  % Chorus 1
  d8. bf16 |
  c8. a16 bf8. g16 a8. f16 g8. d16 |
  ef8 f a c~ c2 |
  b4\rest ef d c |
  f,2~ f4. r8 |

  d4 g g4. d8 |
  ef8. d16 ef8. f16 g4 g |
  g4 a bf d |
  c2~ c4 d |

  cs4 d ef d |
  g,4 c2 c4 |
  b4 c d c |
  f,4 bf2 d8. bf16 |

  c8. a16 bf8. g16 a8. f16 g8. d16 |
  ef8 f a c~ c2 |
  r4 ef d c |
  bf1 | \break

  % Verse 2
  \tuplet 3/2 {d,8 ef e} f8. fs16 g8. a16 bf8. b16 |
  d4 d4~ d8. g,16 fs8. g16 |
  d'4 d2 a4 |
  c2~ c4. bf8 |

  c8. bf16 f8. d16 cs8. d16 a'8. g16 |
  g4 g4~ g8. g16 a8. f16 |
  g4 g2 g4 |
  g2~ g4. r8 |

  e4 a a4. e8 |
  f8. e16 f8. g16 a4. r8 |
  g4 c c4. g8 |
  a8. gs16 a8. bf16 c4 \bar "||" \break

  % Chorus 2
  d8. bf16 |
  c8. a16 bf8. g16 a8. f16 g8. d16 |
  ef8 f a c~ c2 |
  r4 ef d c |
  f,2~ f4. r8 |

  d4 g g4. d8 |
  ef8. d16 ef8. f16 g4 g |
  g4 a bf d |
  c2~ c4 d |

  cs4 d ef d |
  g,4 c2 c4 |
  b4 c d c |
  f,4 bf2 d8. bf16 |

  c8. a16 bf8. g16 a8. f16 g8. d16 |
  ef8 f a c~ c2 |
  r4 ef d c |
  bf1 | \break
  \bar "|."

}
leadWords = \lyricmode {
  % Verse 1
  No -- bod -- y ev -- er sings a -- bout the
  bad girls, __ be -- cause the
  bad girls are
  sad. __
  And
  ev -- 'ry bod -- y sings a -- bout the
  good girls be -- cause the
  good girls are
  glad. __
  Till you've been a --
  round em once or twice
  You can't tell the
  naugh -- ty from the nice,

  There's a
  lit -- tle bit of bad in ev -- 'ry
  good lit -- tle girl __
  They're not to
  blame __

  Moth -- er Eve was
  ver -- y ver -- y good, But
  e -- ven she raised
  Cain __ I

  know a preach -- er's
  daugh -- ter, Who
  nev -- er or -- ders
  wa -- ter There's a
  lit -- tle  bit of bad in ev -- 'ry
  good lit -- tle girl,

  They're all the same. __

  I had a dream I went to see the
  dev -- il __ there was the
  dev -- il to
  pay__ He
  said I'm aw -- ful bus -- y on the
  lev -- el __ I said the
  dev -- il you
  say __

  Why are you so
  bus -- y tell me why
  He re -- plied and
  winked his oth -- er eye.

  There's a
  lit -- tle bit of bad in ev -- 'ry
  good lit -- tle girl __
  They're not to
  blame __

  Though they seem like
  An -- gels in a dream, They're
  naugh -- ty just the
  same __ They

  read the good book
  Sun -- day, and
  “snap -- py stor -- ies”
  Mon -- day There's a
  lit -- tle  bit of bad in ev -- 'ry
  good lit -- tle girl,

  They're all the same. __



}

bariMusic = \relative c {
  % dynamic marking is only for Midi track and should be removed before printing
  \set midiInstrument = #"clarinet"
  % Verse 1
  s1\mf |
  g'4 g4~ g8. bf16 a8. bf16|
  a4 a2 a4 |
  f2~ f4. g8 |

  s8. s16 s8. s16 s8. s16 s8. cs16 |
  a4 a4~ a8. a16 f8. a16 |
  a4 a2 a4 |
  f2~ f4. s8 |

  g4 g g4. g8 |
  a8. g16 a8. g16 fs4. r8 |
  bf4 bf bf4. bf8 |
  c8. b16 c8. c16 a4 s4 |

  % Chorus 1
  f8. f16 f8. bf16 f8. bf16 bf8. bf16 |
  a8 a f f~ f2 |
  g4\rest a a a |
  bf4\(gs a4.\) s8 |

  bf4 bf bf4. af8 |
  a8. a16 a8. c16 a4 a |
  bf4 c g gs |
  a2~ a4 c |

  b4 b b b |
  bf4 bf2 bf4 |
  a4 a a f |
  bf4 f2 s8. s16 |

  f8. f16 f8. bf16 f8. bf16 bf8. bf16 |
  a8 a f f~ f2 |
  g4\rest a a a |
  f2\( f\) |

  % Verse 2
  s1 |
  g4 g4~ g8. bf16 a8. bf16|
  a4 a2 a4 |
  f2~ f4. g8 |

  s8. s16 s8. s16 s8. s16 s8. cs16 |
  a4 a4~ a8. a16 f8. a16 |
  a4 a2 a4 |
  f2~ f4. s8 |

  g4 g g4. g8 |
  a8. g16 a8. g16 fs4. r8 |
  bf4 bf bf4. bf8 |
  c8. b16 c8. c16 a4 s4 |

  % Chorus 2
  f8. f16 f8. bf16 f8. bf16 bf8. bf16 |
  a8 a f f~ f2 |
  g4\rest a a a |
  bf4\(gs a4.\) s8 |

  bf4 bf bf4. af8 |
  a8. a16 a8. c16 a4 a |
  bf4 c g gs |
  a2~ a4 c |

  b4 b b b |
  bf4 bf2 bf4 |
  a4 a a f |
  bf4 f2 s8. s16 |

  f8. f16 f8. bf16 f8. bf16 bf8. bf16 |
  a8 a f f~ f2 |
  g4\rest a a a |
  f2\( f\) |
}
bariWords = \lyricmode {
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _

}

bassMusic = \relative c {
  % dynamic marking is only for Midi track and should be removed before printing
  \set midiInstrument = #"clarinet"
  % Verse 1
  d1\rest\ff |
  c4 c4~ c8. c16 c8. c16 |
  f4 f2 f4 |
  bf,2~ bf4. df8

  s8. s16 s8. s16 s8. s16 s8. a16 |
  c4 c4~ c8. c16 c8. c16 |
  c4 c2 c4 |
  bf2~ bf4. d8\rest

  a4 e' e4. a,8 |
  d8. c16 d8. d16 d4. r8 |
  c4 g' g4. c,8 |
  f8. f16 f8. g16 f4 d4\rest |

  % Chorus 1
  bf8. bf16 bf8. bf16 bf8. bf16 bf8. bf16 |
  c8 c f, f~ f2 |
  c'2 f4 f |
  bf,4\( b c4.\) d8\rest |

  bf4 bf bf4. bf8 |
  f8. f16 f8. f16 c'4 c |
  c4 c c b |
  f'2~ f4 af |

  gs4 g g g |
  c,4 c2 gf'4 |
  fs4 f f c |
  bf4 bf2 s8. s16 |

  bf8. bf16 bf8. bf16 bf8. bf16 bf8. bf16 |
  c8 c f, f~ f2 |
  c'2 f4 f |
  bf,2\( bf\) |

  % Verse 2
  \tuplet 3/2 {bf8 c cs} d8. d16 d8. d16 d8. d16 |
  c4 c4~ c8. c16 c8. c16 |
  f4 f2 f4 |
  bf,2~ bf4. df8

  s8. s16 s8. s16 s8. s16 s8. a16 |
  c4 c4~ c8. c16 c8. c16 |
  c4 c2 c4 |
  bf2~ bf4. d8\rest

  a4 e' e4. a,8 |
  d8. c16 d8. d16 d4. r8 |
  c4 g' g4. c,8 |
  f8. f16 f8. g16 f4 d4\rest |

  % Chorus 2
  bf8. bf16 bf8. bf16 bf8. bf16 bf8. bf16 |
  c8 c f, f~ f2 |
  c'2 f4 f |
  bf,4\( b c4.\) d8\rest |

  bf4 bf bf4. bf8 |
  f8. f16 f8. f16 c'4 c |
  c4 c c b |
  f'2~ f4 af |

  gs4 g g g |
  c,4 c2 gf'4 |
  fs4 f f c |
  bf4 bf2 s8. s16 |

  bf8. bf16 bf8. bf16 bf8. bf16 bf8. bf16 |
  c8 c f, f~ f2 |
  c'2 f4 f |
  bf,2\( bf\) |
}
bassWords = \lyricmode {
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ They're __ not to _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ They're __ all the _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _ _ _ _
  _ _ _ _ _

}

thechords = \chordmode {
  %Verse 1
  s1 c1:9
}

\include "../BadGoodLittleGirl/badGoodLittleGirl.ly"


%{
convert-ly (GNU LilyPond) 2.18.0  convert-ly: Processing `'...
Applying conversion: 2.17.0, 2.17.4, 2.17.5, 2.17.6, 2.17.11, 2.17.14,
2.17.15, 2.17.18, 2.17.19, 2.17.20, 2.17.25, 2.17.27, 2.17.29, 2.17.97
%}
