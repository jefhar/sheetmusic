%{ SVN FILE: $Id$
   @author $Author$
   @version $Revision$
   @lastrevision $Date$
   @filesource $URL$

   What A Wonderful Girl You Are
   Lyrics by Frank Davis and Harry Tobias, Music by Max Prival.
   copyright 1920.

   Arranged for Barbershop Quartet by Jeff Harris.
%}

\version "2.18.0"



\header {
    % http://lilypond.org/doc/v2.12/Documentation/user/lilypond/Creating-titles

    title = "What A Wonderful Girl You Are" % This should be uppercase
    subsubtitle = "(1920)" % Use this for the date
    poet = \markup \column {"Words by FRANK DAVIS" "and HARRY TOBIAS" } % the lyricist
    lastupdated = "11/9/2012"
    composer = "Music by MAX PRIVAL" % composer
    arranger = "Arranged by JEFF HARRIS." % Arranger
    copyright = "Arrangement © 2014 Jeff Harris, Barbershop Bass Publishing, ASCAP. All Rights Reserved. No recording use, public performance for profit use or any other use requiring authorization, or reproduction or sale of copies in any form shall be made of or from this Arrangement unless licensed by the copyright owner or an agent or organization acting on behalf of the copyright owner."
    tagline =
            "Questions about the contest suitability of this song/arrangement should be directed to the judging community and measured against current contest rules. Ask before you sing."


    % dedication = "dedication"
    % subtitle = "(SUBTITLE)" % This should be uppercase
    % instrument = "instrument" % as sung by, from the movie, for female voices
    % meter = "meter" % This will appear under the Poet. Don't use, or use for second lyricist
    % piece = "piece"
    % opus = "opus"
}

\language "english"

global = {
    \key c \major
    \time 3/4
    % \autoBeamOff
    \set Score.tempoHideNote = ##t
    \tempo 4=100
    % \override SpacingSpanner.spacing-increment = #8.0

    % Left-align bar numbers
    \override Score.BarNumber.break-visibility = ##(#f #t #t)
    \override Score.BarNumber.self-alignment-X = #LEFT
}

xVoice = \markup { \smaller \sans { x } } % print this by using '\xVoice'

tenorMusic = \relative c' {
    % dynamic marking is only for Midi track
    \set midiInstrument = #"clarinet"
    e4-\omit\p^\markup { "Verse 1" } e e |
    g g e |
    fs2. \(
    f!2. \)|


    e4 e e |
    e e4. e8 |
    c2. \(
    cs2. \) |

    f2 f8 f |
    fs2 fs4 |
    f?2. \(
    f2. \) |

    a4 fs fs |
    d fs fs |
    f?2. \( |
    f2. \) |

    e4^\markup { "Verse 2" } e e |
    e cs e |
    fs2. \(
    f!2. \) |

    e4 e e |
    e e4. e8 |
    f2. \(
    g2. \) |

    f2 f8 f |
    fs2 fs4 |
    f?2. \(
    f2. \) |

    a4 fs fs |
    d fs fs |
    f?2. \( |
    f2. \) |

    % Chorus
    e4-\omit\mp^\markup { "Chorus" } e e |
    c d e |
    f2. \( |
    f2. \) |

    f4 f e |
    g f d |
    cs2. \( |
    cs \) |

    e4 e e |
    ef ef4. ef8 |
    d2. \( |
    d2. \) |

    d4 c b |
    d d f |
    e2. \( |
    f \) |

    e4 e e |
    e e e |
    d2. \( |
    ef \) |

    gs4 g gs |
    gs gs gs |
    a2. \( |
    bf2. \) |

    a4 f f |
    ds ds ds |
    e2. \( |
    g2^\fermata \) \once \override BreathingSign.text = \markup {
        \musicglyph #"scripts.caesura.straight"
    } \breathe e8 e8


    f4 a a |
    f2 f4 |
    e2. \( |
    ef2 \) \breathe s4 |

    % Tag
    f4-\omit\p^\markup { "Tag" } d f |
    f2 f4 |
    e2. \(
    c2. \)

}
tenorWords = \lyricmode {
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _

}

leadMusic = \relative c' {
    % dynamic marking is only for Midi track and should be removed before printing
    \set midiInstrument = #"trombone"
    c4 b c |
    b e, g |
    a2. \( |
    b2. \) |

    c4 b c |
    b4 d4. c8
    a2. \( |
    a2. \) |

    d2 cs8 d |
    c?2 a4 |
    c2. \( |
    b2. \) |

    fs4 a c |
    fs,4 e' ef |
    d2. \( |
    d2. \) |

    c4 b c |
    b e, g |
    a2. \( |
    b2. \) |

    c4 b c |
    b4 d4. c8 |
    a2. \( |
    a2. \) |

    d2 cs8 d |
    c?2 a4 |
    c2. \( |
    b2. \) |

    fs4 a c |
    fs,4 e' ef |
    d2. \( |
    d2. \)  |  \bar "||"

    % Chorus
    d4 c g |
    e f g |
    gs2. \( |
    a2. \) |

    b4 c cs |
    e d b |
    a2.~ |
    a2. |

    c4 b a |
    c b4. a8 |
    c2. \( |
    f,2. \) |

    b4 a g |
    b c d |
    g,2.~ |
    g2. |

    g4 a bf |
    d c g |
    gs2. \( |
    a2. \) |

    b4 as b |
    d c b |
    c2.~ |
    c2. |

    e4 d c |
    b c a |
    g2. \( |
    e'2 \) g,8 gs |

    a4 f' e |
    c2 d4 |
    c2. \( |
    c2 \) b4\rest
    \bar "||"

    c4-\omit\p c c|
    d2 g,4 |
    g2. \(
    e2. \) |


    \bar "|."

}
leadWords = \lyricmode {
    I nev -- er knew there was sun -- shine,
    My life was emp -- ty and blue, __ _
    I nev -- er knew what love meant,
    Sweet -- heart un -- til I met you; __ _

    I nev -- er knew what a smile meant,
    My life was noth -- ing but sighs, __ _
    You brought the joy to me, dear,
    When I gazed in -- to your eyes; __ _

    You changed a sad heart to glad -- ness,
    You brought me sun -- shine a -- gain, __
    You chased the dark clouds a -- way, dear,
    And eased my sor -- row and pain, __

    Wrong turned to right with you near me,
    Your love is my guid -- ing star, __
    You're like an an -- gel from hea -- ven,
    What a won -- der -- ful girl you are. __
    _
    won -- der -- ful girl you are. __

}

bariMusic = \relative c' {
    % dynamic marking is only for Midi track and should be removed before printing
    \set midiInstrument = #"clarinet"
    % Verse 1
    g4-\omit\mf g g |
    cs cs cs |
    c?2. \( |
    d2. \) |

    g,4 g g |
    gs gs4. gs8 |
    f2. \(
    g2. \) |

    a2 a8 a |
    a2 c4 |
    af2. \(
    g2. \)

    c4 c a |
    c c c |
    b2. \(
    b2. \) |

    % Verse 2
    g4 g g |
    cs a cs |
    c?2. \( |
    g2. \) |

    g4 g g |
    gs gs4. gs8 |
    c2. \(
    cs2. \) |

    a2 a8 a |
    a2 c4 |
    af2. \(
    g2. \)

    c4 c a |
    c c c |
    b2. \(
    b2. \) |

    % Chorus
    g4-\omit\mf g c |
    c b bf |
    b?2. \( |
    b2. \) |

    g4 a g |
    b b f |
    e2. \( |
    g2. \) |

    g4 g c |
    fs, fs4. fs8 |
    fs2. \( |
    b2. \) |

    f4 fs f |
    f fs b |
    c2 \( as4 |
    b2. \) |

    c4 g g |
    g g bf |
    b?2. \( |
    c2. \) |

    d4 df d  |
    b d d |
    e2. \( |
    e2. \) |

    c4 a a |
    a a fs |
    c'2. \(
    cs2 \) \once \override BreathingSign.text = \markup {
        \musicglyph #"scripts.caesura.straight"
    } \breathe s4 |

    c4 c c |
    af2 b4 |
    a2. \( |
    a2 \) \breathe r4

    d4-\omit\p a a
    a2 b4
    c2. \( |
    g2. \) |


}
bariWords = \lyricmode {
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _

}

bassMusic = \relative c {
    % dynamic marking is only for Midi track and should be removed before printing
    \set midiInstrument = #"clarinet"
    % Verse 1
    c4-\omit\mf c c |
    e a, a |
    d2. \( |
    g,2. \) |

    c4 c c |
    e b4. c8 |
    f2. \( |
    e2. \) |

    d2 d8 d |
    ds2 d4 |
    d2. \( |
    d2. \) |

    d4 d d |
    a' a af |
    g2. \( |
    g2. \)

    % Verse 2
    c,4 c c |
    a a a |
    d2. \( |
    d2. \) |

    c4 c c |
    e b4. c8 |
    f2. \( |
    e2. \) |

    d2 d8 d |
    ds2 d4 |
    d2. \( |
    d2. \) |

    d4 d d |
    a' a af |
    g2. \( |
    g2. \)

    % Chorus
    c,4-\omit\mf c c |
    g g c |
    cs2. \( |
    d2. \) |

    d4 c bf |
    g g g |
    a2. \( |
    e' \) |

    c4 c c |
    c c4. c8 |
    a2. \( |
    g \) |

    g4 d' d |
    g, a g |
    c2 \( cs4 |
    d2. \) |

    c4 c c |
    c c c |
    f2. \( |
    f2. \) |

    e4 e4 e |
    e e e |
    a2. \(  |
    g2. \) |

    f4 f f|
    fs fs b, |
    c2. \( |
    a2 \) d4\rest |

    d4 d a |
    d2 g,4 |
    a2 \( g4 |
    fs2 \) g8-\omit\ff gs|

    a4 f' e |
    c2 d4-\omit\mf |
    c2. \(
    c2. \)


}
bassWords = \lyricmode {
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ _ _ _ _
    _ _ _ _ are, my star, What a
}


%{
  convert-ly (GNU LilyPond) 2.16.1  convert-ly: Processing `'...
  Applying conversion: 2.15.7, 2.15.9, 2.15.10, 2.15.16, 2.15.17,
  2.15.18, 2.15.19, 2.15.20, 2.15.25, 2.15.32, 2.15.39, 2.15.40,
  2.15.42, 2.15.43, 2.16.0
%}


%{
  convert-ly (GNU LilyPond) 2.18.0  convert-ly: Processing `'...
  Applying conversion: 2.17.0, 2.17.4, 2.17.5, 2.17.6, 2.17.11, 2.17.14,
  2.17.15, 2.17.18, 2.17.19, 2.17.20, 2.17.25, 2.17.27, 2.17.29, 2.17.97
%}


%{
  convert-ly (GNU LilyPond) 2.18.2  convert-ly: Processing `'...
  Applying conversion: 2.18.0
%}
