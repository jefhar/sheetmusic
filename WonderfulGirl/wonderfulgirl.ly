%{ SVN FILE: $Id$
   @author $Author$
   @version $Revision$
   @lastrevision $Date$
   @filesource $URL$

   What A Wonderful Girl You Are
   Lyrics by Frank Davis and Harry Tobias, Music by Max Prival.
   copyright 1920.

   Arranged for Barbershop Quartet by Jeff Harris.
%}

\version "2.18.0"
\language "english"

\include "../letterpaper.ly"
\include "../WonderfulGirl/wonderfulgirlmusic.ly"
\paper {
    #(define fonts
         (set-global-fonts
#:roman "Times New Roman"
          #:music "cadence"


          ))
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%       STOP EDITING HERE.    STOP EDITING HERE.       %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
showLastLength = R2.*36
\score
{
    \new ChoirStaff <<
        \new Lyrics = "tenors" \with {
            % this is needed for lyrics above a staff
            \override VerticalAxisGroup.staff-affinity = #DOWN
        }{ s1 }
        \new Staff = topstaff <<
            \override Staff.InstrumentName.self-alignment-X = #LEFT
            \set Staff.instrumentName = \markup \left-column {
                "Tenor"
                "Lead"
            }
            \set Staff.shortInstrumentName = #""
            \clef "treble_8"
            \new Voice = "tenors" {
                \voiceOne
                <<
                    \global
                    \set midiInstrument = #"clarinet"
                    \tenorMusic
                >>
            }
            \new Voice = "leads" {
                \voiceTwo
                <<
                    \global
                    \leadMusic
                >>
            }
        >>
        \new Lyrics = "leads" { s1 }
        \new Lyrics = "baris" \with {
            % this is needed for lyrics above a staff
            \override VerticalAxisGroup.staff-affinity = #DOWN
        } { s1 }
        \new Staff = bottomstaff <<
            \override Staff.InstrumentName.self-alignment-X = #LEFT
            \set Staff.instrumentName = \markup \left-column {
                "Bari"
                "Bass"
            }
            \set Staff.shortInstrumentName = #""
            \clef bass
            \new Voice = "baris" {
                \voiceOne
                <<
                    \global
                    \bariMusic
                >>
            }
            \new Voice = "basses" {
                \voiceTwo <<
                    \global
                    \bassMusic
                >>
            }
        >>
        \new Lyrics = basses { s1 }
        \context Lyrics = tenors \lyricsto tenors \tenorWords
        \context Lyrics = leads \lyricsto leads \leadWords
        \context Lyrics = baris \lyricsto baris \bariWords
        \context Lyrics = basses \lyricsto basses \bassWords
    >>
    \layout {
        \context {
            \Score
            \override SpacingSpanner.base-shortest-duration = #(ly:make-moment 1/32)
        }
        \context {
            \Staff
        }
        \context {
            \Lyrics
            \override LyricSpace.minimum-distance = #2.0
            \override LyricText.font-size = \absFontSize #11
        }
    }
    \midi {
        midiChannelMapping = #'voice
        \context {
            \ChoirStaff
            \remove "Staff_performer"
        }
        \context {
            \Voice
            \consists "Staff_performer"
        }
    }
}


%{
convert-ly (GNU LilyPond) 2.18.2  convert-ly: Processing `'...
Applying conversion: 2.18.0
%}
