All works here are copyright Jeff Harris unless otherwise noted. All 
works here are free to use and download in a shareware setting. If the 
song works for you, please donate what you think it is worth. Public 
performances are subject to appropriate ASCAP fees. Any performing of 
the works in a contest or adjudicated stage, for recording, for mass 
media, including but not limited to Television, Movies, internet 
broadcasts, or any other professional or semi-professional use must be 
licensed per performer.

Instrumental works must be licensed for not less than $ 4.00 USD per 
performer. Vocal works must be licensed for not less than $1.75 USD per 
performer. Works incorporating both Vocal and Instrument must be 
licensed for not less than $ 2.50 USD per performer.

For the purpose of licensing, works with pure piano reduction are 
considered Vocal works.

The purchase of a license allows you to perform said work in a realm as 
described above. You are not purchasing the music, nor any copyright.

## Otherwise Noted

The files in the bsm folder are freely available as noted on the page.
